#!/bin/bash

# A helper script for html->markdown
# Initial conversion has been done with
# https://domchristie.github.io/to-markdown/
# The html pasted there was taken from the
# old homepage with firefox's right click/View selection source

# The only links that did not get converted to md are not needed in my samples
perl -i -pe 's|<a[^>]*>||g' $1
perl -i -pe 's|</a>||g' $1

# verbatim is done with ` in markdown
perl -i -pe 's|<kbd>|`|g' $1
perl -i -pe 's|</kbd>|`|g' $1

# Some pages use <tt> as verbatim style
perl -i -pe 's|<tt>|`|g' $1
perl -i -pe 's|</tt>|`|g' $1

# Link flyspray bugs on gitlab
perl -i -pe 's|<fs ([1-9][0-9]*)=[^>]*>|[this bug](https://gitlab.dune-project.org/flyspray/FS/issues/$1)|g' $1
perl -i -pe 's|</fs>||g' $1
