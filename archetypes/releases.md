+++
# Please do not add releases directly, but instead use the script add_release.py
# from the bin subdirectory. It applies some additional magic to this archetype.
version = "{version}"
major_version = "{majorversion}"
minor_version = "{minorversion}"
patch_version = "{patchversion}"
modules = {which}
signed = 1
[menu.main]
parent = "releases"
weight = -1
+++
