+++
title = "Guidelines for bug reporting"
[main.menu]
parent = "issues"
+++

This is meant as a collection for how to make a useful bug report,
either as an issue on our [GitLab server](https://gitlab.dune-project.org/)
or as an email to one of our [mailing lists](/community/mailinglists). *Bug
report* is meant in a wide sense: the information we request here is
often also useful when you just want to ask a question. Also note that
these are *guidelines*: for instance, when you report a problem with the
webpage you don't have to tell us the version of your compiler.

When you are unable to obtain some piece of information requested here,
or it would require much work, please tell us so. Often, this already
gives us some leads about your original problem, and at other times we
may know a better way to obtain that information.

A usual bug report should observe the following points:

-   Keep the discussion on the list
-   Provide a problem description
-   Provide unabbreviated log files
-   Provide the version of your compiler
-   Provide the versions of your Dune modules

This guide may list more points, but generally the above should be a
good start.

Keep the discussion on the list
-------------------------------

Often other people have problems similar to yours, or other developers
on the list may have a better idea of how to solve your problem than the
guy you have been communicating with all the time. There are exceptions
of course: if we request some data that is either too big or too
confidential to send to the list, you can send it privately to whoever
requested it, but please send a note to the list that you did so, so the
others are not kept totally in the dark.

The same applies to the bug-tracker, of course.

Provide a problem description
-----------------------------

"Dune does not compile" does not give us many clues as to what the
problem is. We need to know exactly how you tried to compile, that means
for instance

-   the command, e.g. `dunecontrol --module=dune-foo all` or `make all`,
    and the directory you were in when you executed the command
-   if you used an options file for dunecontrol, the contents of that
    file
-   if you just clicked a button in your IDE, what IDE you used and
    which button it was

We need to know exactly what went wrong:

-   If there was an error message, give us the full error message
-   If there was no error message, tell us so, and also tell us how you
    noticed that something was wrong

Provide unabbreviated log files
-------------------------------

If something went wrong, there is often some output that can help to
determine the exact problem. If you run a compilation command like
`dunecontrol` or `make`, provide us with the complete output of that
command. Yes, it is a lot of information, but it often contains some
clues that we recognize. It is OK (and often even preferable) to give
this logfile as an attachment. If you are nice, you can even compress
it, but please use gzip or bzip2, if possible.

If you must, you can remove confidential information from those
logfiles, but please replace it by something like "\[CENSORED\]" and
explain what you removed ("I replaced all password by \[CENSORED\]") so
we know what is missing.

Provide the version of your compiler
------------------------------------

If you don't know which compiler is being used, look at the output that
is generated during compilation. It will probably contain lines that
have the name of compiler somewhere in the beginning. Common compiler
names include:

-   cc
-   gcc
-   c++
-   CC
-   g++
-   icc
-   clang++
-   xlc
-   Any of the above with a version number appended, e.g. g++-4.3
-   Any of the above with a path prepended, e.g. /opt/gcc/bin/g++

Once you have figured out what is probably your compiler, ask it for the
exact version: open a terminal and type `g++ --version` (substitute the
name of your compiler for g++). Send us the output of that command.

Provide the versions of your Dune modules
-----------------------------------------

-   If you used some tarball or downloadable distribution package, tell
    us the complete URL of the package download.

-   If your distribution provided the package via its package management
    system, tell us which distribution you use, which release of that
    distribution you use and the package version.

-   If you use Dune modules from Git, tell us from which branch and
    commit. You can get this information by running `git branch` and
    `git log -1 | grep '^commit '`, respectively.

Please give us the versions of all relevant Dune modules, not just the
one where the problem occurred. Please also give us similar information
for external libraries you have compiled Dune with, e.g. UG, ALUGrid,
Alberta.

CMakeOutput.log and CMakeError.log
----------------------------------

One of the steps when the buildsystem is generated is to run CMake
in the build directory of each modules directory tree
(by default the build directory is called `build-cmake`).
CMake checks the system for the presence or absence of different
features and generates all those lines beginning with `-- Check for ` or
`-- Performing Test ` in the compilation output. It also writes a logfile
`CMakeFiles/CMakeOutput.log` (and in case of errors
`CMakeFiles/CMakeError.log`) in the build directory.

When there is a problem with CMake, we usually want to see these
logfiles in addition to the output generated when compiling. Please
observe the point about unabbreviated logfiles.
