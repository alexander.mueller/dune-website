+++
title = "Google Summer of Code"
[menu.main]
parent = "dev"
weight = 5
+++

## Getting started as a DUNE GSoC Student

So you are excited about DUNE and Google Summer of Code. Cool! We are always looking for contributors and it is an honor for us if you spend your precious time on DUNE. We do this, too, and therefore value it.

Time is money or should we better say code? Anyway, it is one of the most precious things in life and should never be wasted unthoughtful. To save yourself and us time, we try to outline some steps that you should take if you are interested in being a GSoC student for the DUNE project.

### Before you apply

If you have not read up about the Google summer of code project, then now is the appropriate time. Read the [student guide](https://google.github.io/gsocguides/student/) and make sure that you qualify as a contributor according to Google's guidelines.

It is probably a good idea to subscribe to our [mailing lists](/mailing-lists). If you have questions about DUNE as a user, e.g. problems with installation, you should ask them on the DUNE user mailing list. Once you are acquainted to DUNE and start to develop DUNE further you will also need to read the DUNE developer mailing list. It is also the place to ask questions about the GSoC projects.

Get to know DUNE. As a developer you should of course know DUNE well. This means that you should have used DUNE before. If you have not done that already, now is the right time.

1. Install the development version of DUNE core modules from our repositories. [More](/dev/downloadgit)...
2. Build the DUNE core modules. [More](/doc/installation)...
3. Take a look at the howtos. These might not be directly related to your project, but are still a good way to get to know the way that DUNE works. [More](/doc/tutorials)...
4. Read up the DUNE documentation and some publications. Most probably you have done this already, haven't you?
5. Create your first DUNE module with the duneproject script and do something meaningful (read: related to the project idea) with it. This might be your opportunity to showcase that you are capable to be a our GSoc student this. Why not make it available on [gitlab.dune-project.org](https://gitlab.dune-project.org/) or [github](http://www.github.com/).
6. Read some of the code and try to understand it. Checkout the test programs in the "test" subdirectories of the various DUNE modules. This is a good way to become an expert.
7. Feeling confident already? Your are allowed to contribute before GSoC. What about checking out our [bugtracker](https://gitlab.dune-project.org/groups/core/issues) and see whether you are able to fix some bugs?

### Project idea

Look at this year's project [ideas page](/dev/contributing/). This is a list of projects that we are really interested in and of which we think that they are suited for GSoC students. While that does not mean that they are exclusive, proposing your own ideas will involve a lot of work. You have to write a good proposal to the developer mailing list. You have to convince us that your project is worthwhile our time and find a mentor for you among the developers willing to mentor your project.

### How to Apply
Applying for Google Summer of Code is not possible right now. We apply to GSoC 2022. Until we know whether we will be accepted, you are still welcome to contribute, just contact us through our mailing lists.
