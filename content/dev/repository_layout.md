+++
title = "Repository Layout"
[menu.main]
parent = "guides"
weight = 1
+++

In order to avoid turning the repositories into a messy heap of branches
sprawling all over the place, the Gitolite server on
git.dune-project.org enforces a certain repository layout w.r.t. to the
naming of branches. This layout is also used to provide different levels
of write access to different groups of people.

General layout
--------------

The general layout of the repositories is based around the idea of
"subtrees" of branches for different purposes as well as a pair of main
branches:

  Branch Name                               | Purpose
  ----------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------
  master                                    | Main development branch (corresponds to SVN trunk)
  release                                   | **Current** release branch (2.2 during repository creation)<br/> *This branch was not created during repository creation as some issues need to be ironed out first[^1].*
  releases/&lt;x.y&gt;\[-&lt;suffix&gt;\]   | Release branch for the given DUNE version.<br/> *The version should start with the numeric version of the release (e.g. "2.2"), followed by an optional suffix, e.g. "2.2-cmake".* <br/> *Keeping the numbers up front makes for better ordering of the releases in listings.*
  feature/\<name\>                            | Feature branch <br/> *Please use descriptive names. For consistency, I suggest joining words with dashes, not underscores.* <br/> *Examples:* **feature/cmake feature/dynamic-geometry-initialization**
  p/\<username\>/\<whatever\>                   | Personal branch of user `username`<br/> *This personal namespace provides users with an easy way to share ideas and transfer code between computers.* <br/> *Branches in this namespace are globally readable, but can only be written to by their owner.* <br/> *Commits to these branches will not be announced on the mailing list.*
  v\<x.y.z\>                                  | Annotated[^2] release tag for the given DUNE version. <br/> *As started with the 2.1 series, we should always use triplets for release tags, even for the initial release (e.g. `v2.2.0`).* <br/> *Historic releases have not been renamed to follow the triplet convention, though.*

Groups
------

Currently there are four different groups that grant varying levels of
write access:

-   **developers** Everybody with DUNE developer status (as taken from
    the SVN access file).
-   **releasemanagers** Have some additional rights in addition
    to developers. Currently just contains all **developers**.
-   **committers** Everybody with commit rights to the core repositories
    who doesn't have DUNE developer status (again as taken from the SVN
    access file).
-   **contributors** New tier without access to the release branches and
    the master branch.

Permissions
-----------

#### Remember: Read access in Git is always decided at the repository level - you cannot have private branches!

There is anonymous read access to the complete repositories. Write
access depends on the branch location:

  Branch Name           | Group                                               | Rights
  --------------------- | --------------------------------------------------- | ----------------------
  /4. master            | developers                                          | Normal Updates
  maintainers           | Readonly - but are normally members of developers	  |
  committers            | Normal Updates									  |
  contributors          | Readonly											  |
  /4. release           | developers                                          | Readonly
  maintainers           | Normal Updates									  |
  committers            | Readonly											  |
  contributors          | Readonly											  |
  /4. releases/\*       | developers                                          | Readonly
  maintainers           | Normal Updates\									  |
                        | Create new branches								  |
  committers            | Readonly											  |
  contributors          | Readonly											  |
  /4. feature/\*        | developers                                          | Normal Updates\
                        |                                                     | Create new branches\
                        |                                                     | Delete branches
  maintainers           | Readonly - but are normally members of developers	  |
  committers            | Normal Updates\									  |
                        | Create new branches\								  |
                        | Delete branches									  |
  contributors          | Normal Updates									  |
  /2. p/<username>/\*   | <username>                                          | Normal Updates\
                        |                                                     | Forced Updates\
                        |                                                     | Create new branches\
                        |                                                     | Delete branches
  everyone else         | Readonly											  |
  /4. tags (`v2.x.y`)   | developers                                          | Readonly
  maintainers           | Create new tags									  |
  committers            | Readonly											  |
  contributors          | Readonly											  |

This scheme can easily be extended. It would for example be possible to
have a new namespace `"gsoc/"` for GSoC students.

Editing Permissions
-------------------

The Gitolite server is compartmentalized into submodules. Each submodule
can define a group called `gitadmins` with rights to edit the submodule
configuration. For the core modules, the submodule is called "core" and
all official DUNE developers are members of `gitadmins`. For further
information, ask Steffen or Christian.

### Footnotes

[^1]: It would be nice to just be able to tell people to check out the
    "release" branch, but having a separate "release" and "releases/2.2"
    forces us to keep those two in sync. One idea would be to distribute
    an alias via `vcsetup` that takes care of this automatically. Needs
    some thought...

[^2]: Git has two types of tags, lightweight tags and annotated tags
    (there is also a GPG-signed variant of annotated tags). Lightweight
    tags are mostly like bookmarks to help you remember a certain commit
    while you work and don't have a corresponding object in the
    repository. Annotated tags do have such an object, which carries a
    tag message and permanently records the sha1 of the tagged commit.
    See the [chapter about
    tags](http://git-scm.com/book/en/Git-Basics-Tagging) in the Git book
    for more details.
