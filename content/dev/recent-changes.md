+++
title = "Recent Changes"
[menu.main]
parent = "dev"
weight = 4
+++

## Recent changes in Git master

* [dune-common](https://gitlab.dune-project.org/core/dune-common/blob/master/CHANGELOG.md)
* [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry/blob/master/CHANGELOG.md)
* [dune-grid](https://gitlab.dune-project.org/core/dune-grid/blob/master/CHANGELOG.md)
* [dune-istl](https://gitlab.dune-project.org/core/dune-istl/blob/master/CHANGELOG.md)
* [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions/blob/master/CHANGELOG.md)


## Changes in DUNE 2.8

### New minimal compiler and buildsystem version

* C++ compiler supporting c++-17 language standard, e.g. clang >= 5, g++ >= 7
* CMake >= 3.13

### List of other changes

* [dune-common](https://gitlab.dune-project.org/core/dune-common/blob/releases/2.8/CHANGELOG.md)
* [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry/blob/releases/2.8/CHANGELOG.md)
* [dune-grid](https://gitlab.dune-project.org/core/dune-grid/blob/releases/2.8/CHANGELOG.md)
* [dune-istl](https://gitlab.dune-project.org/core/dune-istl/blob/releases/2.8/CHANGELOG.md)
* [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions/blob/releases/2.8/CHANGELOG.md)


## Changes in DUNE 2.7

* [dune-common](https://gitlab.dune-project.org/core/dune-common/blob/releases/2.7/CHANGELOG.md)
* [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry/blob/releases/2.7/CHANGELOG.md)
* [dune-grid](https://gitlab.dune-project.org/core/dune-grid/blob/releases/2.7/CHANGELOG.md)
* [dune-istl](https://gitlab.dune-project.org/core/dune-istl/blob/releases/2.7/CHANGELOG.md)
* [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions/blob/releases/2.7/CHANGELOG.md)


## Changes in DUNE 2.6

### Dependencies

(The dependencies have not changes so far but are listed here for completeness.)

In order to build this version of DUNE you need at least the following software:

* CMake 3.1 or newer
* pkg-config
* A standard compliant C++ compiler supporting C++14.
  We support GCC 5 or newer and Clang 3.8 or newer. We try to stay compatible to ICC 16.0 and newer
  but this is not tested.

### Mailing lists move

The mailing lists moved into their own subdomain: from
`<list>@dune-project.org` to `<list>@lists.dune-project.org`.  This also
affects most maintainer addresses.

### List of other changes

* [dune-common](https://gitlab.dune-project.org/core/dune-common/blob/releases/2.6/CHANGELOG.md)
* [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry/blob/releases/2.6/CHANGELOG.md)
* [dune-grid](https://gitlab.dune-project.org/core/dune-grid/blob/releases/2.6/CHANGELOG.md)
* [dune-istl](https://gitlab.dune-project.org/core/dune-istl/blob/releases/2.6/CHANGELOG.md)
* [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions/blob/releases/2.6/CHANGELOG.md)
