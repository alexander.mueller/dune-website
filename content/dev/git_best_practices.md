+++
title = "Git best practices"
[menu.main]
parent = "guides"
weight = 4
+++


Git best practices
==================

Use feature branches
--------------------

Develop new features in a feature branch. Once you are done, rebase
the feature branch, merge it into master and delete your feature
branch.

*Rationale*: This keeps all changes done for a feature together. For
other people it is easier to cherry-picking the changes.

Meaningful commit messages
--------------------------

Use the first line as subject, if possible add key-words in brackets
like `[YaspGrid]` or `[CMake]`. Leave the second line blank and use the
following lines (3 ff) for details describing your commit.

*Rationale*: The first line will be shown in the Git history. Having it
meaningful makes life so match easier. For merges the first lines will
be used to indicate which commits are part of the merge.

Create and apply patches
------------------------

Patches are a handy way to share new code or bug fixes, especially if
commit rights are lacking. You can email patches to the mailing list or
attach them them to GitLab issues.

Create patches

1.  Pull to most recent version, that prevents conflicts while merging
    the patch later
2.  Commit your changes
3.  `git format-patch -k origin/master` If necessary replace the name of
    the remote or branch. This will create a patch file per commit.
4.  Accord these patches to the developers with commit rights.

Apply patches

1.  Pull to most recent version, that prevents conflicts while pushing
    the patch later
2.  `git am -k <patchfile>` Be aware that if the commit message starts
    with `[Patch]` or similar, the creator of the
    patch omitted `-k` in his command, do the same for `git am` as the
    flags suppresses this tag. You don't have to commit explicitly.
3.  Push the changes.

Do not use merge for simple commits
-----------------------------------

Someone committed while you were doing your stuff. If you want to push, Git
automatically merges the new head with your changes. Please make a Git
rebase first, to avoid the unnecessary merge.

&gt; \$&gt; git commit\
&gt; \$&gt; git pull\
&gt; (ok, something changed, you have to merge and commit this merge.
Now come you:)\
&gt; \$&gt; git rebase origin/master\
&gt; (merge vanishes)\
&gt; \$&gt; git push\
&gt; (et voilà, you just got rid of the merge)

*Rationale*: The merge is an extra commit which clutters the commit
history. It makes it more difficult to follow the changes as sometimes
many changes are in-between and the diff of the merge becomes large.

Use feature and private branches
--------------------------------

The development in feature branches triggers commit emails. This way
fellow developers will notice and might comment your work. Private
branches are for experimental stuff. It will not bother others with
commit emails but interested parties can still check it out.

*Rationale*: If people are aware of your development, they can join in
or help you with comments.

Remove no longer used branches
------------------------------

As branches are a handy feature of Git, they have to be removed if the
are no longer necessary. Otherwise the repository gets cluttered with
dozens of outdated, bit-rotting branches. Deleting branches with Git:

`git push origin --delete <branchName>`

Git offers the possibility to list all branches that have been already
merged:

`git branch -a --merged`

These branches can safely be removed. This works also for non-merged
branches with `--no-merge`.

The list of branches is cached locally and may contain outdated entries.
The list can be updated by

`git remote update origin --prune`

Referring to a commit by its hash
---------------------------------

All commits can be identified by a hash which is a 40 digits log
hex-decimal number (contains 0-9 and a-f). You can view a commit with

`git show <hash>`

that gives you the commit message and the full diff. Often it is enough
to shorten the full hash to the first seven or eight digits, but it must
stay unique. You can check that a shortened hash is unique by

`git rev-parse --short --verify <short-hash>`

*Rationale*: The full hash is sometimes cluttered, with the first 7-8
digits everybody should be happy.
