+++
date="2021-01-20"
title = "DUNE/PDELab Course Material available for self-study"
tags = [ "events", "core", "dune-pdelab" ]
+++

The Interdisciplinary Center for Scientific Computing in Heidelberg has hosted
an annual summer school on solving PDEs with Dune and PDELab for the last decade.
For this year's virtual edition of the course, we have recorded most of our
lectures. We are now making this material available for asynchronous self-study.
The material can be found here: [https://dune-pdelab-course.readthedocs.io/](https://dune-pdelab-course.readthedocs.io/)
