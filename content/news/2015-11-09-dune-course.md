+++
date = "2015-11-09T15:23:07+01:00"
title = "DUNE/PDELab Course (March 7-11, 2016)"
+++

This one-week course will provide an introduction to the most important DUNE modules and especially to DUNE-PDELab. At the end the attendees will have a solid knowledge of the simulation workflow from mesh generation and implementation of finite element and finite volume methods to visualization of the results. Topics covered are the solution of stationary and time-dependent problems, as well as local adaptivity, the use of parallel computers and the solution of non-linear PDE and systems of PDEs.

Registration deadline: Friday, February 15, 2016 Dates: March 7, 2016 - March 11, 2016

_Course venue_: Interdisciplinary Center for Scientific Computing
University of Heidelberg, Im Neuenheimer Feld 368
69120 Heidelberg, Germany

Fee: The fee for this course is 225 EUR including course material, coffee and lunch breaks as well as a course dinner.

_For registration and further information see_ http://conan.iwr.uni-heidelberg.de/dune-workshop/.
