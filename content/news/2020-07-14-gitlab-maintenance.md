+++
date = "2020-07-14"
title = "Gitlab Maintenance on 27th/28th July and Registry Clean Up"
+++

On the 27th and 28th of July we will have gitlab downtime due to maintenance
reason. This means you won't be able to reach

https://gitlab.dune-project.org/

during this time. Maintenance work will start on monday the 27th of July at 9am
CEST (=UTC+2).

During this downtime we will clean up our registry. This means you should make
sure that you can create all the images you have stored in the registry on your
local machine. After the clean up you will be able to push it back into the new
registry.