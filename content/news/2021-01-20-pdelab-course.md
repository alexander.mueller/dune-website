+++
date="2021-01-20"
title = "Virtual DUNE/PDELab Course (March 8 - March 12, 2021)"
tags = [ "events", "core", "dune-pdelab" ]
+++

The annual DUNE/PDELab course typically hosted by the Interdisciplinary Center for Scientific Computing
in Heidelberg will be held virtually this year. It is scheduled for March 8 - March 12, 2021.

This one week course provides an introduction to the most important DUNE modules and especially to
DUNE-PDELab. At the end the attendees will have a solid knowledge of the simulation workflow from
mesh generation and implementation of finite element and finite volume methods to visualization of the
results. Topics covered are the solution of stationary and time-dependent problems, as well as local
adaptivity, the use of parallel computers and the solution of non-linear PDEs and systems of PDEs.

#### Dates

March 8 - March 12, 2020

#### Application Deadline

The application deadline is February 21, 2021.
For further information, see the [course homepage](https://conan.iwr.uni-heidelberg.de/events/dune-course_2021/).
