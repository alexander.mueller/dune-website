+++
date = "2021-07-30"
title = "DUNE 2.8.0rc1 ready for testing"
tags = [ "releases", "core" ]
+++

The first release candidate for the upcoming 2.8.0 release is now
available.  You can download the tarballs (
[dune-common](https://dune-project.org/download/2.8.0rc1/dune-common-2.8.0rc1.tar.gz),
[dune-geometry](https://dune-project.org/download/2.8.0rc1/dune-geometry-2.8.0rc1.tar.gz),
[dune-grid](https://dune-project.org/download/2.8.0rc1/dune-grid-2.8.0rc1.tar.gz),
[dune-localfunctions](https://dune-project.org/download/2.8.0rc1/dune-localfunctions-2.8.0rc1.tar.gz),
[dune-istl](https://dune-project.org/download/2.8.0rc1/dune-istl-2.8.0rc1.tar.gz)
) or checkout the `v2.8.0rc1` tag via Git.

With the <span style="font-variant: small-caps">Dune</span> core
modules, the staging modules
[dune-uggrid](https://dune-project.org/download/2.8.0rc1/dune-uggrid-2.8.0rc1.tar.gz),
[dune-typetree](https://dune-project.org/download/2.8.0rc1/dune-typetree-2.8.0rc1.tar.gz), and
[dune-functions](https://dune-project.org/download/2.8.0rc1/dune-functions-2.8.0rc1.tar.gz)
are released.

Please go and test, and report any problems that you encounter.
