+++
date = "2011-02-10"
title = "Reminder: DUNE Spring Course, March 21-25 2011"
+++

IWR, University Heidelberg, Germany

For registration and further information see <http://conan.iwr.uni-heidelberg.de/dune-workshop/index.html>

Note: Registration deadline is Sunday, February 27<sup>th</sup> 2011.

This one week course will provide an introduction to the most important DUNE modules. At the end the attendees will have a solid knowledge of the simulation workflow from mesh generation and implementation of finite element and finite volume methods to visualization of the results. Successful participation requires knowledge of object-oriented programming using C++ including generic programming with templates (this knowledge will be brushed up on the first day of the course). A solid background on numerical methods for the solution of PDEs is expected.
