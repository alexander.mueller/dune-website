+++
date = "2012-07-30"
title = "PDELab 1.0.1 Released"
+++

The PDELab developers are proud to announce the first public release 1.0.1 of PDELab. Don't be fooled by the point release, this version is really the first public version and is identical to the never-released 1.0, it only differs by some fixes to the release package of the dune-pdelab-howto module. PDELab 1.0.1 is designed to work with the 2.2 release of the Dune core modules.

PDELab is a discretisation module built on top of the foundation provided by the Dune core modules and provides a powerful toolbox for the numerical solution of Partial Differential Equations using Finite Element Methods, Finite Volume Methods, and Discontinuous Galerkin Methods.

For further information and downloads see the [PDELab homepage](/modules/dune-pdelab/).
