+++
date = "2012-06-22"
title = "dune@dune-project.org has been Split by Scope"
+++

Our mailing list was getting more and more clogged. To make it easier for people to subscribe to only the mails they want we have split it into three lists: `dune@dune-project.org`, `dune-devel@dune-project.org`, and `dune-bugs@dune-project.org`. For the scope of each list and posting policy please see the [mailing lists page](http://www.dune-project.org/mailinglists.html). Subscription on all three lists is open for everyone.

Members of the old list have been ported over to all three new lists so they don't miss anything they were previously subscribed to.
