+++
date = "2009-04-02"
title = "Dune 1.2 Released"
+++

The new version 1.2 of Dune has been released and is available for [download](/releases/). Major features of the new release are a better build system, cleaner interfaces, and a new prototype grid to ease writing your own grid implementations. Have a look at the [release notes](/releases/1.2.0/) for details. Enjoy!
