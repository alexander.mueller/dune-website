+++
date = "2010-11-25"
title = "UG-3.9.1-patch4 Patch File Released"
+++

A new patch file for the UG grid manager has been released, which contains a fix for a bug in the MPI test.
