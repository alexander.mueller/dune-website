+++
date = "2013-05-06"
title = "Dune repositories migrated to Git"
+++

The Dune core modules have been migrated from Subversion to Git. If you have been following the development of Dune on the SVN trunk, you will need to replace your Subversion checkouts with clones of the Git repositories. You can find information on how to do so on the new [Git download page]($(ROOT)/downloadgit.html). While the SVN repositories will not be removed in the near future, they are frozen and will not be updated anymore. On our wiki you can find a description of the [repository layout](/doc/guides/repository_layout) ~~and a collection of guides to using Git with our repositories~~.

Git will make it a lot easier for users to contribute patches to Dune. We have started to [compile some information](/dev/contributing_patches) on the process to make sure your patches go into Dune with a minimum amount of hassle for both patch submitters and Dune developers.

Happy committing, branching, rebasing, merging, bisecting and whatever other crazy things you might want to do in Git!

*Update: Thanks to Steffen, the [dune-grid-glue](/modules/dune-grid-glue) repository has also moved to git.*

*Update: Some Wiki pages are obsolete now.*
