+++
date = "2008-07-29"
title = "New ALUGrid Version"
+++

ALUGrid version 1.11 has been released. We found a bug in the border-border communication. See the ChangeLog file for more details. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
