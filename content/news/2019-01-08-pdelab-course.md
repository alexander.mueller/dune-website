+++
date="2019-01-08"
title = "DUNE/PDELab Course at Heidelberg University (March 4 - March 8, 2019)"
tags = [ "events", "core", "dune-pdelab" ]
+++

The Interdisciplinary Center for Scientific Computing at Heidelberg University will host its annual
DUNE and PDELab course on March 4 - March 8, 2019.

This one week course provides an introduction to the most important DUNE modules and especially to
DUNE-PDELab. At the end the attendees will have a solid knowledge of the simulation workflow from
mesh generation and implementation of finite element and finite volume methods to visualization of the
results. Topics covered are the solution of stationary and time-dependent problems, as well as local
adaptivity, the use of parallel computers and the solution of non-linear PDEs and systems of PDEs.

#### Dates

March 4, 2019 - March 8, 2019

#### Registration Deadline

Friday, February 22, 2019

For further information, see the [course homepage](https://conan.iwr.uni-heidelberg.de/events/dune-course_2019/).
