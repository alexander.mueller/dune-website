+++
date = "2023-08-04T11:14:00+01:00"
title = " Dune User Meeting Sept. 18-19, 2023"
tags = [ "events", "core", "users" ]
+++

We are organizing the 7th DUNE user meeting at the Technical University
Dresden September 18-19, 2023. Directly afterwards the developer
meeting will take place September 19-20, 2023.

This meeting offers a chance to showcase how you are using Dune,
foster collaborations between the users and provide input to the future development of
Dune. The meeting also welcomes contributions from other frameworks such as
[DuMuX](https://dumux.org), [AMDiS](https://amdis.readthedocs.io) and [OPM](https://opm-project.org/)
that are using Dune.

To help the organizers please choose to register soon on the
[user meeting page](/community/meetings/2023-09-usermeeting/) and the
[developer meeting page](community/meetings/2023-09-devmeeting). You
will also find more information about the meetings there.

Please note that the official registration deadline is August
19, 2023. 
