+++
date = "2020-12-21"
title = "Install Dune modules from PyPI"
tags = [ "users", "core" ]
+++

Dune modules can now be installed via `pip` using the Python package
index (PyPI). The module and the modules it depends on are downloaded, build, and
installed either into the system or into a python virtual environment (the latter
might be of more interest as it is an option to install Dune without root privileges).
This could also be of interest for C++ users of Dune as Python bindings are not mandatory.
Scripts like `dunecontrol` can be used the same way as with a system installation so that
installed and source modules can easily be used together.
More details on how to add this installation option to existing Dune modules is
available [here][]. Details will be added to the `Documentation` shortly.

  [here]: /doc/installation-pip
