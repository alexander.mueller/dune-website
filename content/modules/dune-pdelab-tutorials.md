+++
title = "Tutorial for DUNE/PDELab"
group = ["tutorial"]
module = "dune-pdelab-tutorials"
requires = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-typetree","dune-pdelab"]
short = "Tutorial for PDELab providing a range of examples"
git = "http://gitlab.dune-project.org/pdelab/dune-pdelab-tutorials"
+++

This module shows by way of examples that how to use
[dune-PDELab](/modules/dune-pdelab) to actually solve PDEs. This
tutorial is also used in the Dune Spring School held every year in
march in Heidelberg.

[Go here for installation instructions for dune-pdelab and dune-pdelab-tutorials](/doc/gettingstarted/beginners-resources-pdelab).

The PDELab tutorial consists of examples numbered tutorial00,
tutorial01, .... Each tutorial provides exactly
one executable and contains the following subdirectories:

* `src` C++ source code of the example and corresponding executable in
  the build tree
* `doc` documentation in LaTeX source form, the pdf is found in the
  build tree
* `slides` optional set of slides if the example is used in the Dune course
* `exercise` optional set of exercises for the Dune course

This structure is meant to be extendable in the future. So feel free
to add more tutorials that you find useful!

Although each tutorial is self contained it makes sense to work
through them in a certain order, depending on what you want to use
PDELab for (see the list of tutorials below).
A good way to start your own code is then to copy the
tutorial which is "closest" to your application. Currently the
following tutorials are provided:

* `tutorial00` Piecewise linear finite elements for solving the
  Poisson equation with Dirichlet boundary conditions on simplicial
  meshes. Works in dimension 1, 2, 3.
* `tutorial01` Solves Poisson equation with a nonlinear reaction term
  and Dirichlet as well as Neumann type boundary
  conditions. Conforming "Lagrange" finite elements with selectable
  polynomial degree on simplicial and cuboid meshes in dimension 1, 2,
  3 can be used.
* `tutorial02` The same equation as in tutorial01 is solved with the
  cell-centered finite volume method using two-point flux
  approximation. This method is restricted to axi-parallel, cuboid meshes.
* `tutorial03` Solves instationary diffusion-reaction equation (with
  nonlinear reaction term) using a method of lines
  approach. Conforming finite elements of selectable order are used in
  space and diagonally implicit Runge-Kutta methods are used in time.
* `tutorial04` Solves the wave equation written as a first-order in
  time system, thereby extending the methods of tutorial03 to systems
  of partial differential equations.
* `tutorial05` Illustrates how to use adaptive mesh refinement in
  tutorial01.
* `tutorial06` Illustrates how to extend tutorial01 for parallel computation.
