+++
module = "dune-uggrid"
git = "https://gitlab.dune-project.org/staging/dune-uggrid"
group = ["grid"]
maintainers = "Oliver Sander <oliver.sander@tu-dresden.de>, Ansgar Burchardt <ansgar.burchardt@tu-dresden.de>"
requires = ["dune-common"]
short = "This is a fork of the old [UG](https://doi.org/10.1007/s007910050003) finite element software, wrapped as a Dune module, and stripped of everything but the grid data structure. You need this module if you want to use the UGGrid grid implementation from dune-grid."
weight = -5
+++

### Download

<table>
<tr>
  <th>version</th>
  <th>source</th>
  <th>signature</th>
</tr>
<tr>
  <td>2.7.1</td>
  <td><a href="/download/2.7.1/dune-uggrid-2.7.1.tar.gz" download>dune-uggrid-2.7.1.tar.gz</a></td>
  <td><a href="/download/2.7.1/dune-uggrid-2.7.1.tar.gz.asc" download>dune-uggrid-2.7.1.tar.gz.asc</a></td>
</tr>
<tr>
  <td>2.6.0</td>
  <td><a href="/download/2.6.0/dune-uggrid-2.6.0.tar.gz" download>dune-uggrid-2.6.0.tar.gz</a></td>
  <td><a href="/download/2.6.0/dune-uggrid-2.6.0.tar.gz.asc" download>dune-uggrid-2.6.0.tar.gz.asc</a></td>
</tr>
<tr>
  <td>2.5.1</td>
  <td><a href="/download/2.5.1/dune-uggrid-2.5.1.tar.gz" download>dune-uggrid-2.5.1.tar.gz</a></td>
  <td><a href="/download/2.5.1/dune-uggrid-2.5.1.tar.gz.asc" download>dune-uggrid-2.5.1.tar.gz.asc</a></td>
</tr>
<tr>
  <td>2.5.0</td>
  <td><a href="/download/2.5.0/dune-uggrid-2.5.0.tar.gz" download>dune-uggrid-2.5.0.tar.gz</a></td>
  <td><a href="/download/2.5.0/dune-uggrid-2.5.0.tar.gz.asc" download>dune-uggrid-2.5.0.tar.gz.asc</a></td>
</tr>
</table>

The current development version can be obtained via Git:
```shell
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git
```

_dune-uggrid_ is also available in [Debian](https://packages.debian.org/search?keywords=libdune-uggrid-dev&searchon=names&suite=all&section=all), and possibly in other Linux distributions.
