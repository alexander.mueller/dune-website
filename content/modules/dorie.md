+++
# The name of the module.
module = "DORiE"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups) as a list.
# Currently recognized groups: "core", "disc", "grid", "extension", "user", "tutorial"
group = ["user"]

# List of modules that this module requires
requires = ["dune-pdelab", "dune-randomfield"]

# List of modules that this module suggests
suggests = ["dune-testtools"]

# A string with maintainers to be shown in short description, if present.
#maintainers = "DORiE developers <dorieteam@iup.uni-heidelberg.de>"

# Main Git repository, uncomment if present
git = "https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie"

# Python bindings, uncomment if available and add (yes/partially/...)
#pythonbindings = ""

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "Software package for solving transient water flow and passive solute transport in unsaturated porous media with finite volume (FV) and discontinuous Galerkin (DG) methods"

# Please add as many information as you want in markdown format directly below this frontmatter.
+++

### Overview

DORiE is a software package for solving the Richards equation coupled with the
passive transport equation powered by [DUNE PDELab](/modules/dune-pdelab).
It implements cell-centered finite volume (FV) and symmetric weighted
interior penalty discontinuous Galerkin (SWIP-DG) discretization schemes.
The latter can be applied on unstructured grids imported by GMSH and make use
of adaptive local grid refinement (h-adaptivity) based on a water flux error
estimator. DORiE also integrates
[dune-randomfield](https://gitlab.dune-project.org/oklein/dune-randomfield) for
creating heterogeneous soil architectures.

The DORiE application is controlled via a command line interface and
input configuration files. As DUNE module, its code can be integrated into
encapsulating modules.

DORiE can be compiled from source and is available as Docker Image from
[Docker Hub][dockerhub].

### Availability

* [Publication in the Journal of Open Source Software (JOSS)](https://doi.org/10.21105/joss.02313)
* [Software repository](https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie)
* [Development version user manual](https://hermes.iup.uni-heidelberg.de/dorie_doc/master/html/)
* [Stable releases](https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/-/releases)
* [Docker Hub page][dockerhub]
* [Webpage](http://ts.iup.uni-heidelberg.de/research/terrestrial-systems/dorie/)

[dockerhub]: https://hub.docker.com/r/dorie/dorie
