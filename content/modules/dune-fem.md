+++
module = "dune-fem"
group = ["disc"]
requires = ["dune-common", "dune-geometry","dune-grid"]
suggests = ["dune-alugrid", "dune-istl", "dune-localfunctions", "dune-spgrid"]
maintainers = "[The dune-fem team](/modules/dune-fem#Team)"
title = "dune-fem"
git = "https://gitlab.dune-project.org/dune-fem/dune-fem"
pythonbindings = "yes"
short = "A discretization module providing an implementation of mathematical abstractions to solve PDEs on parallel computers including local grid adaptivity, dynamic load balancing, and higher order discretization schemes."
tutorial = "[DUNE-FEM tutorial](/sphinx/content/sphinx/dune-fem)"

doxygen_url = ["/doxygen/dune-fem/master"]
doxygen_modules = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-fem"]
doxygen_branch = ["master"]
doxygen_name = ["DUNE-FEM"]
doxygen_version = ["unstable"]
+++

### General

DUNE-FEM is a discretization module based on DUNE containing all the
building blocks required to implement efficient solvers for a wide range of
(systems of non linear) partial differential equations. DUNE-FEM can also
be used through an extensive Python interface which brings all components of
DUNE-FEM and the DUNE core modules to Python.

In addition to [pip install dune-fem](https://dune-project.org/sphinx/content/sphinx/dune-fem/installation.html)
one can build DUNE-FEM [from source](https://dune-project.org/sphinx/content/sphinx/dune-fem/installation.html#from-source).

DUNE-FEM provides interfaces for:

- Discrete function spaces and functions
- Linear operators and inverse linear operators
- Methods for solving non-linear equations and time dependent problems
- MPI and multithread based parallelization
- Additional grid views e.g. for removing parts of the grid (filtering) or replacing geometry information

In addition DUNE-FEM provides many auxiliary classes for

+ Flexible parameter input
+ Data output including checkpointing
+ Computation of eoc tables, timers for subroutines, and much more...

Efficient strategies for parallelization, adaptivity, and load balancing is a central aspect in the construction of DUNE-FEM. Both are handled by central classes and require very little input from the user. To increase efficiency we try to use caching as much as possible, e.g., for communication patterns in parallel computations and of base function sets in quadrature points on the reference elements. All this is done transparently from the user but can be turned off anywhere in the code.

To measure efficiency we have used a Discontinuous Galerkin method for the compressible Navier-Stokes equation to compute both parallel scale up and FLOP performance. The code showed a parallel efficiency close to one up to 16K processors and we measured 5 GFLOPs on an Intel Core i7 QM 720 @ 1.6 GHz which is about 41 percent of the FLOPs we measured for LINPACK on the same machine.

### Main Features

DUNE-FEM does not only provide interfaces but also comes with realizations of these interfaces:

+ Discrete function spaces:
  - Arbitrary order spaces with Lagrange basis functions and orthonormal basis functions
  - Arbitrary order Lagrange spaces
  - p-adaptive DG and Lagrange spaces
  - spaces for reduced basis methods
+ Discrete function implementations:
  - Discrete function spaces especially build to allow for very efficient grid refinement and coarsening
  - Discrete function based on the block vectors provided by dune-istl
  - Different approaches to extend scalar discrete function spaces to product spaces
+ Inverse linear operators:
  - DUNE-FEM provides some build in iterative solvers, e.g., cg, gmres, and bicgstab
  - There are bindings for dune-istl, UMFPACK, and PETSc
+ Time step methods:
  - SSP Runge-Kutta methods (explicit, implicit, and IMEX)
  - Multistep methods

### Applications

DUNE-FEM has been used for a wide range of applications:
+ Higher order methods for conservation laws (both finite-volume and discontinuous Galerkin)
+ Convection dominated convection-diffusion equations (mostly Discontinuous Galerkin)
+ Higher order methods for parabolic and elliptic problems (both conforming and Discontinuous Galerkin)
+ Hamilton Jacobi equations (different methods used)
+ Two-phase flow in porous media (finite-volume and Discontinuous Galerkin)
+ Reduced basis methods

Most methods have been tested for both problems formulated in Euclidean space and on surfaces including moving surfaces and also moving domains.

Implementations for a wide range of different
[Discontinuous Galerkin](https://gitlab.dune-project.org/dune-fem/dune-fem-dg) and based on the [Virtual Element](https://gitlab.dune-project.org/dune-fem/dune-vem) method are
available in additional modules.


### Documentation and Tutorial

The [DUNE-FEM tutorial](/sphinx/content/sphinx/dune-fem) contains a step by step
introduction to DUNE-FEM and the Python front end.
In addition to introducing all the basic concepts of DUNE,
the tutorial contains a number of individual Python script/Jupyter notebooks showing how to solve different types of equations which provide a good starting point for projects.

For the underlying C++ code there is also a [documentation of DUNE-FEM master](/doxygen/dune-fem/master) is generated using Doxygen.

There is one main publication describing some of the underlying principals and giving some examples and we ask all users of DUNE-FEM to cite in publications using DUNE-FEM:

[A. Dedner, R. Klöfkorn, M. Nolte, M. Ohlberger: "A generic interface for parallel and adaptive scientific computing: Abstraction principles and the DUNE-FEM module". Computing Vol. 90, No. 3, pp. 165--196, 2011 November 2010.](http://link.springer.com/article/10.1007/s00607-010-0110-3)

### Download

The full repository is hosted on our [GitLab instance](https://gitlab.dune-project.org/dune-fem/dune-fem).
In order to use a stable release, please check out the corresponding branch.

Please also read the [tutorial](/sphinx/content/sphinx/dune-fem) for installation instructions.

### Known issues

All the known issues of DUNE-FEM can be found on the [online bug tracker](https://gitlab.dune-project.org/dune-fem/dune-fem/issues). Please use it to report any bug.

### Support

DUNE-FEM provides its own [user mailing lists](https://lists.dune-project.org/mailman/listinfo/dune-fem) and a [developer mailing lists](https://listserv.uni-stuttgart.de/mailman/listinfo/dune-fem-devel).

### <a name="Team"></a>Team

* [Andreas Dedner][4a] ([Warwick Mathematics Institute, University of Warwick, UK][4b])
* [Robert Klöfkorn][11a] ([Lund University, Sweden][11b])
* [Samuel Burbulla][16d] ([Institut für Angewandte Analysis und Numerische Simulation, Universität Stuttgart][16c])

### Former Contributors

* [Christoph Gersbacher][7a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][7b])
* [Martin Nolte][13a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][13b])
* [Martin Alkämper][16a] ([Institut für Angewandte Analysis und Numerische Simulation, Universität Stuttgart][16c])
* [Claus-Justus Heine][16b] ([Institut für Angewandte Analysis und Numerische Simulation, Universität Stuttgart][16c])
* [Tobias Malkmus][13a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][13b])
* [Janick Gerstenberger][18a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][13b])
* [Marco Agnese][20a] ([Applied Mathematics and Mathematical Physics, Imperial College London][20b])
* [Stefan Wierling][19a] ([Institut für Angewandte Mathematik, Universität Münster][19b])


[4a]: http://www2.warwick.ac.uk/fac/sci/maths/people/staff/andreas_dedner/
[4b]: http://www2.warwick.ac.uk/fac/sci/maths/
[7a]: http://aam.uni-freiburg.de/abtlg/wissmit/agkr/gersbacher/
[7b]: http://www.mathematik.uni-freiburg.de/IAM/
[11a]: https://www.maths.lu.se/staff/robert-kloefkorn/
[11b]: https://www.maths.lu.se
[12a]: http://conan.iwr.uni-heidelberg.de/
[12b]: http://conan.iwr.uni-heidelberg.de/
[13a]: https://aam.uni-freiburg.de/mitarb/nolte/
[13b]: https://aam.uni-freiburg.de/
[16a]: http://www.ians.uni-stuttgart.de/nmh/alkaemper
[16b]: http://www.ians.uni-stuttgart.de/nmh/heine
[16c]: http://www.ians.uni-stuttgart.de/
[16d]: https://www.ians.uni-stuttgart.de/institute/team/Burbulla/
[18a]: https://aam.uni-freiburg.de/mitarb/gerstenberger
[19a]: https://www.uni-muenster.de/AMM/ohlberger/team/stefan_wierling.shtml
[19b]: https://www.uni-muenster.de/AMM/ohlberger
[20a]: https://www.imperial.ac.uk/people/m.agnese13
[20b]: http://www.imperial.ac.uk/ammp
