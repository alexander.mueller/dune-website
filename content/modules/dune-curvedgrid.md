+++
module = "dune-curvedgrid"
group = ["grid"]
requires = ["dune-grid", "dune-curvedgeometry"]
suggests = ["dune-localfunctions", "dune-functions", "dune-foamgrid", "dune-alugrid"]
maintainers = "[Simon Praetorius](mailto:simon.praetorius@tu-dresden.de), [Florian Stenger](florian.stenger@tu-dresden.de)"
git = "https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgrid"
short = "A meta-grid for dune grids mapping flat geometries to curved geometries"
+++
# Dune-CurvedGrid - A Dune module for geometry parametrization
This Dune module provides a meta-grid for wrapping other grid implementations and providing
a curved geometry on each element, parametrized by a Dune-CurvedGeometry
class.

## Example of usage
```
// 1. Construct a reference grid
auto refGrid = GmshReader< FoamGrid<2,3> >::read("sphere.msh");

// 2. Define the geometry mapping
auto sphere = [](const auto& x) { return x / x.two_norm(); };
auto sphereGF = analyticDiscreteFunction(sphere, *refGrid, order);

// 3. Wrap the reference grid to build a curved grid
CurvedGrid grid{*refGrid, sphereGF};
```
At first, we have to create a reference grid that acts as a parametrization domain for the
target curved grid. In the example, we use `Dune-FoamGrid` and read a piece-wise flat grid
from a GMsh file `sphere.msh`.

In a second step, we describe the parametrization of the curved surface/domain by a
closest-point projection to the sphere. This projection is given by a callable `sphere`
and is wrapped into a grid-function by a call to `analyticDiscreteFunction`. This
grid-function wrapper associates the lambda-expression with a grid and a polynomial order
for the local interpolation of the projection into a Lagrange basis. This interpolation
allows to obtain values and derivatives of the geometry mapping.

Finally, the reference grid and the parametrization together build the curved grid. This
meta-grid `CurvedGrid` implements the Dune grid interface and can thus be used as
replacement for, e.g., the reference grid.

## Further reading
The description of the module for surface parametrizations is written in the paper

[S. Praetorius, F. Stenger: "Dune-CurvedGrid - A Dune module for surface parametrization". Arch Num Soft, 6(1), 2022](https://doi.org/10.11588/ans.2022.1.75917), [arXiv](https://arxiv.org/abs/2009.04938)

and an illustration of its functionality is given in the presentation

[S. Praetorius: "Curved Grid, A Dune module for surface parametrization". 2020](https://wwwpub.zih.tu-dresden.de/~praetori/presentations/dune-curvedgrid)
