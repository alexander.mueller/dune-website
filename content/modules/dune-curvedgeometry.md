+++
module = "dune-curvedgeometry"
group = ["grid"]
requires = ["dune-geometry"]
suggests = ["dune-localfunctions", "dune-grid", "dune-vtk"]
maintainers = "[Simon Praetorius](mailto:simon.praetorius@tu-dresden.de)"
git = "https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgeometry"
short = "Implementation of curved dune geometries"
+++
# Dune-CurvedGeometry
This module provides three implementations of curved geometries, `CurvedGeometry`,
`ParametrizedGeometry`, and `LocalFunctionGeometry` either parametrized with a local
finite-element cache, the local finite-element directly, or a local-function following the
dune-functions interface.

## Example of usage
Parametrization of the unit-sphere surface grid with elements of geometry order 3.
```
#include <dune/curvedgeometry/parametrizedgeometry.hh>
#include <dune/localfunctions/lagrange.hh>

int main() {
  // construct a reference grid
  Grid grid{...};

  for (const auto& e : elements(grid.leafGridView()))
  {
    // sphere projection from local to global coordinates
    auto X = [geo=e.geometry()](const auto& local)
    {
      auto x = geo.global(local);
      return x / x.two_norm();
    };

    // create a local finite-element
    using LFE = LagrangeLocalFiniteElement<EquidistantPointSet, Grid::dimension, double, double>;
    LFE localFE{e.type(), /* order= */ 3};

    // create a curved geometry
    ParametrizedGeometry geometry{referenceElement(e), localFE, X};
  }
}
```
