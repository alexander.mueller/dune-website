+++
module = "dune-multidomaingrid"
git = "https://gitlab.dune-project.org/extensions/dune-multidomaingrid"
group = ["grid"]
maintainers = "Santiago Ospina <santiago.ospina@iwr.uni-heidelberg.de>"
requires = ["dune-common","dune-grid"]
short = "MultiDomainGrid is a meta grid that allows you to carve up an existing DUNE grid into subdomains and extract interfaces for e.g. multi-physics simulations with multiple domains."
+++

### Download

<table>
<tr>
  <th>Version</th>
  <th>Source</th>
  <th>Signature</th>
</tr>
<tr>
  <td>2.6.0</td>
  <td><a href="/download/2.6.0/dune-multidomaingrid-2.6.0.tar.gz" download>dune-multidomaingrid-2.6.0.tar.gz</a></td>
  <td><a href="/download/2.6.0/dune-multidomaingrid-2.6.0.tar.gz.asc" download>dune-multidomaingrid-2.6.0.tar.gz.asc</a></td>
</tr>
</table>

The current development version can be obtained via Git:
```shell
git clone https://gitlab.dune-project.org/extensions/dune-multidomaingrid.git
```
