+++
group = ["extension"]
module = "dune-tpmc"
requires = ["dune-common"]
short = "Provides a topology preserving implementation of the marching cubes and marching simplex algorithm. This module can be used to implement cut-cell algorithms on top of the Dune interface."
maintainers = "[Christian Engwer](mailto:christi@dune-project.org), [Andreas Nüßing](mailto:andreas.nuessing@uni-muenster.de)"
git = "https://gitlab.dune-project.org/extensions/dune-tpmc"
title = "dune-tpmc"
+++

<figure class="figure center-block img-thumbnail" style="margin:30px">
<img class="figure-img img-fluid img-rounded" src="/img/test_levelset.png" style="width:200px;height:auto"></td>
<img class="figure-img img-fluid img-rounded" src="/img/test_lewiner.png" style="width:200px;height:auto"></td>
<img class="figure-img img-fluid img-rounded" src="/img/test_our.png" style="width:200px;height:auto"></td>
<figcaption class="figure-caption text-xs-right">Exact level-set, reconstruction using the standard MC33 algorithm and reconstruction with the Topology Preserving Marching Cubes.</figcaption>
</figure>

Given a scalar P1/Q1 function Φ on a domain Ω, we define a partition of Ω into two sub-domains {Ω1, Ω2} with a common interface Γ. The interface is given as the zero level-set of the scalar function Φ. The marching cubes algorithm computes a piece-wise linear reconstruction of Γ.

The _dune-tpmc_ modules not only computes a reconstruction of the interface, but also further information. The user can access the following information:

* Reconstruction of the interface Γ
* Reconstruction of Ω1
* Reconstruction of Ω2
* Connectivity pattern w.r.t. each sub-domain

In order to allow the algorithm to be employed in Finite Element simulations, a reconstruction has to fulfill certain topological guarantees:

* The connectivity pattern of the cell vertices must be preserved within each subentity. In particular this means that vertices connected along an edge, face or volume, should still be connected via the same subentity.
* The exact interface Γ partitions each grid cell into patches belonging to either Ω1 or Ω2. We require that number of patches and their domain association is the same in the polygonal reconstruction.
* The vertices of the reconstructed interface lie on the exact zero level-set.

### Publications and Documentation

If you are using the _dune-tpmc_ module, please cite

<!-- * {{% doi doi="10.1145/3104989" %}} (arXiv preprint [arXiv:1601.03597](https://arxiv.org/abs/1601.03597)) -->

The first concepts of _dune-tpmc_ go back to implementations described in

<!-- * {{% doi doi="10.1002/nme.2631" %}} -->
* Ch. Engwer, An Unfitted Discontinuous Galerkin Scheme for Micro-scale Simulations and Numerical Upscaling, Heidelberg University, 2009. ([PDF](http://www.ub.uni-heidelberg.de/archiv/9990))

### Installation

_dune-tpmc_ depends on the [`TPMC`](https://github.com/tpmc/tpmc) library for generating of the lookup-tables.

You can install `TPMC` via
[`pip`](https://pypi.org/project/pip/). Currently it is not listed in
the `pypi` repository and must be installed directly from the github repository:

* for python 2:
```
> python -m pip install git+https://github.com/tpmc/tpmc.git
```
* for python 3 we have preliminary support:
```
> python -m pip install git+https://github.com/tpmc/tpmc.git@python3
```

### Maintainers

dune-tpmc has been written by [Christian Engwer](https://www.uni-muenster.de/AMM/engwer/team/engwer.shtml) and [Andreas Nüßing](https://www.uni-muenster.de/AMM/engwer/team/alumni.shtml).
