+++
# The name of the module.
module = "ikarus"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "extension", "tutorial"
group = ["extension"]

# List of modules that this module requires
requires = ["dune-common", "dune-grid", "dune-localfunctions", "dune-istl", "dune-geometry", " dune-functions"]

# List of modules that this module suggests
suggests = ["dune-uggrid", "dune-iga",  "dune-alugrid", "dune-foamgrid", "dune-vtk", "dune-fufem"]

# A string with maintainers to be shown in short description, if present.
maintainers = "Alexander Müller"
title = "ikarus"

# Main Git repository, uncomment if present
git = "https://github.com/ikarus-project/ikarus"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "Ikarus is a module for simulations in the context of computational mechanics"
pythonbindings = "yes"
+++

This project tries to provide an easy-to-read and an easy-to-use finite element framework.
Our usage in the context of computational mechanics. 

More information on Ikarus can be found on the [Ikarus website](https://ikarus-project.github.io/).
