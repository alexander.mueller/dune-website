+++
# The name of the module.
module = "dune-mmesh"

# The title of the module.
title = "dune-mmesh"

# Groups that this module belongs to
group = ["grid"]

# List of modules that this module requires
requires = ["dune-common", "dune-geometry", "dune-grid"]

# List of modules that this module suggests
suggests = ["dune-python"]

# A string with maintainers
maintainers = "[Samuel Burbulla](mailto:samuel.burbulla@mathematik.uni-stuttgart.de)"

# Main Git repository
git = "https://gitlab.dune-project.org/samuel.burbulla/dune-mmesh"

# Short description
short = "MMesh is a grid implementation based on CGAL triangulations."

# Doxygen documentation
doxygen_url = ["/doxygen/dune-mmesh/master", "/doxygen/dune-mmesh/release-1.2"]
doxygen_branch = ["master", "release/1.2"]
doxygen_name = ["dune-mmesh", "dune-mmesh"]
doxygen_version = ["unstable", "1.2"]
weight = -1
+++

The dune-mmesh module is a wrapper of CGAL triangulations in 2d and 3d.

Additionally, the implementation is able to export a predefined set of facets as a surface ('interface') grid and provides remeshing utilities for moving this interface.


License
-------

The dune-mmesh library, headers and test programs are free open-source software,
licensed under version 3 or later of the GNU General Public License.
