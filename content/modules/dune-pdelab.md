+++
title = "dune-pdelab"
group = ["disc"]
module = "dune-pdelab"
requires = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-typetree", "dune-functions"]
short = "A generalized discretization module for a wide range of discretization methods. It allows rapid prototyping for implementing discretizations and solvers for systems of PDEs based on DUNE."
suggests = ["dune-alugrid", "dune-multidomaingrid"]

git = "https://gitlab.dune-project.org/pdelab/dune-pdelab"
tutorial = "[PDELab beginners guide](/doc/beginners-resources-pdelab), [PDELab Tutorial](/modules/dune-pdelab-tutorials)"

# Trigger the generation of doxygen documentation
doxygen_url = ["/doxygen/pdelab/releases2.7", "/doxygen/pdelab/releases2.8", "/doxygen/pdelab/master"]
doxygen_modules = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-typetree", "dune-pdelab", "dune-functions"]
doxygen_branch = ["releases/2.7", "releases/2.8", "master"]
doxygen_name = ["DUNE PDELab", "DUNE PDELab", "DUNE PDELab"]
doxygen_version = ["2.7", "2.8", "git"]
# PDELab now has a custom main page to guide users to the recipes
doxygen_mainpage = ["mainpage.txt", "mainpage.txt", "mainpage.txt"]
+++

### Beginners Resources

[Go here for installation instructions for beginners](/doc/beginners-resources-pdelab).

[Go here for the PDELab Tutorial](/modules/dune-pdelab-tutorials).

### General
<mark>dune-pdelab</mark> is a discretization module based on [DUNE](/about/home) with the following aims:

* Rapid prototyping: Substantially reduce time to implement discretizations and solvers for systems of PDEs based on DUNE
* Simple things should be simple: Suitable for teaching
* Use of different linear solvers: Exchangeable linear algebra backend

Its main abstractions are:

* Flexible discrete function spaces:
  - Conforming and non-conforming
  - hp-refinement
  - general approach to constraints
  - Product spaces for systems
  - Adaptivity and parallel data decomposition
* Operators based on weighted residual formulation:
  - Linear and nonlinear
  - stationary and transient
  - FE and FV schemes requiring at most face-neighbors

The following applications have been realized using PDELab

1. Various elliptic, parabolic and hyperbolic modelproblems.
* Higher-order conforming and discontinuous Galerkin finite element methods.
* Cell-centered finite volume method.
* Mixed finite element method.
* Mimetic finite difference method.
* Incompressible Navier-Stokes equations.
* Two-phase flow.
* Multi-phase flow in porous media.
* Linear acoustics.
* Maxwell's equations.
* [DuMu<sup>x</sup>](http://www.dumux.org/) is a simulator for flow and transport in porous media based on DUNE and PDELab.

---
### Tutorials
The module [dune-pdelab-tutorials](https://dune-project.org/modules/dune-pdelab-tutorials/) contains code examples,
exercises and documentation to get you started with PDELab, split up into a number of separate tutorials. This module
also forms the basic of the yearly PDELab introduction course at Heidelberg University.

Older versions of PDELab instead provided a different module called *dune-pdelab-howto*, but this is not maintained any longer.

### Multiphysics
Currently an extension of PDELab is in development that allows to couple different models in different subdomains. It consist of the following two additional modules

* [dune-multidomaingrid](http://github.com/smuething/dune-multidomaingrid) provides a metagrid that allows to decompose a grid into several (possibly overlapping) subdomains. This module is independent of dune-pdelab.
* [dune-multidomain](http://github.com/smuething/dune-multidomain) extends dune-pdelab such that different models in the subdomains can be coupled.

### Documentation
* Access the online documentation of the 2.7.0 release ([click](https://dune-project.org/doxygen/pdelab/releases2.7/)).
* Access the online documentation of the 2.8.0 release ([click](https://dune-project.org/doxygen/pdelab/releases2.8/)).
* Access the online documentation of the development version ([click](https://dune-project.org/doxygen/pdelab/master/)).

### Bug Tracking System
The bug tracker for PDELab can be found on [Dune's GitLab server](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues).

### Mailing list
If you really cannot find your answer in either the Howto or the documentation, you might consult the [Dune PDELab mailing list](https://lists.dune-project.org/mailman/listinfo/dune-pdelab).

### Download

#### Release packages
If you are interested in developing your own applications on top of PDELab, the release packages provide you with a stable foundation that is not subject to the sudden changes that might happen in the development branch. Moreover, these packages contain all of the API documentation and the Howto documents, which is handy if you do not have Doxygen and/or Latex installed on your machine.

Starting from PDELab 2.0, we provide GPG signatures for the release packages. Those signatures have been created with subkeys of the following key:
<table summary="">
<tr>
<th>Module</th>
<th>File Name</th>
<th>Size</th>
<th>PGP Signature</th>
</tr>
<tr>
<td rowspan="3"style="text-align:center;"><b>dune-typetree</b></td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-typetree-2.3.1.tar.gz">dune-typetree-2.3.1.tar.gz</a></td>
<td>1.6 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-typetree-2.3.1.tar.gz.asc">dune-typetree-2.3.1.tar.gz.asc</a></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-typetree-2.3.1.tar.bz2">dune-typetree-2.3.1.tar.bz2</a></td>
<td>1.2 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-typetree-2.3.1.tar.bz2.asc">dune-typetree-2.3.1.tar.bz2.asc</a></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-typetree-2.3.1.tar.xz">dune-typetree-2.3.1.tar.xz</a></td>
<td>1.1 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-typetree-2.3.1.tar.xz.asc">dune-typetree-2.3.1.tar.xz.asc</a></td>
</tr>
<tr><td>&nbsp;</td></tr>

<!-- Empty line as separator -->
<tr>
<td rowspan="3"style="text-align:center;"><b>dune-pdelab</b></td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-2.0.0.tar.gz">dune-pdelab-2.0.0.tar.gz</a></td>
<td>58 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-2.0.0.tar.gz.asc">dune-pdelab-2.0.0.tar.gz.asc</a></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-2.0.0.tar.bz2">dune-pdelab-2.0.0.tar.bz2</a></td>
<td>49 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-2.0.0.tar.bz2.asc">dune-pdelab-2.0.0.tar.bz2.asc</a></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-2.0.0.tar.xz">dune-pdelab-2.0.0.tar.xz</a></td>
<td>34 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-2.0.0.tar.xz.asc">dune-pdelab-2.0.0.tar.xz.asc</a></td>
</tr>
<tr><td>&nbsp;</td></tr>

<!-- Empty line as separator -->
<tr>
<td rowspan="3" style="text-align:center;"><b>dune-pdelab-howto</b></td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-howto-2.0.0.tar.gz">dune-pdelab-howto-2.0.0.tar.gz</a></td>
<td>46 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-howto-2.0.0.tar.gz.asc">dune-pdelab-howto-2.0.0.tar.gz.asc</a></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-howto-2.0.0.tar.bz2">dune-pdelab-howto-2.0.0.tar.bz2</a></td>
<td>31 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-howto-2.0.0.tar.bz2.asc">dune-pdelab-howto-2.0.0.tar.bz2.asc</a></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-howto-2.0.0.tar.xz">dune-pdelab-howto-2.0.0.tar.xz</a></td>
<td>24 MB</td>
<td><a href="http://dune-project.org/download/pdelab/2.0/dune-pdelab-howto-2.0.0.tar.xz.asc">dune-pdelab-howto-2.0.0.tar.xz.asc</a></td>
</tr>
</table>

<h4><a name="ToC11">Version 1.1.0</a></h4>
<p>The release packages for PDELab 1.1.0 have been generated using the release packages of Dune 2.2.1.</p>
<table summary="">
<tr>
<th>Module</th>
<th>File Name</th>
<th>Size</th>
<th>SHA1</th>
</tr>
<tr>
<td rowspan="3"style="text-align:center;"><b>dune-pdelab</b></td>
<td><a href="http://dune-project.org/download/pdelab/1.1/dune-pdelab-1.1.0.tar.gz">dune-pdelab-1.1.0.tar.gz</a></td>
<td>17.7 MB</td>
<td><b>fb32f55a4538bc7a6ebca6668f272742da92cb44</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.1/dune-pdelab-1.1.0.tar.bz2">dune-pdelab-1.1.0.tar.bz2</a></td>
<td>15.2 MB</td>
<td><b>7ffea91e05932abab3d076e288805804c9290d4e</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.1/dune-pdelab-1.1.0.tar.xz">dune-pdelab-1.1.0.tar.xz</a></td>
<td>13.5 MB</td>
<td><b>9ebebf0eaa5ff9013799a03bf70c0db08f4f8122</b></td>
</tr>
<tr><td>&nbsp;</td></tr>

<!-- Empty line as separator -->
<tr>
<td rowspan="3" style="text-align:center;"><b>dune-pdelab-howto</b></td>
<td><a href="http://dune-project.org/download/pdelab/1.1/dune-pdelab-howto-1.1.0.tar.gz">dune-pdelab-howto-1.1.0.tar.gz</a></td>
<td>55.1 MB</td>
<td><b>cd90095de8f4cc9b169084a359a9ccb66c514869</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.1/dune-pdelab-howto-1.1.0.tar.bz2">dune-pdelab-howto-1.1.0.tar.bz2</a></td>
<td>35.0 MB</td>
<td><b>8efb399e85b637dc00ead1ad0c9c70d234ef5e91</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.1/dune-pdelab-howto-1.1.0.tar.xz">dune-pdelab-howto-1.1.0.tar.xz</a></td>
<td>26.1 MB</td>
<td><b>ca5caafc6c694fb6255cde3d22e9a0be468f3b04</b></td>
</tr>
</table>

<h4><a name="ToC12">Version 1.0.1</a></h4>
<p>The release packages for PDELab 1.0.1 have been generated using the release packages of Dune 2.2.0.</p>
<table summary="">
<tr>
<th>Module</th>
<th>File Name</th>
<th>Size</th>
<th>SHA1</th>
</tr>
<tr>
<td rowspan="3"style="text-align:center;"><b>dune-pdelab</b></td>
<td><a href="http://dune-project.org/download/pdelab/1.0.1/dune-pdelab-1.0.1.tar.gz">dune-pdelab-1.0.1.tar.gz</a></td>
<td>23.3 MB</td>
<td><b>075de112e9da69a726f1c052232dc3a9944b30fb</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.0.1/dune-pdelab-1.0.1.tar.bz2">dune-pdelab-1.0.1.tar.bz2</a></td>
<td>20.8 MB</td>
<td><b>99ef27877f34f9ca831d97ab8348b87d4b1bca78</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.0.1/dune-pdelab-1.0.1.tar.xz">dune-pdelab-1.0.1.tar.xz</a></td>
<td>17.5 MB</td>
<td><b>a7f2467025e34dcaab0522f9cdf45cbf3810d58f</b></td>
</tr>
<tr><td>&nbsp;</td></tr>

<!-- Empty line as separator -->
<tr>
<td rowspan="3" style="text-align:center;"><b>dune-pdelab-howto</b></td>
<td><a href="http://dune-project.org/download/pdelab/1.0.1/dune-pdelab-howto-1.0.1.tar.gz">dune-pdelab-howto-1.0.1.tar.gz</a></td>
<td>59.0 MB</td>
<td><b>be97168c375e5233287ebbcebec32b77539fb329</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.0.1/dune-pdelab-howto-1.0.1.tar.bz2">dune-pdelab-howto-1.0.1.tar.bz2</a></td>
<td>39.5 MB</td>
<td><b>bb26fb33e3b7ee5dfbc5e0c232a7185134c79c94</b></td>
</tr>
<tr>
<td><a href="http://dune-project.org/download/pdelab/1.0.1/dune-pdelab-howto-1.0.1.tar.xz">dune-pdelab-howto-1.0.1.tar.xz</a></td>
<td>30.1 MB</td>
<td><b>f53260e467ad3365fb835920d224c42776c6982e</b></td>
</tr>
</table>

### Git Access
If you want access to the latest and greatest features of PDELab or you are using the master of the Dune core modules, you can directly check out the development version via anonymous Git. Alternatively, you can check out a release branch (or an older snapshot branch) if you need compatibility with a specific Dune release, but still want access to some backported fixes that have not made it into release packages yet.

Please note that you need additional software to build the documentation from an Git checkout, and that the documentation is not built automatically. If you want to build it, try running make doc and install any missing software the build process complains about.

**Note:** As of version 2.0, PDELab now depends on [dune-typetree](https://gitlab.dune-project.org/staging/dune-typetree). Starting with this release, we are also providing source packages of dune-typetree. The versioning scheme of dune-typetree is synchronized with the core modules; the correct release version of the package will also be listed alongside the release packages of PDELab itself. If you are living on the bleeding edge, you will have to download the library from its Git repository with

`git clone https://gitlab.dune-project.org/staging/dune-typetree.git`

<dl>

<dt><b>dune-pdelab</b></dt>
<dd><b>git clone
  https://gitlab.dune-project.org/pdelab/dune-pdelab.git</b>
  <table style="font-size: smaller;" summary="">
  <tr>
      <th>PDELab Version</th><th>&nbsp;</th>
      <th>Compatible Dune / TypeTree Release</th>
      <th style="text-align:left; padding-left:10px;">Git branch</th>
  </tr>
  <tr>
      <td style="text-align:center;">master</td><td>&harr;</td>
      <td style="text-align:center;">master</td>
      <td style="padding-left:10px;">
    <b>master</b>
      </td>
  </tr>
  <tr>
    <td style="text-align:center;">2.0 branch</td><td>&harr;</td>
    <td style="text-align:center;">Dune 2.3.1+</td>
    <td style="padding-left:10px;">
      <b>releases/2.0</b>
    </td>
  </tr>
  <tr>
    <td style="text-align:center;">1.1 branch</td><td>&harr;</td>
    <td style="text-align:center;">Dune 2.2</td>
    <td style="padding-left:10px;">
      <b>releases/1.1</b>
    </td>
  </tr>
  <tr>
    <td style="text-align:center;">1.0 branch</td><td>&harr;</td>
    <td style="text-align:center;">Dune 2.2</td>
    <td style="padding-left:10px;">
      <b>releases/1.0</b>
    </td>
  </tr>
  <tr>
      <td style="text-align:center;">Snapshot</td><td>&harr;</td>
      <td style="text-align:center;">Dune 2.1</td>
      <td style="padding-left:10px;">
    <b>releases/2.1snapshot</b>
      </td>
  </tr>
  <tr>
      <td style="text-align:center;">Snapshot</td><td>&harr;</td>
      <td style="text-align:center;">Dune 2.0</td>
      <td style="padding-left:10px;">
    <b>releases/2.0snapshot</b>
      </td>
  </tr>
  </table>
</dd>


<dt><b>dune-pdelab-howto</b></dt>
<dd><b>git clone
    https://gitlab.dune-project.org/pdelab/dune-pdelab-howto.git</b>
  <table style="font-size: smaller;" summary="">
  <tr>
      <th>PDELab Version</th><th>&nbsp;</th>
      <th>Compatible Dune / TypeTree Release</th>
      <th style="text-align:left; padding-left:10px;">Git branch</th>
  </tr>
  <tr>
      <td style="text-align:center;">master</td><td>&harr;</td>
      <td style="text-align:center;">master</td>
      <td style="padding-left:10px;">
    <b>master</b>
      </td>
  </tr>
  <tr>
      <td style="text-align:center;">2.0 branch</td><td>&harr;</td>
      <td style="text-align:center;">Dune 2.3.1+</td>
      <td style="padding-left:10px;">
    <b>releases/2.0</b>
      </td>
  </tr>
  <tr>
      <td style="text-align:center;">1.1 branch</td><td>&harr;</td>
      <td style="text-align:center;">Dune 2.2</td>
      <td style="padding-left:10px;">
    <b>releases/1.1</b>
      </td>
  </tr>
  <tr>
      <td style="text-align:center;">1.0 branch</td><td>&harr;</td>
      <td style="text-align:center;">Dune 2.2</td>
      <td style="padding-left:10px;">
    <b>releases/1.0</b>
      </td>
  </tr>
  <tr>
      <td style="text-align:center;">Snapshot</td><td>&harr;</td>
      <td style="text-align:center;">Dune 2.1</td>
      <td style="padding-left:10px;">
    <b>releases/2.1snapshot</b>
      </td>
  </tr>
  <tr>
      <td style="text-align:center;">Snapshot</td><td>&harr;</td>
      <td style="text-align:center;">Dune 2.0</td>
      <td style="padding-left:10px;">
    <b>releases/2.0snapshot</b>
      </td>
  </tr>
  </table>
</dd>

</dl>
<p>You can also browse the Git repositories of <b><a href="https://gitlab.dune-project.org/staging/dune-typetree">dune-typetree</a></b>, <b><a href="https://gitlab.dune-project.org/pdelab/dune-pdelab">dune-pdelab</a></b> and <b><a href="https://gitlab.dune-project.org/pdelab/dune-pdelab-tutorials">dune-pdelab-tutorials</a></b> through a more comfortable web frontend.</p>
