+++
module = "dune-curvilineargrid"
group = ["grid"]
requires = ["dune-grid"]
maintainers = "[LSPR AG](https://lspr.swiss/)"
short = "CurvilinearGrid implements a 3d simplex grid based on Lagrangian interpolation. Features include flexible polynomial order, parallel mesh reading and partitioning, and curvilinear visualization via VTK."
git = "https://github.com/LSPR-AG/dune-curvilineargrid"
+++
