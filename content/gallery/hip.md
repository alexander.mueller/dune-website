+++
title = "Heterogeneous hip model"
content = "carousel"
image = ["/img/example-hip.png"]
+++

Heterogeneous hip model with multiple coupled 3d and 1d grids.
ECMath
[project A-CH1](http://www.mi.fu-berlin.de/en/math/groups/ag-numerik/projects/A-CH1/)
*Reduced basis methods in orthopedic hip surgery planning*
(Simulation by Jonathan Youett)

<!--more-->
