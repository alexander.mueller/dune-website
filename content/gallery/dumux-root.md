+++
title = "Root--Soil Interaction"
content = "carousel"
image = ["/img/dumux-root.png"]
+++

Simulation of the water flow field in the soil and the root xylem of
a growing white lupin system. For details see
[Koch et al 2018](https://www.doi.org/10.2136/vzj2017.12.0210).

<!--more-->

The geometry is obtained from a white lupin root system. The lighter the color
the drier the soil. Arrow visualize the water flow field. Visualized using Paraview.
The code used in the publication is available [here](https://git.iws.uni-stuttgart.de/dumux-pub/Koch2017a).
