+++
title = "Pore-scale simulation using Cut-Cells"
content = "carousel"
image = ["/img/unfitted-dg-vector.png"]
+++

An unfitted dG cut-cell discretization of laminar flow at the
pore-scale ([Engwer 2009](http://archiv.ub.uni-heidelberg.de/volltextserver/9990/)).

<!--more-->

The geometry is obtained from a micro-CT scan. In order to avoid
meshing problems we employed a cut-cell based finite element
simulation using discontinuous trial functions. Solving the flow-field
at the pore-scale yields an effective permeability at the macro-scale.

Level-set based cut-cell reconstructions as used in this simulation
are now available in the [dune-tpmc](/modules/dune-tpmc) module.
