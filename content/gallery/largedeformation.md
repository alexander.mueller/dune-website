+++
title = "Large deformation contact"
content = "carousel"
image = ["/img/example-large-deformation-torus.png"]
+++

Large deformation contact problem.
J. Youett, O. Sander, R. Kornhuber.
A globally convergent filter-trust-region method for large deformation contact problems.
SIAM J. Sci. Comp., accepted 2018
(Simulation by Jonathan Youett)
