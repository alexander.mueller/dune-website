+++
title = "Parallel-adaptive higher order simulation of Forward Facing Step"
content = "carousel"
image = ["/img/ffs3d_small.gif"]
+++

Parallel-adaptive simulation of the Euler equations of gas dynamics using
a stabilized 3rd order discontinuous Galerkin solver.
[DK](https://doi.org/10.1007/s10915-010-9448-0).

<!--more-->
