+++
# Please do not add releases directly, but instead use the script add_release.py
# from the bin subdirectory. It applies some additional magic to this archetype.
date = "2021-09-03T18:43:00+01:00"
version = "2.8.0"
major_version = "2"
minor_version = "8"
patch_version = "0"
modules = ['dune-common', 'dune-istl', 'dune-geometry', 'dune-localfunctions', 'dune-grid', 'dune-typetree', 'dune-functions', 'dune-uggrid']
signed = 1
title = "Dune 2.8.0"
doxygen_branch = ["v2.8.0"]
doxygen_url = ["/doxygen/2.8.0"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.8.0"]
support = ["linux","windows"]
[menu.main]
parent = "releases"
weight = -1
+++
# Dune 2.8 - Release Notes

## Module dune-common

- Python bindings have been moved from `dune-python` to the respective
  core modules. `dune-python` is now obsolete. To activate Python bindings the
  CMake flag `DUNE_ENABLE_PYTHONBINDINGS` needs to be turned on (default is off).
  Furthermore, flags for either shared library or position independent code
  need to be used.

- Remove the cmake check for `HAVE_MPROTECT` and also do not define this variable in the
  `config.h` file. It is defined only inside the header `debugallocator.hh`.

- Added `Dune::IsCallable` (in `dune/common/typetraits.hh`) which is
  an improved version of the deprecated `Dune::Std::is_callable` and allows
  for checking if a type is a function object type,
  i.e. has a ()-operator than can be invoked with the given argument types and
  returns a specified return type.

- Remove c++ feature tests in cmake for existing c++-17 standards. Add default
  defines for `DUNE_HAVE_CXX_BOOL_CONSTANT`, `DUNE_HAVE_CXX_EXPERIMENTAL_BOOL_CONSTANT`,
  `DUNE_HAVE_HEADER_EXPERIMENTAL_TYPE_TRAITS`, `DUNE_HAVE_CXX_APPLY`,
  `DUNE_HAVE_CXX_EXPERIMENTAL_APPLY`, `HAVE_IS_INDEXABLE_SUPPORT` in `config.h` for one
  more release.

- Previous versions of dune-common imported `std::shared_ptr` and `std::make_shared`
  into the `Dune` namespace.  dune-common-2.8 stops doing that.

- The file `function.hh` is deprecated.  It contained the two base classes
  `Function` and `VirtualFunction`.  In downstream codes, these should be
  replaced by C++ function objects, `std::function` etc.

- Python bindings have been moved from the `dune-python` module which is now
  obsolete. To activate Python bindings the CMake flag
  `DUNE_ENABLE_PYTHONBINDINGS` needs to be turned on (default is off).
  Furthermore, flags for either shared library or position independent code
  needs to be used.

- Support for distributing DUNE modules as python packages has been added.
  Package meta data is parsed in `packagemetadata.py` from the dune.module file.
  A script `/bin/dunepackaging.py` was added to generate package files
  (`setup.py`, `pyproject.toml`) that can also be used to upload packages to
  the Python Package Index. For a brief description of what is required to add
  this support to existing dune modules see
  https://gitlab.dune-project.org/core/dune-common/-/merge_requests/900
  Note that this can also be used to generate a package for dune modules
  that don't provide Python bindings.

- Eigenvectors of symmetric 2x2 `FieldMatrix`es are now computed correctly
  even when they have zero eigenvalues.

- Eigenvectors and values are now also supported for matrices and
  vectors with `field_type` being float.

- The `ParameterTreeParser::readINITree` can now directly construct and
  return a parameter tree by using the new overload without parameter tree
  argument.

- `MPIHelper::instance` can now be called without parameters if it was
  already initialized.

- `MPITraits` now support complex.

- There is now a matrix wrapper `transpose(M)` that represents the
  transpose of a matrix.

### build-system

- Set minimal required CMake version in cmake to >= 3.13.

- Set minimal required MPI version to >= 3.0.

- Add backport of `FindPkgConfig.cmake` from cmake 3.19.4 since there was a bug in
  an older find module leading to problems finding tbb in debian:10.

- Update the FindTBB cmake module to search for the `TBBConfig.cmake` or the `tbb.pc`
  file containing the configuration. Add the `AddTBBFlags.cmake` file containing
  the macro `add_dune_tbb_flags` that must be called to use TBB.

- The name mangling for Fortran libraries like BLAS and LAPACK is now done
  without a Fortran compiler. So a Fortran compiler is no longer a built
  requirement.

- `dune_list_filter` is deprecated and will be removed after Dune 2.8. Use
  `list(FILTER ...)` introduced by CMake 3.6 instead.

- Remove the CMake options `DUNE_BUILD_BOTH_LIBS` and
  `DUNE_USE_ONLY_STATIC_LIBS`. Use the default CMake way instead by
  setting `BUILD_SHARED_LIBS` accordingly. Building both static
  and shared libraries is no longer supported.

- Remove the CMake function deprecated `inkscape_generate_png_from_svg`.

- Remove the old and deprecated use of UseLATEX.cmake.
  `dune_add_latex_document` is a redirection to `add_latex_document`
  which internally uses `latexmk`.

- Many of the CMake find modules habe been rewritten to use CMake's
  imported targets. These targets are also used in the DUNE CMake
  package configuration files, where they might appear in e.g. the
  dune-module_LIBRARIES. If you do not use the DUNE CMake build system
  the linker might complain about e.g. METIS::METIS not being
  found. In that case your either need to use the CMake modules shipped with
  DUNE or create these targets manually.

### Deprecations and removals

- Remove deprecated type-traits `has_nan`, `is_indexable`, and
  `is_range`, use the CamelCase versions instead.

- Deprecate fallback implementations `Dune::Std::apply`, `Dune::Std::bool_constant`, and
  `Dune::Std::make_array` in favor of std c++ implementations.

- Deprecate type traits `Dune::Std::to_false_type`, `Dune::Std::to_true_type`.
  `Dune::AlwaysFalse` and `Dune::AlwaysTrue` (from header `dune/common/typetraits.hh`)
  now inherit from `std::true_type` and `std::false_type` and are therefore
  exact replacements for these two type traits.

- Deprecate fallback implementation `Dune::Std::conjunction`, `Dune::Std::disjunction`,
  and `Dune::Std::negation`. Use std c++17 implementations.

- Deprecate fallback implementations `Dune::Std::is_callable` and `Dune::Std::is_invocable`.
  Use C++17 std implementation `std::is_invocable` instead. Be aware that
  `Dune::Std::is_callable` and `std::is_invocable` are slightly different concepts,
  since `std::is_invocable` also covers invocation of pointers to member functions
  and pointers to data members. To additionally constrain for that case,
  there is now `Dune::IsCallable` (in `dune/common/typetraits.hh`)

- `ToUniquePtr` is deprecated and will be removed after Dune 2.8. Use
  `std::unique_ptr` or `std::shared_ptr` instead.

- Remove deprecated header `dune/common/std/memory.hh`; use `<memory>`
  instead.

- Deprecate header `dune/common/std/utility.hh`; use `<utility>` instead.

- Deprecate header `dune/common/std/variant.hh`; use `<variant>` instead.

- Remove incomplete CPack support that was never used to make an official
  build or tarball.

- Both macros `DUNE_DEPRECATED` and `DUNE_DEPRECATED_MSG(text)` are
  deprecated and will be removed after Dune 2.8. Use C++14 attribute
  `[[deprecated]]` but be aware that it is no drop-in replacement,
  as it must be sometimes placed at different position in the code.

- The macros `DUNE_UNUSED` is deprecated and will be removed after
  Dune 2.8. Use C++17's attribute `[[maybe_unused]]` instead, but be
  aware that it is no drop-in replacement, as it must be sometimes
  placed at different position in the code.
  The use of `DUNE_UNUSED_PARAMETER` is discouraged.

- `Dune::void_t` has been deprecated and will be removed. Please use
  `std::void_t`

- `Dune::lcd` and `Dune::gcd` are deprecated and will be removed. Please
  use `std::lcd` and `std::gcd`.

- `VariableSizeCommunicator::fixedsize` has been renamed to FixedSize in
  line with the communicator changes of dune-grid. The old method will
  be removed in 2.9.


## Module dune-geometry

- Python bindings have been moved from the `dune-python` module which is now
  obsolete. To activate Python bindings the CMake flag
  `DUNE_ENABLE_PYTHONBINDINGS` needs to be turned on (default is off).
  Furthermore, flags for either shared library or position independent code
  needs to be used.

- The class `AxisAlignedCubeGeometry` has always had a constructor taking
  two arguments `FieldVector<ctype,coorddim> lower` and `FieldVector<ctype,coorddim> upper`.
  This constructor was always to be used in the case `dim==coorddim` only,
  but this was never enforced.  Starting with version 2.8, compilation
  fails with an error message if this constructor is used with `dim!=coorddim`.

- Two new sets of quadrature rules are provided: the left and right Gauss-Radau quadrature rules.
  These are optimal rules that include only one endpoint of the integration interval
  (either left or right) and integrate polynomials of order 2n - 2 exactly.

- GeometryType has four new methods: `isPrismatic()`, `isPrismatic(int step)` and `isConical()`,`isConical(int step)`.
  The versions with an argument return true if the corresponding construction was used in step 0 <= `step` <=dim-1.
  The other two assume a default argument of `dim-1` (the latest construction step).

- GeometryTypes has two new methods: `prismaticExtension(GeometryType gt)` and `conicalExtension(GeometryType gt)`.
  They return an extended GeometryType based on `gt` via the corresponding construction. For example:
  ```c++
  GeometryType gt = GeometryTypes::line;
  auto square = GeometryTypes::prismaticExtension(gt);
  auto triangle = GeometryTypes::conicalExtension(gt);
  ```

### Deprecations and removals

- Remove code needed to use reference elements by reference.

- Remove `GeometryType`'s deprecated member functions
  `GeometryType::make...()`.

- Remove deprecated constructor `GeometryType(unsigned dim)`.

- Remove deprecated `CompositeQuadratureRule(QuadratureRule, int)`. Use
  `CompositeQuadratureRule(QuadratureRule, Dune::refinement{Intervals|Levels}(int))`
  instead.

- Removed all structs from `Impl` dealing with the recursive topology construction: `TopologyFactory`, `TopologySingletonFactory`,
  `Point`, `Prism`, `Pyramid`, `IsSimplex`, `IsCube`, `SimplexTopology`, `CubeTopology`, `PyramidTopology`, `PrismTopology`, `IfTopology`.
  Deprecated the free function `Impl::isTopology`.
  Use the geometries provided by `GeometryType` and `GeometryTypes` instead.
  To simplify the transition you can include the header "dune/geometry/deprecated_topology.hh".


## Module dune-grid
 
- Specialization of `StructuredGridFactory` for `AlbertaGrid` is added to address the special
  numbering requirements of that grid.

- Return type of `GridFactory::createGrid()` changed to `std::unique_ptr`. While still the
  replacement type `ToUniquePtr` from dune-common works, it is marked deprecated and the std
  implementation should be used directly now.

- `UGGrid` removes support for `_2` and `_3` macros.

- `SingleCodimSingleGeomTypeMapper` and `MultipleCodimMultipleGeomTypeMapper` now have an `update(gridView)`
  method to update the stored `GridView` and recalculate the indices after mesh adaptation.

- The `update()` member function of mappers is deprecated. Use the new
  member function `update(gridView)` with a grid view argument when updating
  the mapper after the grid or grid view changes. The interface
  change reflects that grid view has value semantics.

- The "convenience" classes `LeafSingleCodimSingleGeomTypeMapper`,
  `LevelSingleCodimSingleGeomTypeMapper`, `LeafMultipleCodimMultipleGeomTypeMapper`,
  `LevelMultipleCodimMultipleGeomTypeMapper` have been deprecated since they
  don't comply with the new mapper interface.
  Just use `SingleCodimSingleGeomTypeMapper` and `MultipleCodimMultipleGeomTypeMapper`.

- `IdSet` now exports grid `dimension` and `Codim<cd>::Entity`.

- `UGGrid` index sets can now compute the indices of vertices of edges.

- `UGGrid`: Fixed a bug in the numbering of prism edges.

- Various bugs have been fixed in the `UGGrid` subdomain communication implementation.

- `YaspGrid` now supports class template argument deduction
   (https://en.cppreference.com/w/cpp/language/class_template_argument_deduction)
   if you have a C++17-CTAD-compatible compiler (gcc >= 8, clang 5)

- Python bindings have been moved from the `dune-python` module which is now
  obsolete. To activate Python bindings the CMake flag
  `DUNE_ENABLE_PYTHONBINDINGS` needs to be turned on (default is off).
  Furthermore, flags for either shared library or position independent code
  needs to be used.
- Properly implement the `canCommunicate` capability for `UGGrid` and `IdentityGrid`.

- The return type of the `IndexSet::size` methods isn't `IndexType`
  anymore. In general the return type should be an unsigned integral
  type. The actual type is implementation specific. All
  implementations in `dune-grid` now return `std::size_t`, following
  our approach to make all size information be unsigned.

- The grid capability `hasEntityIterator` now defaults to whatever
  the capability `hasEntity` specifies. Most grid manager implementers
  now only need to implement/specialize `hasEntity`.

- Add a new range generator `subEntities` that allows to iterate over the sub-entities
  of a specified codimension for a given element (codim-0 entity)
  ```cpp
  for (const auto& vertex : subEntities(element, Codim<dim>))
    std::cout << vertex.geometry().corner(0) << "\n";
  ```
  The returned range is sized, i.e. `subEntities(element, Codim<dim>).size()`
  returns the number of vertices in the element and is equivalent to
  `referenceElement(element).size(dim)`.

### Deprecations and removals

- Remove `Intersection`'s deprecated enums `dimension` and
  `codimension`. Instead use grid's dimension and 1.

- Remove deprecated `Grid::getRealImplementation`.
  Use the `impl()` member function of the facade classes directly instead.

- Remove GeometryGrid's deprecated constructors accepting raw pointers.

- Remove deprecated `AlbertaGrid::getRealIntersection`.
  Use the `impl()` member function of the intersection class directly instead.

- Remove deprecated `AlbertaGrid::readGridXdr` and `AlbertaGrid::writeGridXdr`.
  Instead use `AlbertaGrid::readGrid` and `AlbertaGrid::writeGrid`.

- Remove deprecated header `dune/common/universalmapper.hh`.

- Support for PSurface is deprecated and will be removed after Dune 2.8.

- Support for AmiraMesh is deprecated and will be removed after Dune 2.8.


## Module dune-istl

- Extended the MatrixMarket IO functions for reading and writing vectors with
  SIMD field_type as tall-skinny matrices.

- Added public access of the `cholmod_common` object in class `Cholmod`.

- Python bindings have been moved from the `dune-python` module which is now
  obsolete.
  Note that for `dune-istl` bindings are still very much work in progress.
  To activate Python bindings the CMake flag
  `DUNE_ENABLE_PYTHONBINDINGS` needs to be turned on (default is off).
  Furthermore, flags for either shared library or position independent code
  needs to be used.

- Added new utility functions templates `maxBlockLevel`, `minBlockLevel`, `blockLevel` in `dnue/istl/blocklevel.hh` to
  automatically determine the block level of possibly nested ISTL vectors & matrices at compile time.
  The block level cannot be always uniquely determined for `MultiTypeBlockMatrix`/`MultiTypeBlockVector` since the nesting level
  of different block types might differ. Hence, `maxBlockLevel`, `minBlockLevel` always works
  but if they yield different results `blockLevel` will not compile.
  This condition can be checked with the function template `hasUniqueBlockLevel`.

- The internal setup code of the various SuiteSparse solvers (like `UMFPack`)
  has been cleaned up.  The effects should be invisible to all regular users.
  However, if you have happened to use the `ColCompMatrix` and `ColCompMatrixInitializer`
  classes in the past you need to port your code to use `Impl::BCCSMatrix` and
  `Impl::BCCSMatrixInitializer` instead.  Their interfaces have changed a little bit;
  please look at the class documentation for details.  The old header `colcompmatrix.hh`
  is still there, but backward compatibility is only partial.

- More implementation code of the ILU preconditioners (in `ilu.hh`) has moved
  into the `ILU` namespace.  With the move, some methods have changed their names
  from std-style to CamelCase.  The old methods are still there, but they are
  deprecated now. The class `MatrixBlockError` has moved from the file `ilu.hh`
  to the file `istlexception.hh`, because it is of wider interest.

- Added the routines `flatVectorForEach` and `flatMatrixForEach` that traverse a (blocked) vector or matrix container.
  At each entry a functor is called taking the entry and the (flat) index offset.

- `Cholmod` solver can now be used with each blocked matrix/vector type compatible with `flatVectorForEach` and `flatMatrixForEach`.

### Deprecations and removals

- Drop deprecated bindings of direct solver Pardiso.

- Remove deprecated preconditioner implementations `SeqILU0` and `SeqILUn`. Use
  `SeqILU` instead, which implements incomplete LU decomposition of any order.

- Remove deprecated methods 'BlockVector::resize' and 'BlockVector::reserve'
  with two arguments.

- Drop support SuperLU 4.

- Rename the exception `ImplicitModeOverflowExhausted` to `ImplicitModeCompressionBufferExhausted`,
  to better reflect its meaning.  The old exception is still there, but it triggers
  a deprecation warning.

- Remove deprecated `SequentialInformation::getSolverCategory()`, use
  `category()` instead.

### Known issues

- SuiteSparse's threading tends to conflict with the threading from OpenBLAS. The
  author of SuiteSparse reports cases of an
  [100 fold slowdown](https://github.com/DrTimothyAldenDavis/SuiteSparse/issues/1)
  for cholmod. See also
  [dune-istl #91](https://gitlab.dune-project.org/core/dune-istl/-/issues/91).


## Module dune-localfunctions:

* Passing functions that support `f.evaluate(x,y)` to `interpolate()`
  is deprecated. Instead the functions should now provide `operator()`.
  Passing functions providing the old interface is still supported in 2.8.
  * `LocalFiniteElementFunctionBase` is deprecated. You can rely
    on duck-typing when passing functions with the new interface.
  * The virtual interface for interpolating functions in `LocalFiniteElementVirtualInterface`
    now uses `std::function` instead of the deprecated `VirtualFunction`
    for the passed function.
  * The virtual interface wrapper `LocalFiniteElementVirtualImp` now
    requires that the wrapped `LocalFiniteElement` implementation
    supports the new `operator()` based interpolation interface.

* Add an implementation of the Nédélec element of the first kind,
  as introduced in "Nédélec, Mixed finite elements in R^3, 1980,
  DOI: http://dx.doi.org/10.1007/BF01396415".
  Only the first-order case for triangles, tetrahedra, squares and cubes is implemented.

* Fix a bug in a shape function of the second-order Lagrange element
  on the three-dimensional pyramid.

* Add an implementation of the Raviart-Thomas element for tetrehedra with order 0.

* Remove deprecated `GenericLocalFiniteElement::topologyId()`, use
  `type().id()` instead.

* Imported the Python bindings from the 2.7 branch of dune-python.

* Replaced the combination of function arguments `topologyId` and `dim` with a single `GeometryType` argument.
  Tagged the old versions of: `numLagrangePoints`, `equidistantLagrangePoints`, `RTL2InterpolationBuilder::topologyId()`,
  `VirtualMonomialBasis(topologyId)`, `VirtualMonomialBasis::topologyId()` as deprecated.

* Add a construction algorithm for high order Nédélec elements on triangles and tetrahedra.


## Module dune-functions

- `PreBasis` implementations are now required to provide a method
  `PreBasis::indices(node,iterator)` that replaces binding a `NodeIndexSet`
  to `node` and then calling `NodeIndexSet::indices(iterator)`.
  As a consequence `PreBasis::IndexSet` and `PreBasis::makeIndexSet`
  are no longer needed.

- The `RaviartThomasBasis` and `BrezziDouglasMariniBasis` now return
  Piola-transformed shape functions.  This is implemented by changing
  the return value of `tree().finiteElement()`: It is not an object
  of type `RaviartThomasLocalFiniteElement`
  or `BrezziDouglasMariniLocalFiniteElement` anymore. Rather, it is
  an object of a new type `GlobalValuedLocalFiniteElement`, which wraps
  other `LocalFiniteElement` implementations and applies a range-space
  transformation.  Domain-space transformations still have to be done
  by the calling code. The `GlobalValuedLocalFiniteElement` still
  implements the `LocalFiniteElement` interface of `dune-localfunctions`.

- The `RaviartThomasBasis` class now supports tetrahedral grids for `order=0`,
  quadrilateral grids for `order=2`, and hexahedral grids for `order=1`.

- The `RannacherTurekBasis` class now supports Crouzeix-Raviart elements.
  Grids containing simplices, cubes or both in 2d and 3d are supported now.

- The `dune-functions` module now contains an implementation of a
  Nedelec basis (for problems posed in H(curl)).  While the interface
  caters to different basis orders, grid dimensions and element types,
  only the first-order basis called "of the first kind" is implemented,
  and only for grids containing simplices, cubes or both in 2d and 3d.

- The `dune-functions` module now contains an implementation of a Hierarchical Lagrange
  basis for second order on simplex grids

- There is now an experimental implementation of a periodic basis in form
  of a class `PeriodicBasis`.  It is a meta basis, i.e., a basis that is
  parametrized with another basis (the host basis).  In a `PeriodicBasis`,
  global degrees of freedom of the host basis can be grouped into
  equivalence classes, which are then treated as single global degrees
  of freedom.  This allows, in particular, to implement periodic
  boundary conditions for discretizations without intersection integrals.

  The `PeriodicBasis` class can only be constructed by using the `periodic`
  method from the namespace `Dune::Functions::BasisFactory::Experimental`.
  It can change at any moment without much advance notice.  Use it at your
  own risk, and give us feedback!

- Imported the Python bindings from the 2.7 branch of dune-python and fixed remaining issues.
  Added a CI test that builds various global bases in 2d and 3d  and verifies the correct number of dofs.

- `interpolate` is now capable of interpolating vector-valued finite element functions correctly.
  The method of using scalar basis functions combined with vector-valued coefficients to mock a power basis is still supported.


## Module dune-typetree

-   Add support for tree transformations for `DynamicPowerNode`.

-   A power node with runtime degree, `DynamicPowerNode`, is added.

-   Allow tree traversal for nodes with runtime degree that are not necessarily power nodes.

-   The `forEachNode()` traversal function with multiple callbacks is deprecated. Use the
    more general `applyToTree()` instead.

-   Removed the type aliases for the storage type `Storage` and `ConstStorage` in the
    node implementations.

-   The memberfunction `childStorage()` in the nodes now consistently take an index or an
    `index_constant` as argument.


## Module dune-uggrid

* Added support for All_All communication on facets.

* Removes support for `_2` and `_3` macros.

* Remove support for XDR.
