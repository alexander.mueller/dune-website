+++
date = "2010-04-25T16:03:25+01:00"
major_version = 2
minor_version = 0
modules = ["dune-common", "dune-istl", "dune-grid", "dune-localfunctions", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 0
title = "Dune 2.0.0"
version = "2.0.0"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 2.0 - Release Notes

A new module `dune-localfunctions` was introduced. This package brings a new interface for finite element shape functions to DUNE and it will in the future become a DUNE core module.

## Build system

*   The Doxygen style files are now automatically generated from the sources in dune-common/dune-web. If present, you should remove the following files from your repository:
    `doc/doxygen/doxy-footer.* doc/doxygen/doxy-header.* doc/doxygen/dune-doxy.css`
*   The wml based html generation does now use the layout files from dune-common. If present, you should remove the following files from your module:
    `doc/.wmlrc doc/layout/*`
    If you remove the layout directory remember to update the configure.ac and doc/Makefile.am accordingly.
*   It is now possible to do out-of-source builds. For this, pass the option `--build-dir` to the `configure` command.

## dune-common

*   The `dunecontrol` program now has an option `--resume` which allows to continue an interrupted build run in the first unbuild module. During debugging, this can decrease your build time considerably. Nice!

### Deprecated or removed features

*   The file `bitfield.hh`, already deprecated in 1.2, has been removed. Use `bitsetvector.hh` or `std::vector<bool>` instead.
*   The file `stack.hh` and class `Stack`, already deprecated in 1.2, have been removed. Please use `std::stack` instead.
*   The class `DoubleLinkedList`, already deprecated in 1.2, has been removed. Please use `std::list` instead.
*   The file `fixedarray.hh` has been renamed to `array.hh`. That way the file has the same name as the class it contains. The old file is still there for backward compatibility.
*   The type trait `EnableIf` has been removed. Please use `enable_if` instead.
*   A new smart pointer implementation `shared_ptr` (in shared_ptr.hh) has been added, which will replace `SmartPointer`. The new class reimplements the stl shared_ptr. Beware of the following differences when transitioning from SmartPointer to `shared_ptr`:
    *   the type SmartPointer::MemberType becomes shared_ptr::element_type,
    *   the method SmartPointer::count() becomes shared_ptr::use_count(),
    *   The default constructor of SmartPointer allocates an element, and hence returns a dereferenceable pointer. Default-constructing a shared_ptr instead creates a pointer pointing to NULL. Hence, if you rely on the SmartPointer default constructor allocating memory, THIS CHANGE WILL BREAK YOUR CODE!
*   The class `CompileTimeChecker`, already deprecated in 1.2, has been removed. Please use `dune_static_assert` instead.
*   The header `dune/common/helpertemplates.hh` and with it the classes `Derived_from, Can_copy, Can_compare, Can_multiply` are deprecated and will be removed in one of the following releases.

## dune-grid

*   You can now import grids and boundary description from [gmsh](http://geuz.org/gmsh/). Gmsh is an open source grid generator and CAD engine. Gmsh reading is supported for all grid that implement the `GridFactory`.
*   Considerable progress has been made on the parallel features of `UGGrid`. Special thanks go to Andreas Lauser for this. The following things should be working now.
    *   Static load balancing
    *   Level iterators for all partition types
    *   Communication for vertex data with constant and variable data size per vertex (for the interfaces InteriorBorder_All and InteriorBorder_InteriorBorder)
    *   CollectiveCommunication
    *   The GlobalIdSet
*   The IndexSet classes now have a method `subIndex` where the codimension of the subentity can be given at run-time. Previously the codimension had to be a static parameter. The same holds for the `subId` method in `IdSet`, and the `map` methods in the various mapper classes.
*   The geometry class now has a method `jacobianTransposed` that returns the transposed of the Jacobian (i.e., tangential vectors are in the matrix rows).
*   The methods `intersectionGlobal`, `intersectionSelfLocal` and `intersectionNeighborLocal` on the intersection have been renamed to `geometry`, `geometryInInside` and `geometryInOutside`.
*   We introduced a new numbering of the subentities of the reference elements. The reason is that the new numbering is more consistent and generalizes more easily to higher dimensions.
    **Warning: This change may affect your code in subtle ways if you depend on a particular numbering!**
    We have assembled a [transition page](http://www.dune-project.org/doc-2.0/doxygen/html/group__GridDuneREToGenericRE.html) with the technical details.
*   The methods `numberInSelf` and `numberInNeighbor` have been renamed to `indexInInside` and `indexInOutside`, respectively. The meaning of the return value has changed; now it returns the index with respect to the generic reference element.
*   The methods `overlapSize` and `ghostSize` are now available on grid views.
*   A new grid implementation, the `GeometryGrid`, has been added. It wraps any other DUNE grid and replaces the geometry by a generic one. It can, for example, be used to transform a 2d Cartesian grid into a helix. Have a look a the [documentation](http://www.dune-project.org/doc/doxygen/dune-grid-html/group__GeoGrid.html) for more information.
*   Intersections with the domain boundary now provide a boundary segment index, which is unique within the macro grid. Children of boundary segments inherit this index from their father. The index starts at zero is consecutive and the maximum number is returned by the method `numBoundarySegments` on the grid.
*   A few new methods have been added to the interface (see the documentation for details):
    *   Entity::hasFather()
    *   Geometry::center()
    *   Intersection::centerUnitOuterNormal()
*   The `GridFactory` class has been extended to include methods which map a given entity (codimension equal to zero or dimension) to the order in which it was inserted. This makes it possible to easily attach data to a dune grid after creation through the grid factory.
*   The `--with-grid-type`, `--with-grid-dim`, and `--with-world-dim` flags have been removed from the dunecontrol process. The required CXXFLAGS for using `gridtype.hh` are now always available. The consequence is that no default can be prescibed during the build process and also no default is set in the gridtype.hh header. Thus the `GRIDTYPE` and `GRIDDIM` variables have to be set either in Makefile.am or during the call to make. For programs using gridtype.hh, which are made during the build process, these variables have to be set in Makefile.am. As before `WORLDDIM` defaults to `GRIDDIM`.
*   Objects of type `OneDGrid` can now be created using the `GridFactory`.

### Deprecated or removed features

*   The method `Geometry::checkInside` has been deprecated. The same functionality is now available from the `GenericReferenceElements`.
*   The `dimworld` template parameter has been removed from `YaspGrid`. It was there for historical reasons only and had already been deprecated in the 1.2 release.
*   The class `IntersectionGetter` has been removed. With the new `GridView` mechanism it is not necessary anymore.
*   The IndexSet classes do not provide iterators over the grid any more. This feature had been deprecated in 1.2\. Now it is gone for good.
*   In 1.2, several methods and exported types of the `IntersectionIterator` class have been deprecated there and moved to the `Intersection` class. These methods have now been removed from `IntersectionIterator`.
*   The method `Grid::name` has been deprecated.

## dune-istl

*   A new matrix class `BTDMatrix` has been added. It allows to store tridiagonal matrices and solve tridiagonal linear problems in linear time. **Caveat:** Solving currently only works for scalar matrices.

## dune-grid-howto

*   A new example of a Finite-Element code for the Poisson equation was added.

# Known Bugs

## Severe Bugs

*   _Dune violates strict-aliasing_

    You have to compile DUNE will the the option "--no-strict-aliasing". Newer versions of `g++` make more use of strict aliasing rules. If you don't use the "--no-strict-aliasing" option, you will receive many warnings, and you might even observe undesired results and unreproducible errors.

    But we are not alone with this problem. Several software projects intentionally violate strict-aliasing. For example, Python 2.x did so to implement reference counting ( [http://mail.python.org/pipermail/python-dev/2003-July/036898.html](http://mail.python.org/pipermail/python-dev/2003-July/036898.html)). The Linux kernel does this because strict aliasing causes problems with optimization of inlined code ( [http://lkml.org/lkml/2003/2/26/158](http://lkml.org/lkml/2003/2/26/158)).

    For more information see [Flyspray task #674](http://www.dune-project.org/flyspray/index.php?do=details&task_id=674)

## Minor Bugs

A list of all minor bugs can be found online in our bug-tracker [http://www.dune-project.org/flyspray/](http://www.dune-project.org/flyspray/). Some of these bugs are already fixed in the trunk, but not in the release.

*   _[FS#760](http://www.dune-project.org/flyspray/index.php?do=details&task_id=760): geometryInFather is not topological in ALUGrid with boundary parametrization_
*   _AlbertaGrid does not compile with the C++0x `shared_ptr` implementation contained in GCC 4.5_
*   Our drop-in replacement of `std::shared_ptr` differs slightly from the official implementation. With `Dune::shared_ptr` only the destructor of the base class A will be called in the following example:

```
struct A{...};
struct B:public A{...};
void voo()
{
    shared_ptr<A> p = shared_ptr<A>(new B);
}
```

In contrast the `shared_ptr` in (std|std::tr1|boost) will call the destructor of `B` even if it is not virtual. We intend to resolve this issue only implicitly by eventually discarding our own reimplementation of `shared_ptr`:
