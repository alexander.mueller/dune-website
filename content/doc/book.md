+++
title = "Dune book"
tags = [ "publication", "C++" ]
+++
<div markdown style="float:right; padding-right: 1em">
<img src="/img/dune-book-cover.png" width="50%" />
</div>

Our fellow developer Oliver Sander wrote a book on Dune and it's
concepts, the `C++` interfaces and offers additional information.

The book is available via Springer at their
[online store][dune-book].
It explains the ideas and concepts
underlying Dune, shows the main `C++` interfaces, and discusses several
complete examples.

You get a peek into the book, as the first chapter on 
*How to get started with the C++ code*
[can be read online](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf).

[dune-book]: https://www.springer.com/gp/book/9783030597016
