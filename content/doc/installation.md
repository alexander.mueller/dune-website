+++
title = "Installation"
toc = 1
[menu.main]
parent = "docs"
identifier = "installation"
weight = 1
+++

## Installing Binary and Python packages

User friendly installation of pre-compiled binary packages or Python packages of the main
Dune components is described in the [Getting started](/doc/gettingstarted)
section.

 * [Installation on Debian and Ubuntu systems](/doc/installation/installation-deb)
 * [Installation on Windows via WSL+Ubuntu](/doc/installation/installation-windows-wsl)
 * [Installation via pip](/doc/installation/installation-pip)

## Installation from source

Details on the [installation from source](/doc/installation/installation-buildsrc).

## Prerequisites

In order to build your own DUNE module you need at least the following software:

* C++ compiler supporting c++-17 language standard, e.g. [clang](https://clang.llvm.org/) >= 5, [g++](https://gcc.gnu.org/) >= 7
* [CMake](https://cmake.org) >= 3.13
* [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)

Detailed information on supported compiler and CMake versions can be found in the [release notes](/releases) for releases and in the list [recent changes](/dev/recent-changes) for the development branch *master*. See also the [FAQ section](#faq) for information about how to resolve the build dependencies.

The following software is recommend but optional:

* MPI (e.g. [OpenMPI](https://www.open-mpi.org/) or [MPICH](https://www.mpich.org/))
* Python Virtual Environment (e.g. [python3-venv](https://wiki.ubuntuusers.de/venv/) or [virtualenv](https://pypi.org/project/virtualenv/))

This will provide you with the core DUNE features.

Some DUNE modules might support further software. At the end of the configuration process of each dune module, a list of found, optional and required dependencies is listed.

## [External libraries](external-libraries)

Apart from the immediate requirements there are various external
libraries that you can use together with DUNE. These include grid
managers, file format libraries, fast linear algebra packages and
more.

[On this page](external-libraries) we list some information how to configure DUNE to
find these packages.
