+++
title = "Grid features"
toc = 1
[menu.main]
parent = "grids"
weight = 2
+++

The following is a list of available Dune grids. Some of them are just wrapper
to external libraries.

{{<table>}}
| Name            | <abbr title="name of the dune module providing the grid">Module</abbr>       | <abbr title="whether the grid only wraps an external library">External</abbr>
| ---             | ---          | ---
| AlbertaGrid     | [dune-grid](/modules/dune-grid)    | yes
| ALUGrid         | [dune-alugrid](/modules/dune-alugrid) | no
| CurvilinearGrid | [dune-curvilineargrid](/modules/dune-curvilineargrid)  | no
| FoamGrid        | [dune-foamgrid](/modules/dune-foamgrid)  | no
| OneDGrid        | [dune-grid](/modules/dune-grid)    | no
| P4estGrid       | [dune-p4estgrid](/modules/dune-p4estgrid) | yes
| PolygonGrid     | [dune-polygongrid](/modules/dune-polygongrid)  | no
| SPGrid          | [dune-spgrid](/modules/dune-spgrid)  | no
| UGGrid          | [dune-uggrid](/modules/dune-uggrid)  | yes
| YaspGrid        | [dune-grid](/modules/dune-grid)    | no
{{</table>}}

## Metagrids

Metagrids wrap other Dune grid, providing facilities such as replacing geometry, glueing domains, etc.

{{<table>}}
| Name            | <abbr title="name of the dune module providing the grid">Module</abbr> | Description
| ---             | ---                                                                    | ---
| GeometryGrid    | [dune-grid](/modules/dune-grid)           | Wrap any other DUNE grid and replace its geometry
| PrismGrid       | [dune-prismgrid](/modules/dune-prismgrid) | Add an additional dimension to the grid using generic prismatic elements
| CacheItGrid     | [dune-metagrid](/modules/dune-metagrid)   |
| CartesianGrid   | [dune-metagrid](/modules/dune-metagrid)   |
| FilteredGrid    | [dune-metagrid](/modules/dune-metagrid)   | Filter elements by given predicate
| IdGrid          | [dune-metagrid](/modules/dune-metagrid)   | Example of a grid wrapper representing the original grid
| MultiSPGrid     | [dune-metagrid](/modules/dune-metagrid)   |
| ParallelGrid    | [dune-metagrid](/modules/dune-metagrid)   |
| SphereGrid      | [dune-metagrid](/modules/dune-metagrid)   |
| GridGlue        | [dune-grid-glue](/modules/dune-grid-glue) | Coupling of two unrelated Dune grids.
| MultiDomainGrid | [dune-multidomaingrid](/modules/dune-multidomaingrid) | Carve up an existing DUNE grid into subdomains and extract interfaces
| SubGrid         | [dune-subgrid](/modules/dune-subgrid)     | A subset of the elements of a given grid treated as a grid hierarchy in its own right.
| CurvedGrid      | [dune-curvedgrid](/modules/dune-curvedgrid) | Parametrization of the geometries of a given grid using [dune-curvedgeometry](/modules/dune-curvedgeometry)
{{</table>}}

# Features

These are the features provided by the grid via its Dune interface -- note that
these may be different from the features provided by the underlying external
grid library.


{{<table>}}
| <abbr title="C++ grid type that is passed to a GridFactory">GridType</abbr>                                                       | Dimensions                  | Structured   | Notes                                                                                   |
| ---                                                        | ---                         | ---          | ---                                                                                     |
| `AlbertaGrid<dim,dimw>`                                    | 1≤dim≤3, 1≤dimw≤9, dim≤dimw | unstructured | Can't have two grids of different dimw in the same binary                               |
| `ALUGrid<dim,dimw,simplex,conforming>`                     | 2≤dim≤dimw≤3                | unstructured |                                                                                         |
| `ALUGrid<dim,dimw,cube,nonconforming>`                     | 2≤dim≤dimw≤3                | unstructured |                                                                                         |
| `ALUGrid<dim,dimw,simplex,nonconforming>`                  | 2≤dim≤dimw≤3                | unstructured |                                                                                         |
| `GeometryGrid<HostGrid>`                                   | hostdim=dim≤dimw            | unstructured |                                                                                         |
| `OneDGrid`                                                 | dim=dimw=1                  | unstructured |                                                                                         |
| `UGGrid<dim>`                                              | 2≤dim=dimw≤3                | unstructured |                                                                                         |
| `SPGrid<ct,dim,rs,comm>`                                   | dim=dimw                    | structured   |                                                                                         |
| `YaspGrid<dim>`                                            | dim=dimw                    | structured   | lower left corner fixed to 0.                                                           |
| `YaspGrid<dim, EquidistantOffsetCoordinates<ctype, dim> >` | dim=dimw                    | structured   | arbitrary lower left corner (introduced in 2.4)                                         |
| `YaspGrid<dim, TensorProductCoordinates<ctype, dim> >`     | dim=dimw                    | structured   | (introduced in 2.4)                                                                     |
{{</table>}}





## Supported Entities and Iterators

{{<table>}}
| Name                          | Element types                      | Entities for codim | Iterators for codim |
| ---                           | ---                                | ---                | ---                 |
| `AlbertaGrid<dim,dimw>`       | simplices                          | all                | all                 |
| `ALUGrid<dim,dimw,simplex,*>` | simplices                          | all                | all                 |
| `ALUGrid<dim,dimw,cube,*>`    | cube                               | all                | all                 |
| `GeometryGrid<HostGrid>`      | =host                              | all                | all                 |
| `OneDGrid`                    | lines                              | all                | all                 |
| `UGGrid<dim>`                 | simplices, prisms, pyramids, cubes | all                | 0, dim              |
| `SPGrid<ct,dim,rs,comm>`      | cubes                              | all                | all                 |
| `YaspGrid<dim>`               | cubes                              | all                | all                 |
{{</table>}}

## Refinement

{{<table>}}
| Name                                      | Refinement  | Strategy                    | conforming? | boundary adaptation |
| ---                                       | ---         | ---                         | ---         | ---                 |
| `AlbertaGrid<dim,dimw>`                   | adaptive    | recursive bisection         | yes         | yes                 |
| `ALUGrid<dim,dimw,simplex,conforming>`    | adaptive    | bisection                   | yes         | yes                 |
| `ALUGrid<dim,dimw,cube,nonconforming>`    | adaptive    | red                         | no          | yes                 |
| `ALUGrid<dim,dimw,simplex,nonconforming>` | adaptive    | red                         | no          | yes                 |
| `GeometryGrid<HostGrid>`                  | =host       | =host                       | =host       | =host               |
| `OneDGrid`                                | adaptive    | ?                           | yes         | --                  |
| `UGGrid<dim>`                             | adaptive    | red/green                   | selectable  | yes                 |
| `SPGrid<ct,dim,rs,comm>`                  | global only | bisection, red, anisotropic | yes         | no                  |
| `YaspGrid<dim>`                           | global only | 2<sup>dim</sup> children    | yes         | no                  |
{{</table>}}

## Parallel Features

{{<table>}}
| Name                              | parallel? | overlap? | ghosts? | communicate() for codim | dynamic load balancing |
| ---                               | ---       | ---      | ---     | ---                     | ---                    |
| `AlbertaGrid<dim,dimw>`           | no        | --       | --      | --                      | --                     |
| `ALUGrid<2,dimw,*,*>`             | no        | --       | --      | --                      | --                     |
| `ALUGrid<3,3,*,nonconforming>`    | yes       | no       | yes     | all                     | yes                    |
| `ALUGrid<3,3,simplex,conforming>` | yes       | no       | no      | all                     | yes                    |
| `GeometryGrid<HostGrid>`          | =host     | =host    | =host   | =host                   | =host                  |
| `OneDGrid`                        | no        | --       | --      | --                      | --                     |
| `SPGrid<ct,dim,rs,comm>`          | yes       | yes      | no      | all                     | no                     |
| `UGGrid<dim>`                     | yes       | no       | yes     | all                     | yes                    |
| `YaspGrid<dim>`                   | yes       | yes      | no      | all                     | no                     |
{{</table>}}
