+++
title = "Guides"
[menu.main]
name = "Guides"
identifier = "guides"
parent = "docs"
weight = 0
+++

* [Repository layout](/dev/repository_layout)
* [Coding style](/dev/codingstyle)
* [Whitespace hook](/dev/whitespace_hook)
* [Git best practices](/dev/git_best_practices)
* [Adding Python support](/dev/adding_python)
