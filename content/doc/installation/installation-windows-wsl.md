+++
title = "Installation of DUNE on Windows systems via WSL"
[menu.main]
parent = "installation"
weight = 2
name = "Windows (WSL)"
+++

### WSL setup

Windows users should use the [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/install) and install the latest Ubuntu LTS on it.
Once Ubuntu is installed proceed with installing the [Dune binary packages](/doc/installation/installation-deb)
or by using [pip and the packages from PyPi](/doc/installation/installation-pip) or by [installing from source](/doc/installation/installation-buildsrc).

In order to connect graphical output of the WSL to the Windows desktop
one has to modify the `.bashrc` file in the home directory inside the WSL.
Simply use `nano` to edit the file:

```
nano $HOME/.bashrc
```

Scroll to the end of this file and add these two lines:

```
export DISPLAY=$(/sbin/ip route | awk '/default/ { print $3 }'):0
export LIBGL_ALWAYS_INDIRECT=1
```

Save with `Ctrl-S` and quit the editor with `Ctrl-X`.

To activate this new configuration the WSL should simply be restarted (close the terminal and start again).

For older versions of WSL (version 1) you should also install a X-server (e.g. [VcXsrv](https://sourceforge.net/projects/vcxsrv/))
in order to connect graphical output from the WSL Ubuntu system to your Windows desktop.

### VcXsrv setup

__Note:__ This setup of VcXsrv seems not necessary on Windows 11 clients.
You still might have to adjust your .bashrc file (see above).


When using the Windows Subsystem for Linux one may wants to
install a X-server for connecting the graphical output of the WSL
to the Windows desktop. [VcXsrv](https://sourceforge.net/projects/vcxsrv/) has been reported to work fine.
Once installed there should be a program called `XLaunch`.

After installation there are a couple of configuration tweaks that have
to be done in order for it to work correctly. When using `XLaunch` for the
first time select `Multiple Windows`, then `Start no client`, then `All extra settings` and `Disable access control`.
After this save the configuration somewhere easily accessible, e.g. on the desktop.

Double clicking on the configuration file will start a client. After starting a
client there should be a firewall message. `Private and global access` for VcXsrv has to be granted.

The last step is to adjust the configuration inside the WSL Linux system (see above).
