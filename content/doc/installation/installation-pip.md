+++
title = "Installation Dune and its Python bindings from PyPI"
[menu.main]
parent = "installation"
weight = 3
name = "From PyPI"
+++

We have added installation support of Dune modules via `pip` using the Python package
index (PyPI). The module and the modules it depends on are downloaded, build, and
installed either into the system or into a python virtual environment (the latter
might be of more interest as it is an option to install Dune without root privileges).
This could also be of interest for C++ users of Dune as Python bindings are not mandatory.
Scripts like `dunecontrol` and `duneproject` can be used the same way as with a system installation so that
installed and source modules can easily be used together.

Starting with the 2.8 release, we are planning to upload stable release
versions as well as regular developer snapshots of all core modules. That
should make it easy for users to work with an installed Dune which can be
updated to a developer version if a newer feature is required.

Installing the main core modules (`common`, `geometry`, and `grid`) can be
easily installed into a virtual environment without requiring root
privileges by running

```
python3 -m venv ~/dune-env
source ~/dune-env/bin/activate
pip install dune-grid
```

You should now be able to test with one of your own Dune modules or you can run
a Python test:

```
python -m dune.grid
cd grid_tutorial
python example.py
```

Simply remove `~/dune-env` to get rid of everything.

To install the most recent development snapshot simply run
```
pip install --pre dune-grid
```

All `core` modules are available on the Python Package Index (PyPI), as well
as some additional grid managers (e.g. `pip install dune-alugrid`),
and the discretization module `dune-fem`.

We have tested on a number of Linux distributions and on MacOS.
On Windows follow the [WSL installation](/doc/installation/installation-windows-wsl)
and instead of installing the binary packages use `pip` as described above.
Please report if you encounter any problems with the setup.


### Install from the Git repository

In addition to the stable release packages on PyPI, it is possible to install the current development version from the Git repository via `pip`.
This will also install the current Git versions of all dependent Dune modules.

For example, you can install `dune-grid` via

````
python3 -m venv ~/dune-env
source ~/dune-env/bin/activate
pip install dune-grid@git+https://gitlab.dune-project.org/core/dune-grid.git
````

It is also possible to run
```
pip install .
```
within a cloned Git repositroy of some Dune module to download and install all dependent module. After that 
```
dunecontrol --current all
```
can be used to configure and build the source module and replace the installed python package with a editable source version.


### Known issue

If some software component was found through the environment variable `CMAKE_PREFIX_PATH`
then there might be problems later when loading some shared libraries requiring to add those paths also the `LD_LIBRARY_PATH`.
