+++
title = "Building the Python bindings from source"
[menu.main]
parent = "installation"
weight = 4
name = "Building the Python bindings"
+++

Starting with DUNE 2.9 the Python support is enabled by default.
The DUNE build system will try to set up an internal virtual environment during the configuration of DUNE.
This provides an easy way to use the Python bindings of DUNE based on a source build.

__Note:__ Installation for DUNE versions prior to 2.9 works differently. See the next section but one.

# Building the Python bindings for Dune 2.9 and later

The setup of the virtual environment will happen in the build directory of `dune-common`.
This only succeeds if the python virtual environment capabilities are installed, e.g., for Debian `python3-venv` needs to be installed.
The internal virtual environment can be used by calling
````
./dune-common/build-cmake/run-in-dune-env python [...]
````
or, it can be activated by
```
source dune-common/build-cmake/dune-env/bin/activate
```

Alternatively, a custom virtual environment can be activated before configuring DUNE that will replace the internal virtual environment.

Note: When loading the first module in python, the `dune-py` module will be generated automatically to perform the necessary just-in-time compilation.
The just-in-time (JIT) compilation is carried out in a newly created DUNE module `dune-py`. 
This module is automatically set up the first time a DUNE package is imported in python.
This module has a hard dependency on all DUNE modules found. 
As a consequence all these modules need to be in a working condition, e.g., correctly configured and built.
It can also be configured manually by
````
./dune-common/bin/setup-dunepy.py --opts=config.opts
````

## Testing
Now you should be able to use the python bindings. You can test them with the following examples.

The first example only needs `dune-common` to be installed.
First, we test with a short Python script in an environment variable.
```bash
testScript="\
from dune.common import FieldVector ;\
print(\"START\") ;\
x=FieldVector([1,2]) ;\
print(\"END\") ;\
"
```
and this can be executed via `python -c "$testScript"`.

The output should be similar to
```bash
START
DUNE-INFO: Generating dune-py module in /dune/dune-common/build-cmake/dune-env/.cache/dune-py
DUNE-INFO: Compiling FieldVector (new)
END
``` 

If you have `dune-grid` installed you can also check

```bash
testScript="\from dune.grid import structuredGrid ;\
grid = structuredGrid([0,0],[1,1],[10,10]) ;\
print( grid.size(0) ) # should return 100;\
"
```
## Embedded Python
The Python bindings are based on the [pybind11](https://github.com/pybind/pybind11)
library. Hence, they also allow to embed Python in your Dune application.
Follow the [pybind11 instructions](https://pybind11.readthedocs.io/en/stable/advanced/embedding.html) to see how this can be done.
However, you cannot link against `pybind11::embed` in CMake,
but you have to link to the Python library manually using:
```
if(DUNE_ENABLE_PYTHONBINDINGS)
  add_executable("application" application.cc)
  target_link_dune_default_libraries("application")
  target_link_libraries("application" PRIVATE Python3::Python)
 endif()
```
Here `if(DUNE_ENABLE_PYTHONBINDINGS)` prevents your application from being built
if the Python bindings are not enabled. Note that the Python bindings for Dune C++ types can also be used in an embedded Python context.

# Building the Python bindings for Dune 2.8 and 2.7

__Note__: The steps described in this document are no longer necessary for DUNE 2.9 and later. The testing scripts described above still apply.

If you use the DUNE core modules (resp. `dune-common`) at 2.8, the setup of the python bindings requires a few more steps.
For the 2.7 release the steps are very similar except that an additional module [dune-python](/modules/dune-python) is required containing the scripts described below,
i.e., instead of `dune-common/bin/setup-dunepy.py` use `dune-python/bin/setup-dunepy.py`.


## Virtual environment

First of all, we recommended to activate a custom virtual environment before installing the python bindings.
Otherwise, DUNE will be installed into your system - there is no internal environment setup available with the 2.8 version.
````
python3 -m venv dune-env
source dune-env/bin/activate
````

## Building the Python bindings

All DUNE modules have to be configured with python bindings enabled.
A quasi-minimal `config.opts` file looks like:
```
CMAKE_FLAGS=" \
  -DCMAKE_BUILD_TYPE=Release \
  -DDUNE_ENABLE_PYTHONBINDINGS=ON \
  -DADDITIONAL_PIP_PARAMS='-upgrade' \
  -DBUILD_SHARED_LIBS=TRUE \
"
```
These flags will enable the python bindings and install the python packages in editable mode.
For details on how to build DUNE see the [installation instructions](/doc/installation).

Configure your DUNE modules
```
./dune-common/bin/dunecontrol --opts=config.opts all
```
and install the python packages
```
./dune-common/bin/dunecontrol make install_python
```

This will run `pip install` in the folders `build-cmake/python` of all DUNE modules.

If you used `dunecontrol` with a customized `config.opts` you can use the same file to also build the `dune-py` module by running
```
./dune-common/bin/setup-dunepy.py --opts=config.opts
```
after running `dunecontrol`. 
This will lead to the generation of a new `dune-py` module to perform the just-in-time compilation when loading the first modules. 
The default location is in `.cache/` located either in the root folder of the virtual environment if one is active or in the user's home directory.

It is also possible to have `dune-py` generated automatically, i.e., without calling `setup-dunepy.py`  but you might need to set some environment variables before running a python script. 
These are:
```
# path to the DUNE source modules (colon separated list, defaults to `.`)
DUNE_CONTROL_PATH=DUNE_SOURCE_PATH

# the build dir used for `dunecontrol` (defaults to `build-cmake`)
DUNE_BUILDDIR=BUILDDIR_PATH

# location of new `dune-py` module (defaults to [virt-env-path]/.cache)
DUNE_PY_DIR=DUNEPY_PATH

# possible cmake flags to use, e.g., compiler flags (shared libs are activated by default)
DUNE_CMAKE_FLAGS=CMAKE_FLAGS
```

## The dune-py module
The just-in-time (JIT) compilation is carried out in a newly created DUNE module `dune-py`. 
This module is automatically set up the first time a DUNE package is imported in python.
This module has a hard dependency on all DUNE modules found. 
As a consequence all these modules need to be in a working condition, e.g., correctly configured and build.
The `dune-py` module is (per default) created in a folder `.cache` in the current Python virtual environment. 
The location of the module can be changed by setting the environment variable `DUNE_PY_DIR`. 
Since the module is created on the first import it is possible to set the directory for a single test/script, e.g., `DUNE_PY_DIR="." ./my-python-test.py` to create it in the same folder. 
This might be useful for developers or if you want to make sure that all JIT-compiled modules get rebuilt.
