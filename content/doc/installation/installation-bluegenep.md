+++
title = "Installation Notes for Blue Gene P"
+++
### Installation Notes for Blue Gene P
On IBM's Blue Gene System P the system and architecture (like [Jugene in Jülich](http://www.fz-juelich.de/jsc/jugene) of the front node (where one can login and compile stuff) and the compute node (where the parallel problem will run) are different from each other. Therefore one has to facilitate the auto-tool-chain's cross compilation powers. This is a little bit tricky and it took me (Markus Blatt) quite some time to compile DUNE in a way that it is reliably working. Thanks to IBM's support I got it working now.

In addition to the normal [Installation Notes](/doc/installation) you need the following special options to compile and link DUNE on Blue Gene P.

On the jugene all the tools needed for the cross compilation are available in the directory `/bgsys/drivers/ppcfloor/gnu-linux/bin/` We therefore we need to add it to our PATH:
```
export PATH=/bgsys/drivers/ppcfloor/gnu-linux/bin/:$PATH
```

We need to tell configure that we want to cross compile using the
```
--host
```

option. Furthermore we certainly want to enable the parallel DUNE using the

```
--enable-parallel
```

option. As all the tests take place on the front node we cannot start any parallel program and therefore disable the tests with the
```
--disable-mpiruntest
```

option.
Jugene uses static linkage by default. Unfortunately this seems not to work out of the box with autotools-chain. If one tries to configure and compile stuff with the default options libtool gets messed up like this when linking to libdunecommon:
```
checking for dune-common installation or source tree...     found in /homeb/jicg42/jicg4201/DUNE/dune-common
checking dune/common/stdstreams.hh usability... yes
checking dune/common/stdstreams.hh presence... yes
checking for dune/common/stdstreams.hh... yes
checking for libdunecommon... no
```
configure: WARNING: `/homeb/jicg42/jicg4201/DUNE/dune-common does not seem to contain a valid dune-common (failed to link with libdunecommon.la)`

A closer look into config.log reveals the actual problem:

```
configure:22583: ./libtool --tag=CXX --mode=link powerpc-bgp-linux-g++ -I/bgsys/local/libtiff/v3.8.2_gcc/include -I/bgsys/local/libjpeg/v6b_gcc/include -I/bgsys/drivers/ppcfloor/arch/include/ -DNOOUTPUT -g -O0 -DNDEBUG -Wall  -L/bgsys/local/libtiff/v3.8.2_gcc/lib/ -L/bgsys/local/libjpeg/v6b_gcc/lib -o conftest -I/bgsys/local/libtiff/v3.8.2_gcc/include -I/bgsys/local/libjpeg/v6b_gcc/include -I/bgsys/drivers/ppcfloor/arch/include/ -DNOOUTPUT -g -O0 -DNDEBUG -Wall  -I/homeb/jicg42/jicg4201/DUNE/dune-common  -L/bgsys/local/libtiff/v3.8.2_gcc/lib/ -L/bgsys/local/libjpeg/v6b_gcc/lib  -L/homeb/jicg42/jicg4201/DUNE/dune-common/lib conftest.cc  -ldunecommon>&5
mkdir .libs
powerpc-bgp-linux-g++ -I/bgsys/local/libtiff/v3.8.2_gcc/include -I/bgsys/local/libjpeg/v6b_gcc/include -I/bgsys/drivers/ppcfloor/arch/include/ -DNOOUTPUT -g -O0 -DNDEBUG -Wall -o conftest -I/bgsys/local/libtiff/v3.8.2_gcc/include -I/bgsys/local/libjpeg/v6b_gcc/include -I/bgsys/drivers/ppcfloor/arch/include/ -DNOOUTPUT -g -O0 -DNDEBUG -Wall -I/homeb/jicg42/jicg4201/DUNE/dune-common conftest.cc  -L/bgsys/local/libtiff/v3.8.2_gcc/lib/ -L/bgsys/local/libjpeg/v6b_gcc/lib -L/homeb/jicg42/jicg4201/DUNE/dune-common/lib /homeb/jicg42/jicg4201/DUNE/dune-common/lib/.libs/libdunecommon.a /bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/powerpc-bgp-linux/lib/libstdc++.so -L/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/toolchain/gnu/build-powerpc-bgp-linux/gcc-4.1.2-build/powerpc-bgp-linux/libstdc++-v3/src -L/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/toolchain/gnu/build-powerpc-bgp-linux/gcc-4.1.2-build/powerpc-bgp-linux/libstdc++-v3/src/.libs -L/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/toolchain/gnu/build-powerpc-bgp-linux/gcc-4.1.2-build/./gcc -L/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/powerpc-bgp-linux/bin -L/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/powerpc-bgp-linux/lib -L/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/lib/../powerpc-bgp-linux/lib -lc -lgcc_s -lm -Wl,--rpath -Wl,/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/powerpc-bgp-linux/lib -Wl,--rpath -Wl,/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/powerpc-bgp-linux/lib
/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/lib/gcc/powerpc-bgp-linux/4.1.2/../../../../powerpc-bgp-linux/bin/ld: attempted static link of dynamic object `/bgsys/drivers/V1R3M0_460_2008-081112P/ppc/gnu-linux/powerpc-bgp-linux/lib/l
ibstdc++.so'
collect2: ld returned 1 exit status
configure:22589: $? = 1
```

To get rid of this one needs to force shared linkage within the compiler flags using the
```
-dynamic
```

compiler option for the Darwin linker.
To pass all these options to DUNE one can use the following options file:
```
CONFIGURE_FLAGS="--prefix=$HOME/dune_install --disable-documentation --disable-mpiruntest --enable-parallel --host=powerpc-bgp-linux  --without-x CXXFLAGS=\"-I/bgsys/drivers/ppcfloor/arch/include/ -DNOOUTPUT -g -O3 -dynamic -DNDEBUG -Wall\" CFLAGS=\"-I/bgsys/drivers/ppcfloor/arch/include/ -DNOOUTPUT -g -O3 -dynamic -DNDEBUG -Wall\"\""
MAKE_FLAGS="all"
```
