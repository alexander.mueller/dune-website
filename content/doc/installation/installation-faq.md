+++
title = "Installation FAQ"
[menu.main]
parent = "installation"
weight = 4
name = "Installation FAQ"
+++

Here is a list of some frequently asked questions regarding the installation of Dune.

### My System does not provide the required minimal compiler version. Where can I find updates?
This heavily depends on your system. For Debian/Ubuntu based Linux distributions, sources are:

- PPA for recent gcc/g++ compilers for various ubuntu distributions: https://launchpad.net/~ubuntu-toolchain-r/+archive/ubuntu/test
- APT repository for clang (debian and ubuntu packages): https://apt.llvm.org

Follow the instruction on the referenced pages.

### How can I update my system CMake version?
Similar to the compilers, it depends on your distribution:

- APT repository from Kitware: https://apt.kitware.com
- Snap package: https://snapcraft.io/cmake (for RHEL, see https://snapcraft.io/install/cmake/rhel)
- Using PIP: `python -m pip install cmake`
- docker container including CMake: e.g. https://registry.dune-project.org/docker/ci/ubuntu:20.04 or https://registry.dune-project.org/docker/ci/debian:10
- Binary from Kitware website: https://cmake.org/download + add `CMAKE="YOUR_PATH/bin/cmake"` to your options file.
- Compile CMake from source: https://cmake.org/download, using your old CMake.

### My dune packages are located in different directories. How can I find them using dunecontrol?
The `dunecontrol` script searches in the current directory and all its subdirectories for files name `dune.module`.
Those files contain the information about the corresponding dune module and its dependencies. Additionally,
`dunecontrol` searches in all directories (and subdirectories of those) listed in the variable
`DUNE_CONTROL_PATH`. Similarly to the `PATH` variable, directories are separated by colon `:`.

You can either prefix the call of `dunecontrol` to individually specify the search paths, i.e.,
```
DUNE_CONTROL_PATH=path/to/module1:/path/to/module2 dunecontrol [...] COMMAND
```

or you can put this path information into an options file, next to the other options like `CMAKE_FLAGS` or
`MAKE_FLAGS`.
