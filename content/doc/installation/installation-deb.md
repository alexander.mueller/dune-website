+++
title = "Installation of Dune from binary packages on Debian and Ubuntu systems"
[menu.main]
parent = "installation"
weight = 1
name = "Debian and Ubuntu"
+++

We assume that you are running a freshly installed Ubuntu 20.04 LTS
with no additional packages installed yet.


## Installing Dune core modules
To now install the precompiled main Dune packages run the following commands:

```
sudo apt install libdune-grid-dev
sudo apt install libdune-uggrid-dev
sudo apt install libdune-istl-dev
```

You need root access in order to execute this command. The sudo
command will ask for your password and you need to be in the list of
sudoers (which is automatically the case for the user who has been
created when installing Ubuntu).

The command installs libraries and include files at appropriate places in the
system. Since Dune is a software framework that allows you to write
your own applications in C++ we also need some source code to
compile.

## Installing more Dune modules
There are a few more modules available in Debian based system,
e.g. you can run the following to install the
[dune-pdelab](/modules/dune-pdelab) and 
[dune-functions](/modules/dune-functions) package:
```
sudo apt install libdune-functions-dev
sudo apt install libdune-pdelab-dev
```

## Further tools
Most likely you will want to also install the following packages to
be able to build own code, visualize results and generate unstructured meshes
```
sudo apt install git
sudo apt install cmake
sudo apt install gmsh
sudo apt install paraview
```
