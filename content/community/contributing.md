+++
title = "Getting involved"
[menu.main]
identifier = "contributing"
parent = "community"
weight = 1
+++

## How to Contribute
DUNE is a community project and we are happy about all forms of external contributions. There are many easy things that you can do, like

* read the documentation and tell us where and how it should be improved,
* try to install DUNE on your platform and report bugs if it doesn't work,
* fix bugs and send us patches.

If you decide to contribute code please head to our [GitLab Instance](http://gitlab.dune-project.org/) and get an account there. Please read the contribution guide shown by GitLab in all the core modules.

### Google Summer of Code
In 2013 Dune participated in GSoC as a mentoring organization for the first time.

In 2016, we were selected a second time. The 2016 projects were:

* Xinyun is working on a grid with spline geometries using B-spline and NURBS
  geometries.
  Checkout the
  [screenshots](http://gsoc2016xinyun.blogspot.com/2016/06/b-spline-and-nurbs-geometry-interface_19.html)
  with according surfaces.
  Usually we don't see such smooth output with our methods.
  Blog: [http://gsoc2016xinyun.blogspot.de](http://gsoc2016xinyun.blogspot.de)
  Code repository: [https://gitlab.dune-project.org/Xinyun.Li/dune-iga](https://gitlab.dune-project.org/Xinyun.Li/dune-iga)
* Michael is developing Python bindings for the DUNE grid interface.
  As an example, he implemented a
  [cell-centered finite volume in Python](http://misg.github.io/gsoc2016/dune/2016/06/05/two-weeks-later),
  inspired by the corresponding example in the DUNE grid how-to, using his bindings.
  Blog: [http://misg.github.io/](http://misg.github.io)
  Code repository: [https://gitlab.dune-project.org/michael.sghaier/dune-corepy](https://gitlab.dune-project.org/michael.sghaier/dune-corepy)

Both projects were successful. We greatly appreciate the work put into our code base
by the GSoC students.

Information on how [to apply](/dev/gsoc) as a GSoC student is found [here](/dev/gsoc). 

### Project Ideas
The following is a list of things that we find interesting but that we probably will never have the time to do ourselves.

#### Additional Features

##### dune-common

* Improve the CMake-based build system to enable superbuilds. This would replace the per-module build and, instead, build all modules together.  
Prior to application: Some initial code for superbuild  
175 h project: Figure out superbuild, implement CMake and duneproject part, get it running for all core modules

#### dune-geometry

* Implement a collection of the latest published quadrature rules. It provides a portable and high-precision data format and an export utility to generate the quadrature code for any library, e.g. for dune-geometry. Additionally, the code should be able to increase the quadrature precision, using existing values as initial data for a nonlinear solver implemented with arbitrary precision arithmetic. The existing quadrature rules are old and outdated. Even for 2d and especially for 3d rules with much lower number of points are available. Additionally, the library could provide extra information about the location and properties of the quadrature points, e.g., all inside the element, or points on the boundary or positive/negative weights, or symmetry properties of the points...  
Prior to application: Write a first quadrature rule.  
Prior to application: Read quadrature rule papers and share your insights.  
175 h / 350 h project: Depending on how many quadrature rules and the actual design of the project, both sizes are possible.

##### dune-ISTL

* Write an improved parallel ILU preconditioner, following the approach proposed in http://www.cc.gatech.edu/~echow/pubs/parilu-sisc.pdf  
175 h project

* Implement an incomplete Cholesky preconditioner, to improve efficiency for symmetric matrices.  
Prior to application: Get yourself known with Cholesky decomposition, implement it with GNU Octave or NumPy.  
175 h project

* Design and implement a special matrix class, to store symmetric matrices, reducing the memory footprint.
Prior to application: Implement some sparse or symmetric matrix format using GNU Octave or NumPy.  
175 h project

##### dune-grid

* Improve efficiency to the new virtual grid interface and provide Python bindings for it.
See [GitLab dune-grid!573](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/573).  
Prior to application: Read some of the Python and CMake code and if you find something to be improved, open an issue or a merge request  
175 h project

* VTK can visualize higher order elements and our export could support this feature.  
Prior to application: Investigate some code for data and geometry [dune-vtk](https://gitlab.dune-project.org/extensions/dune-vtk)  
175 h project: Integrate into dune-grid

* dune-gmsh: During the development of dune-curvedgrid Florian Stenger and Simon Praetorius have written a new GMSH reader for the version 4 format. In this context there was the idea to implement a reader that can read distributed grids including the partitioning information from the gmsh files. The file format provides lots of information and we currently only read a small subset. Additionally, the encoding of boundary data is changed compared to the old format. The project would be about reading as much information as possible from the gmsh file format and provide a way to construct grids from this. Currently it is possible to construct curved grids automatically.  
Prior to application: Investigate existing code from [dune-curvedgrid](https://www.dune-project.org/modules/dune-curvedgrid/)  
175 h or 350 h project: Integrate into dune-grid

#### Documentation
Tutorials are a good way to get started with Dune. A good tutorial for dune-istl would be of great help.  
Prior to application: Read existing documentation. If you find outdated stuff, propose updates.  
175 h project: Write a tutorial with example programs that cover the different aspects of ISTL.

#### Infrastructure

* Invent a mechanism that extracts all finite element implementations from dune-localfunctions (plus their doxygen documentation), and renders them into a LaTeX document. This document should then in turn become a reference section in the dune-localfunctions tutorial. It seems that the doxygen-LaTeX functionality is too limited for this. You may have to play around with the XML feature of doxygen. See the brief discussion in .

#### Additional Grid Backends

* Write a wrapper for the [Racoon II](http://www.tp1.ruhr-uni-bochum.de/~jd/racoon/) grid manager. This is a parallel AMR grid manager for 2d and 3d.
* Write a wrapper for the [AmRoc](http://amroc.sourceforge.net/) grid manager. This is another AMR manager. Some initial work has been done. Ask on the mailing list for details.
* Write a wrapper for [p4est](https://www.p4est.org) grid manager. There was some work done, that could serve as a starting point.  
Prior to application: Some code already exists at [dune-p4estgrid](https://gitlab.dune-project.org/robert.kloefkorn/dune-p4estgrid). Have a look and try to get it running. Merge requests are welcome.  
350 h project
* Write a parallel AMR grid manager that works in more than three space dimensions.
* Write a structured simplex grid for rectangular domains. 2d and 3d would be great, the brave can implement Freudenthal's algorithm for a general n-d simplex grid.  
350 h project
* Implementing a serialization for UG grid hierarchies.

#### Meta Grids
Meta grids are DUNE grids which are parametrized by another grid. An example is SubGrid, which allows you to select a subset of elements of a DUNE grid and treat this subset as a grid in its own right. Here are a few other ideas for meta grids which may be useful.

* Write a meta grid which, given an affine grid with a parametrized boundary, yields an isoparametric approximation of the boundary.  
Prior to application: Have a look at [dune-curvedgrid](https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgrid) meta grid, that comes handy for this project.  
350 h project: You just need to provide the mapping that results in curved boundaries while leaving the rest untouched. It has be done by the use of Lobatto shape functions, with high polynomial degree only on the boundary faces and order 1 everywhere else.

* A proper periodic grid-wrapper: There are some old implementation that allow to wrap GridParts of dune-fem and also a sketch on a meta-grid based on dune-grid, but nothing is finished yet. The idea is to have a meta-grid plus some association/geometry-transformation that wraps an existing grid and transforms the index-set such that associated entities give the same index. For non-parallel grids this is not so complicated but technical, but for parallel grids it is not so trivial. Either the domain partitioning is controlled to guarantee that associated entities are always on the same partition, or a virtual inner interface must be established to have communication of these entities in a domain-decomposition sense.
