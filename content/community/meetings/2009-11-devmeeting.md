+++
title = "DUNE Develeoper Meeting November 2009"
+++

Major Topics
------------

### Boundary/GridFactory

#### index translation for coarse grid creation

*Responsible:* Andreas, Martin

*Status:* Interface updated, supported in AlbertaGrid

Data stored in files must be associated with entities in a grid-file.
The GridFactory must provide the necessary infrastructure…

Two proposals:

1.  <div class="li">

    level-0 elements/vertices/(boundary-segments) are numbered in the
    order of insertion (only level-0-indexSet).

    </div>

2.  <div class="li">

    add some kind of indexSet to translate between insertionIndex and
    entityIndex (methods on the grid factory).

    </div>

Both indices are only valid until you first modify the grid (e.g.
refine, load balance).

Arguments:

-   \(1) is easier fo the user
-   \(1) principle of least surprise
-   \(1) helps for debugging
-   \(1) requires changes to the grid managers
-   \(2) imposes less restrictions on future grids
-   \(2) more efficient (for certain grids) during computation
-   \(2) requires reorder of data during setup phase

Vote:

1.  insertion order: 5
2.  indexSet: 6
3.  abstained: 3

#### boundary IDs/indices

*Responsible:* Andreas, Martin

*Status:*

The user must be able to attach boundary information to the level-0
boundaries.

1.  Up to now the boundaryIDs allowed to associate an int with a
    boundary segament.
2.  The new proposal is to have an index with a boundary index. This
    is (more) consistent with the other indices and allows to associate
    more or less any data with boundaries. The number would be inherited
    from the coarse grid.

Vote:

1.  coarse boundary index: 11
2.  boundaryIDs: 0
3.  abstained: 3

Additional details:

 -   `bool wasInserted(Level-0-Intersection)`
 -   `boundarySegmentIndex()` replaces `boundaryID()` on the intersection

#### BoundarySegment vs. BoundaryProjection

*Responsible:*

*Status:*

Refinement towards the boundary. You have to project newly inserted
vertices onto the boundary.

Two options:

1.  map from local to global coordinates (i.e. a parametrization of the
    boundary in coarse grid local coordinates)
2.  map from global to global coordinates (i.e. a geometric projections)

Arguments:

-   \(1) is safer for generic CAD data
-   \(2) can fail for arbitrary boundaries
-   both approaches are equivalent in the sense that it is possible to
    compute local from global coordinates and the other way around
-   \(2) might lead to epsilon problems

Votes:

-   local to global: 14
-   global to global: 0

Additional Information:

-   on insertBoundarySegment the parametrization of a boundary is
    optional (null-pointer == no parametrization, null is default)
-   coherent handling of parametrizations in grids which don't support
    it … gridFactory should through an exception.
-   BoundarySegmentPointer is a shared\_ptr&lt;…&gt;
-   we might come back to this point when discussing moving boundaries

#### parametrized elements

*Responsible:*

*Status:*

Oliver proposes to add a parametrization for elements.

-   add “local to global” parametrization as an optional parameter to
    the insertElement method.
-   BoundarySegment should be renamed

Votes: agreed

### new dune-geometry module

*Responsible:* Joe

*Status:*

Proposal
([FS\#604](http://www.dune-project.org/flyspray/index.php?do=details&task_id=604 "http://www.dune-project.org/flyspray/index.php?do=details&task_id=604"),
[FS\#538](http://www.dune-project.org/flyspray/index.php?do=details&task_id=538 "http://www.dune-project.org/flyspray/index.php?do=details&task_id=538")):

1.  a new DUNE module for geometry related components
2.  shall contain GeometryType, (Generic)Geometries,
    GenericReferenceElements, QuadratureRules, VirtualRefinement
3.  the Geometry facade class stays, an interface is introduced in
    dune-geometry and used in dune-grid
4.  not necessarily right now, but once someone finds time.

Vote:

-   leave it as it is: 3
-   move the necessary stuff to dune-common: 0
-   create the proposed new module: 6
-   abstained: 3

### dune-localfunctions

#### <span id="naming_convention_directory_structure"></span>naming convention/directory structure

*Responsible:*

*Status:*

name of the module/include
1.  local functions (5 votes)
2.  finite elements (2 votes)

rename the directory structure to dune/localfunctions/

how are associated local functions related to different element types
stored in the directory hierarchy

-   different elementtypes/dimensions in one file/directory
-   one subdirectory for each header named “header-name minus dot-hh”
-   files in these subdirectories are internal headers and should not be
    used directly

#### static/virtual interface

*Responsible:* Oliver

*Status:*

Proposed solutions:

1.  (only) pure virtual methods (0 votes)
2.  static interface + virtual wrapper (4 votes)

Documentation of the virtual-wrapping-thing is missing.

For the virtual interface the interpolation should be based on the
Fuvtion interface from dune-common.

For the virtual interface the CoefficientType must be specified. It
should be template parameter. For the static interface it stays a
parameter of the interpolation.

#### which static polymorphism to use?

*Responsible:* Oliver

*Status:*

1.  “Barton-Nackmann” (CRTP) approach (1 vote)
2.  stl-like engine approach (7 votes)

specialization for the engine/capabilities approach:

```
void foo(typename enable_if<hasCapabilityA::value,Type>::type);
void foo(typename enable_if<!hasCapabilityA::value,Type>::type);
```

#### When to release?

-   the module should published with the next dune release
-   if the module is in the right shape it becomes a core module
-   otherwise it is published as a technology preview

### Generalized Entity types (Geometry Types, Reference Elements, ...)

*Responsible:*

*Status:*

see Sintef, [FS\#449](http://www.dune-project.org/flyspray/index.php?do=details&task_id=449 "http://www.dune-project.org/flyspray/index.php?do=details&task_id=449")

proposed changes:

1.  add a GeometryType::??? BasicType (agreed) none (8), general (6),
    arbitrary (4), unknown (1) … New name is “none”
2.  add a center/centroid() method for the center/centroid of the cell,
    exact semantics are discussed later (agreed)
3.  add unitOuterNormal() without local coordinate returning
    unitOuterNormal at center/centroid() (pro 11, con 1)
4.  make arbitrary intersections return “none” (postponed)

New DUNE release (2.0)
----------------------

*Responsible:* Christian, Martin D.

*Status:*

-   target end of feb. 2010
-   show-stoppers: todays decisions (GridFactory, Geometry-stuff,
    automated-tests, flyspray)
-   release manager: Christian (buildsystem), Martin Drohmann
-   automated-tests: running, more test-systems needed
-   flyspray: homework
-   supported platforms: linux >= gcc 3.4.1, not 4.0, 4.1, 4.2, 4.3,
    4.4, linux >= intel (7? or an other one), mac, libtool 1.5 & 2.x,
    autoconf/make (>= ??)
-   try to speedup the dune-grid tests, leave out the test-programs
-   Christian changes the buildsystem so that “make all” doesn't build
    the documentation. The user has to call “make doc”.
-   SVN access for Martin Drohmann (Mario is the sponsor)
-   Vote via mail: Should Jö become core developer?

Medium Topics
-------------

### UG license issues

**SKIPPED**

### Extension of grid interface

**SKIPPED**

-   node→element relation
-   EntityKeys
-   Random entity access
-   Richer interface for codim>0 entities

### Semantics of certain interfaces/classes

-   Ghost\_Partitition (discussion postponed, via email)
-   IDs: are they unique across codims? Not necessarily (Oliver updates
    the docs)
-   What is the purpose of the PartitionType parameter of a GridView?
    (remove PartitionType-parameter, keep size-method)
-   GridViews: Do they stay valid after refinement? GridViews and
    IndexSets are not valid after a grid change (adapt, loadbalance)

### strategies for changes to core modules

-   “Vorwarnzeit”

### Use of tr1 c++0x stuff

-   `Dune::array`: we agreed on the following: Dune::array will only substituted by std::array, but not by std::tr1::array. That way we avoid the aligmnent bug in std::tr1::array which is present in gcc 4.0, 4.1, 4.2. FieldVector can then safely inherit from Dune::array.
