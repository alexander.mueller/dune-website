Protocol of the Dune Developer Meeting 2014
===========================================

Participants
------------

Markus Blatt\
Ansgar Burchardt\
Andreas Dedner (partially via Skype)\
Christian Engwer\
Jö Fahlke\
Christoph Gersbacher\
Carsten Gräser\
Christoph Grüninger\
Dominic Kempf (Protocol)\
Steffen Müthing\
Martin Nolte\
Oliver Sander

(9 of 12 core developers present)

Reflection of the 2013 developer meeting
----------------------------------------

Generally, the largest part of decisions has been implemented. The
following topics were not:

-   C++11 specific stuff isn't imported into the namespace Dune::std\
    All core developers agree on this being okay, due to all used
    compilers\
    implementing C++11 sufficiently.
-   DUNE\_DELETE is probably not needed.
-   Carsten will fix the subentity orientation test FS\#980.
-   The naming convention decided upon should be moved to a more
    prominent place on the homepage.
-   Removal of the lbegin() and lend() methods: Martin Nolte will remove
    it from the interface and from the gridcheck. The grid maintainer
    decide individually on removing them or keeping them as an
    implementation detail.
-   Jö will keep working documentation on the thread safety of grid
    views and will maybe introduce a capability.

Development practices
---------------------

### Git Best Practices

A collection of git best practices for Dune should be integrated into
the Dune wiki. Some content might be

-   Markus presentation from the user meeting on writing good commit
    messages
-   Christophs email on rebasing against master to avoid merge commits
-   A guideline on how much squashing is appreciated.

### Development Team

Christian Engwer proposes to give core developers the possibility to
default-abstain on daily discussions. A quorum should be in use to
prevent decisions that too few developers agree upon.

Oliver Sander asks what the developer team is supposed to be: Is it an
Alumni Club or a steering committee? In the latter case, it would be
consequent to have a limited number of developers, which are elected for
a limited period.

There is a consensus, that the polling tool on the wiki has a positive
impact on the development. Especially for topics that are
non-controversial, it has simplified decision-making a lot. For
controversial topics, the debating culture is the crucial problem.

Flyspray-Tasks, where after heavy discussions no consensus can be
reached, should proceed to a poll more quickly. In the past, nobody
stepped in and requested a poll.

It would be helpful to start and end polls on fixed dates, like: All
polls start on the beginning of the month and end on the 15th. That way,
less involved core developers can get up-to-date rather quickly.

### Decisions on the development team

The homepage should list in the peoples section all developers, that
have in past or present contributed significantly to the dune core
modules. All current core developers are part of this list. The members
of the list can vote to add members to the list by simple majority. The
members of this list are called core developers.

A separate list of "Former Developers" is to be established. Core
developers can retire and will be listed there. Non-Core Developers,
that have in the past contributed significantly, can be listed too, if
they agree.

Future decisions are to be taken by voting with a simple majority (at
least 50% of non-abstained votes) by all core developers. The votes of
non-participating core developers are to be taken as abstentions. An
option "needs further discussion" is to be given in every poll.

The above three paragraphs are accepted with: 9 yes, 0 no, 3 not present

Christoph Grüninger is added to the list of core developers: 9 yes, 0
no, 3 not present. Welcome Christoph!

### Development Model

Visibility of features developed in other work groups should be
improved. Christian Engwer proposes to put a roadmap on the homepage,
which links to the feature branches.

### Licensing

Christian Engwer has collected a list of email addresses of all
contributors. Before contacting them, he wants to reconsider the
license, that we aim for. The old license is to be kept for sure, we
talk about dual licensing.

OPINION poll:\
LGPL3+: all present developer could agree on this license.\
BSD: 4 present developers would like the license, others are unsure, are
against it or would need to think a lot more about it (and discuss with
others).

### CMake vs. Autotools

There is a general consensus, that supporting two build systems is
unwanted in the long run.

The following things do not work in cmake yet, but will be worked on as
soon as a decision is made:

-   make package
-   there is no howto

Deciding to completely transition to CMake for 3.0 should involve a
transition period. One such idea is to introduce --use-autotools in
release 2.4 already and have cmake as default buildsystem. This could
bring more bug reports and testers, which is important to the
finalization of the cmake build system.

CMake should be the only buildsystem in Dune 3.0. Vote: 7 yes, 1 no, 2
abstentions, 3 not present.

CMake should be the default build system in Dune 2.4. Vote: 9 yes, 1
abstentions, 3 not present.

Markus Blatt wants to simplify the build system to have it as small as
possible. This should include to add all flags to all targets instead of
having the user select (and forget) the flags. Vote: 9 yes, 1
abstention, 3 not present.

Christoph Grüninger wants to switch to Cmake 2.8.9 as minimum required
version of cmake after the Dune 2.4 release. We should evaluate with our
users, whether this is possible.

Supported Compilers
-------------------

### ICC

ICC 13 doesn't work with Dune. ICC 14 has a lot bugs, that are very hard
to work around. Users need to patch their C++ std library. This is
definitely hard to support through the mailing lists. ICC 15 has solved
those problems. The homepage should be changed to state: 13 (doesn't
work), 14 (needs patches, no support on the mailing lists), 15 (seems to
work)

### Clang

Until now, clang was not officially supported. Due to the growing user
base, we want to officially support clang &gt;=3.4 with the next
release.

### gcc

For the next release, no change in the supported gcc is to be made. (gcc
4.4)

Release
-------

-   Our current changes qualify for a 2.4 release (and not 3.0)
-   Carsten Gräser volunteers as release manager.
-   The release should happen this year.

Grid implementations
--------------------

### SGrid

SGrid should be deprecated in the 2.4, because YaspGrid implements all
of its feature and more. Oliver Sander will do the work. Vote: 10 yes, 0
no, 3 not present.

### Alberta 2

Alberta2 is not supported anymore by the developers, though Markus is
still running tests. As Alberta3 is now in Debian, Alberta2 support
should be removed. It was discussed to deprecate or remove it. Vote: 2
remove, 1 deprecate, 6 abstentions, 3 not present. The alberta 2 code
will be removed before 2.4. Christoph Grüninger will clean the
buildsystem.

### ALUGrid

Martin Nolte will do a dune-alugrid release, Ansgar will package it for
Debian. Ansgar needs to have the release in October.

### YaspGrid

<x>CoordinateContainer should be renamed into <x>Coordinates. There are
no objections to merging the new features in
feature/all-entities-yaspgrid before 2.4.

New feature requests
--------------------

### Global indices

There is an implementation from Benedikt Oswald. Oliver Sander wants to
have it in utilities in dune-grid. This should be part of 2.4. Vote: 9
yes, 0 no, 4 not present.

Markus Blatt wants the implementation to work with scotch instead of
ParMetis, too.

### QuadratureRuleKey in dune-geometry

In dune-fufem, there exists a mechanism to choose quadrature formulas
via a key (geometrytype, order, refinement level ...). It can be included
as a utility. The only interface change is adding a method to the
quadrature rule container, that takes a such key.

The functionality is generally considered very useful. Carsten Gräser
will prepare a feature branch for discussion on the details.

Interface Changes
-----------------

### Can EntityIterators be true STL forward\_iterators?

For clarity reasons, it would be nice if they were. All grid
implementors think that they can implement all capabilities of a
forward\_iterator with the exception that dereferencing may return a
proxy object (in the same fashion std::vector<bool> does). This can be
ignored implementing all other iterator categories, too. As part of the
forward\_iterator interface, the postfix increment operator has to be
implemented. Grid implementations can also implement more comprehensive
iterator categories. Vote: 9 yes, 1 no, 3 not present. We aim for 2.4
with this unless a grid maintainer says that it is not feasible.

### EntityPointer's future

If EntityPointer was to be removed, the Entity has to:

-   have copy and move-constructor
-   have assignment and move assignment
-   lifetime of an entity is to be guaranteed until the grid is modified
-   the entity may never return references to itself (not
    even indirectly)

In order to have operator-&gt; work, a helper object should be provided
by the facade infrastructure.

The move constructors must be implemented by hand, because gcc 4.4 and
4.5s generated move constructors are buggy.

Removing the EntityPointer is a 3.0 feature. Copiable entities should be
put into 2.4. This way, the user can be warned about the future change
and adapt his code. The decision whether or not moveability is
implemented in 2.4, is with the grid implementor. Vote: 9 yes, 1
abstention, 3 not present.

### Copyable intersections

The previous point can be adapted to intersections. In 2.4 these should
copyable, moveability is up to the grid implementor. In 3.0,
intersection iterators should return temporaries instead of references.
Vote: 7 yes, 2 abstentions, 4 not present.

### Gridcheck headers

The headers of the gridcheck should be .hh instead of .cc. Vote: 10 yes,
0 no, 3 not present. Responsible: Markus Blatt.

### monom.hh -&gt; monomial.hh

monom.hh is to be renamed to monomial.hh. Vote: 10 yes, 0 no, 3 not
present. Responsible: Oliver Sander

### Remove boost dependency

ISTL has code that relies on boost. dune-common has no such code. It
should be checked, whether the boost check runs in dune-common, which is
not necessary. The test should only be run if the module needs boost.
Responsible: Christoph Grüninger.

### Getting rid of Dune::istl\_assign\_to\_fmatrix

The functionality can be fulfilled by a cast operator. It should be
removed directly. Responsible: Christoph Gersbacher.

### Rename and remove geometry specific constants and functions

Geometry::dimension and Geometry::dimensionworld are information
unrelated to the geometry. Remove it. Vote: 10 yes, 0 no, 3 abstentions.
Responsible: Oliver Sander

mydimension und coordDimensions should be changed, some more names on
the geometry too. This is postponed to next years meeting due to the
need of more discussion.

### Change the template argument list of the geometry

This would allow grid implementors to directly use geometry classes,
instead of writing a duplication of all constructors just to match the
template argument list.

The geometry facade should be declared a friend of all grid
implementations. This way the impl() method will keep working. This is
for 3.0. Vote: 10 yes, 0 no, 3 not present. Responsible: Martin Nolte.

### Getting rid of the PartitionType template parameter on the GridView

The template parameter is to be removed. The default iterators should
iterate over All\_Partition.

The methods leafGridView() etc. on the grid that are templated to the
PartitionIteratorType should be removed accordingly.

This is to be deprecated in 2.4 and removed in 3.0. Vote: 8 yes, 0 no, 2
abstentions, 3 not present. responsible: Martin Nolte.

### Duplicated methods on Grid and GridView

overlapSize(), ghostSize(), communicate() are duplicated on GridView and
Grid. Deprecate them in 2.4 and remove them in 3.0 from Grid. Vote: 9
yes, 0 no, 4 not present.

### geomTypes() on the IndexSet

The method should be deprecated in 2.4 and removed in 3.0.

There should be a new method types(), that returns an object that is
iterable with a forward iterator.

It is conceptionally unclear, whether this method belongs with the index
set or the grid view. It should be added to both of them. The return
type of the method types() should be exported as Types. Vote: 6 yes, 0
no, 3 abstentions, 4 not present. Responsible: Martin Nolte.

### Range based for

Conceptionally, range based for can be implemented as methods of the
grid view or as free functions. There is a consensus among the present
core developers, that free functions allow a nicer syntax.

The naming in the pdesoft proposal was discussed. The shortcuts "Dim"
and "Codim" (esp. the former) were discussed controversily. The
alternative is "Dimension" and "Codimension". Vote: 4 short, 2 long, 3
abstentions, 4 not present.

An object Dune::Partition (a tuple of Dune::PartitionType s) is
introduced. It has an overloaded operator+/- that describes the union of
partitions. The free functions for range based for take the
Dune::Partition object as a third parameter, which defaults to the
All\_Partition.

The entities method with Codim and Partition parameters are the only
real implementations. All other (convenience) methods should forward to
that method.

There is also a method intersection(gv,element) that allows to iterate
over all intersections with range based for.

This is a 2.4 feature! It is only for convenience of the users, that use
sufficiently new compilers. It is not yet to be used within the core
modules. Vote: 8 yes, 0 no, 1 abstention, 4 not present. Responsible:
Steffen Müthing.

### parallel features of the grid interface

Martin Nolte will organize a meeting on parallelism in dune-grid.
Interested developers are Markus Blatt, Christian Engwer, Jö Fahlke,
Peter Bastian, Bernd Flemisch, Steffen Müthing, Andreas Dedner, and
Robert K.

### Differences between the Mapper and IndexSets

The interface of the mapper should be similar to that of an IndexSet:
the map() method should be called index() resp. subIndex() instead. The
return type should be the correct IndexType instead of int.

Deprecate map() in 2.4, remove it in 3.0. Vote: 6 yes, 0 no, 4
abstentions, 3 not present. Responsible: Oliver Sander.

### Revisiting capabilities

isCartesian : Oliver Sander wants confirmation, that the semantics are
intended as written down in the docs. The documentation should clarify
what is meant by the ith intersection. This should be the numbering from
the reference element. The capability should also ask the cartesian grid
to be conforming leaf and levelwise. A better interface for structured
cartesian grids would be very nice, but is out of scope for the moment.

hasEntity : The capability hasEntity is to be removed after 2.4. There
will be a new capability

    template
    struct hasEntityIterator
    {
      static const bool v = false;
    };

All grids should specialize this capability to true, if the grid view
implements iteration over elements of the codimension codim. This should
be introduced in 2.4.

threadSafe and viewThreadSafe should be discussed on the parallel
meeting.

isParallel : This capabilities' semantics are unclear. It will be
deprecated in 2.4 and removed in 3.0, unless somebody comes up with a
convincing use case.

Vote (on all capability changes): 9 yes, 0 no, 4 not present.

### intersectionIds

There is a general consensus, that intersectionIds are a nice feature.
The exact semantics of this feature are however non-trivial. The topic
should be discussed at a future meeting. Having a written proposal on
the semantics would be of great help here.

### hasBoundaryIntersections()

The method doesn't seem to be used. It is, however, for some grids
non-trivial to implement or costly to evaluate. A decision is postponed
to ask Robert for his current opinion on it.

Topics not discussed
--------------------

The following topics have been proposed, but have not been discussed due
to lack of time. This can be taken as a starting point, when the next
meeting is prepared.

-   Supported gcc version after next release (e.g., 4.6 - 4.9)
-   Lifetime of objects passed to methods/constructors.
-   the variable/typedef/constant seems to be inconsistent in some of
    the every day implementations such as FieldMatrix, see FS\#925
-   What precisely is the semantics of the Entity&lt;0&gt;::isRegular
    method? What is it needed for?
-   Does the hasBoundaryIntersections method consider the domain
    boundary or the processor boundary?
-   What is the precise meaning of the overlapSize() and ghostSize()
    methods?
-   Is the communicate method synchronous?
-   What is the exact semantics of CommDataHandleIF::fixedSize? Does it
    mean that I may call size on the receiving side instead of
    communicating it?
-   What is the exact lifetime of grid views/index sets? Are they still
    valid after grid modification?
-   The method LocalInterpolation::interpolate has a template parameter
    'C', which is the type of coefficients returned by
    the interpolation. What kind of interface is expected from this
    type?
-   If this 'interpolate' method uses derivative degrees of freedom of
    the input function, how is the input function expected to provide
    them?
-   Parallel Interface: FS\#1105 (Point-To-Point Communication)
    FS\#943 (MPI\_Scan)
-   The Capabilities are currently a separate namespace filled with lots
    of small classes. It may be more elegant to have everything in a
    single class, like std::numeric\_limits.
-   Remove Entity&lt; codim &gt;::level(), codim &gt; 0. It is hardly
    ever used and not trivial to implement.
-   Migrate mappers from dune/grid/common to dune/grid/utility or
    dune/grid/utility/mapper - they are utility classes after all.
