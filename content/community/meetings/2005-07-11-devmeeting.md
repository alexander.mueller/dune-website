+++
title = "DUNE Developer Meeting 11.7.2005"
+++

0.5) Berichte
------------

1) Indizes
----------

Result of the discussion on indices and related stuff with Robert
in Avignon in the week June 6-10, 2005.

We propose to have four different ways to number entities:

1. Levelwise index: Consecutive, zero-starting, non-persistent numbering of
   entities of one codimension, geometry type and level in one process.
   I.e. entities of different levels, codimension, geometry type and processes can have the same number.
   Non-persistent means that the number can change during grid refinement and
   load balancing.

2. Leafwise index: Consecutive, zero-starting, non-persistent numbering of
   entities of leaf entities of one codimension and geometry type. All children which are copies
   of an entity have the same number.

3. Globally unique id: Every entity has a globally unique identification, i.e. across
   all levels, codimensions and processes. The type used for this id is
   user-defined. But: The copies in the refinement tree are identified which means
   e.g. an entity in the coarse grid that is copied to the fine grid has the *same*
   id.

4. Locally unique id: Same as 3. but only *within* processes.

> Note 1: In principle all information could be created externally from the globally
> unique index defined in 3. But as usual we will provide default implementations
> that can be overloaded.

> Note 2: The indices and id are accessible via extra objects that are returned
> by the grid. This offers the possibility that indices or ids are only computed
> on demand. These classes are:
>  * LevelIndexSet
>  * LeafIndexSet
>  * GlobalIdSet
>  * LocalIdSet

> Note 3: The classes LevelIndexSet and LeafIndexSet implement the *same* methods.
> This enables us to write other classes independent of whether they use a level
> or leaf index. This also requires the size methods to be in theses classes (see below).

> Note 4: GlobalIdSet and LocalIdSet implement the same interface.

There are the following four methods on the grid that deliver the index/id objects:

  * `const GlobalIdSet& globalidset();`
  * `const LevelIndexSet& levelindexset(int level);`
  * `const LeafIndexSet& leafindexset();`
  * `const LocalIdSet& localidset();`

```
//! Class to access the level index
template<class GridImp>
class LevelIndexSet
{
public:
  //! get index of an entity
  template<int cd>
  int index (const typename GridImp::Traits::template codim<cd>::Entity& e) const
  {
  }

  //! get index of subentity of a codim 0 entity
  template<int cc>
  int subindex (const typename GridImp::Traits::template codim<0>::Entity& e, int i) const
  {
  }

  //! get number of entities of given codim, type and level (the level is known to the object)
  int size (int codim, GeometryType type) const
  {
  }

  //! deliver all geometry types used in this grid
  const std::vector<GeometryType>& geomtypes () const
  {
  }
};

//! Class to access the leaf index
template<class GridImp>
class LeafIndexSet
{
public:
  //! get index of an entity
  template<int cd>
  int index (const typename GridImp::Traits::template codim<cd>::Entity& e) const
  {
  }

  //! get index of subentity of a codim 0 entity
  template<int cc>
  int subindex (const typename GridImp::Traits::template codim<0>::Entity& e, int i) const
  {
  }

  //! get number of entities of given codim, type
  int size (int codim, GeometryType type) const
  {
  }

  //! deliver all geometry types used in this grid
  const std::vector<GeometryType>& geomtypes () const
  {
  }
};


//! Class to access the globally unique id
template<class GridImp>
class GlobalIdSet
{
public:
  //! define the type used for persisitent indices
  typedef ... GlobalIdType;

  //! get id of an entity
  template<int cd>
  GlobalIdType id (const typename GridImp::Traits::template codim<cd>::Entity& e) const
  {
  }

  //! get id of subentity
  template<int cc>
  GlobalIdType subid (const typename GridImp::Traits::template codim<0>::Entity& e, int i) const
  {
  }
};

//! Class to access the locally unique id
template<class GridImp>
class LocalIdSet
{
public:
  //! define the type used for persisitent indices
  typedef ... LocalIdType;

  //! get id of an entity
  template<int cd>
  LocalIdType id (const typename GridImp::Traits::template codim<cd>::Entity& e) const
  {
  }

  //! get id of subentity
  template<int cc>
  LocalIdType subid (const typename GridImp::Traits::template codim<0>::Entity& e, int i) const
  {
  }
};


```

int -> size_t
Rückgabewert in local id -> LocalIdType
Methoden index(), subindex(), globalindex() verschwinden von Entity
globalidset herausheben da es das slim interface darstellt.
globalid soll auf allen Kopien (aller Stufen) verfügbar sein

OK, ist abgehakt und wird jetzt implementiert.

1.5) Mapper
----------

Überarbeiten und dann nach grid/common

2) Referenzelemente
-------------------

Referenzelemente in grid/common einchecken (möglichst schnell)
Prisma und Pyramide fehlen noch -> dazumachen
GeometryType in extra header file
triangle, quadrilateral, hexahedron, tetrahedron verschwinden -> simplex, cube


3) Weitere Ergänzungen der Gitterschnittstelle
----------------------------------------------

 1. *Leafiteratoren für alle codims/partition types*
   * werden aufgenommen
   * maxlevel Argument aus LeafIterator raus
 1. *auch für HierarchicIterator ?*
   * beginnt auf erstem Kind
   * HierarchicIterators für alle codims werden aufgenommen (keine partition types!)
 1. *IntersectionIterator*
   * Dereferenzierung in methode Packen : Methoden inside() (d.h. selbst) und outside() (der Nachbar)
   * nicht mehr von EntityPtr ableiten
   * wird als Schönheitskorrektur angesehen
 1. *communicate*
   * -> soll später diskutiert werden
   * leafcommunicate ?!
 1. *Tangentialvektoren*
   * -> später
 1. *Prozedere zur Änderung der Gitterschnittstelle*
   * Heute sollte die Funktionalität des stable branch 1.0 beschlossen
   * Wie kommt später was von development nach stable?
   * Zuerst ,,Feature freeze'', dann bug fixes bis der 1.0 Test läuft. Danach stable release und jetzt erst ein neuer development branch.
 1. *isLeaf() Methode für alle Entities?*
   * Ja! kommt in 1.0.

4) Version 1.0 = stable branch / Workshop
-----------------------------------------

 1. LeafIterator Umstellung (verpflichtend)
 1. IntersectionIterator Umstellung (verpflichtend)
 1. Indizes (verpflichtend)
 1. HierarchicIteratoren für codim!=0 sollen ins Interface. Implementierung ist optional und wird über capabilities geprüft. (optional)
 1. level-/leaf-communicate ist noch nicht fest, muß aber in die stable release. (verpflichtend für die parallelen Gitter)
 1. isLeaf() für alle Codimensionen. (verpflichtend)
 1. Paper-Workshop im Herbst.
 1. Grid::codim -> Grid::Codim umbenennen.

5) Buildsystem
--------------

  **a. verschiedene Compiler**

  gewünscht ist
  `g++-3.4 g++-4.0 icc-7.1 icc-8.1 icc-9.0`

  testen mit
  `xlC pgc SGI-Compilier XCode-2.1`

   * xlC: Account in Freiburg
   * pgc: auf Helics
   * SGI-Compiler: steht in Freiburg
   * XCode: Thimo

  woher kommen die icc Lizenzen?

   * 7.1 existiert -> Heidelberg
   * 9.0 109€ -> Heidelberg

  **b. Bug Tracking**
  kurz vorgstell
  ... jeder möge sich anmelden.

  **c. ALU3D**
  erscheint in Kürze auf einer homepage als tar.gz

  **d. Versionskontrolle**
  müssen wir später nochmal sehen.

6) Quadraturen
--------------
 * Neue Quadraturen -> quadrature Verzeichnis
 * Regeln für Prisma und Pyramide


7) Shape Functions
------------------
verschoben...
