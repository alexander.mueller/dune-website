+++
title = "Dune User Meeting 2023"
+++

# Dune User Meeting 2023 in Dresden

## Invitation

We invite all Dune users to participate in the 7th Dune User meeting, to
be held at the Technische Universität Dresden from **2023/09/18** to **2023/09/19**.

This meeting offers a chance to showcase how you're using Dune,
foster collaborations between the users and provide input to the future development of
Dune. The meeting also welcomes contributions from other frameworks such as
[DuMuX](https://dumux.org), [AMDiS](https://amdis.readthedocs.io) and [OPM](https://opm-project.org/)
that are using Dune.

We keep the format rather informal. Participants are encouraged to give a presentation. In addition,
we will reserve plenty of time for general discussions on Dune.

Directly after the user meeting a [meeting of the Dune developers](/community/meetings/2023-09-devmeeting) will be held
in the same location. We encourage all interested participants of the user meeting to participate in
the developer meeting as well.


## Registration
The online registration is already over. If you still want to join, please send a mail to the [organizing committee](mailto:meeting-2023@dune-project.org).
There will be no registration fee. Coffee and snacks are provided. Lunch is available in the nearby
local [mensa](https://www.studentenwerk-dresden.de/mensen/) of the university.


## Invited Speakers

- [Christophe Geuzaine](https://people.montefiore.uliege.be/geuzaine) (University of Liège, Dept. of Electrical Engineering and Computer Science, Belgium; Developer of Gmsh)
- [Luca Heltai](https://sites.google.com/view/luca-heltai/home) (SISSA - International School for Advanced Studies, mathLab Laboratory, Trieste; Developer of deal.ii)


## Location

### Conference

[Z21 Rotunde und Bürogebäude](http://navigator.tu-dresden.de/karten/dresden/geb/z21),
Zellescher Weg 25,
01217 Dresden

*[Room Z21/250](http://navigator.tu-dresden.de/etplan/z21/02/raum/342102.0850)*

### Videoconference

The user meeting will be streamed via the platform BigBlueButton. 
Please identify yourself by your name.

[Start BBB videoconference](https://tud.link/gza0)

### Conference dinner

There will be a conference dinner on Monday 18.9.2023 at 19:00
at the [Brauhaus Watzke](https://watzke.de/).  To get [there](https://osm.org/go/0MLkEOXlb?node=7852199969),
take a [tram](https://dvb.de) to the stop "Altpieschen".


## Program

### Monday, September 18, 2023

{{< table >}}

  Time | Speaker | Title
-------|---------|------
09:00-09:25  | Robert Klöfkorn | An overview of the Python interface to DUNE-FEM
09:25-09:50  | Alexander Müller | dune-iga: Isogeometric analysis within the DUNE framework
09:50-10:15  | Lukas Renelt | Representation of parametric PDEs in DUNE-PDELab with application in Model Order Reduction
10:15-10:40  | Eloy de Kinkelder | Monolithic coupling of bulk and surface
10:40-11:10  | *Coffee break* |
11:10-12:10  | Christoph Geuzaine | Gmsh: general overview and recent developments *(online)*
12:10-13:35  | *Lunch break* |
13:35-14:00  | Linus Seelinger | UM-Bridge: Enabling Uncertainty Quantification on Advanced Numerical Models *(online)*
14:00-14:25  | Antonella Ritorto | Local grid refinement in OPM
14:25-14:50  | Timo Koch | Finite volume schemes and coupled problems in DuMux
14:50-15:15  | Leopold Stadler | dumux-shallowwater - a DuMux application for simulating shallow water flows *(online)*
15:15-15:45  | *Coffee break* |
15:45-16:10  | Martin Utz | Simulating dunes with DUNE; dumux-sediment a bedload transport model for engineering applications
16:10-16:35  | Tilman Steinweg | Aquarius - a web application for data management and automated numerical simulations on HPCs
16:35-17:00  | Santiago Ospina | Fine-grained locks for multithreaded grid operators
{{< /table >}}

#### Conference dinner

19:00 Conference dinner at the [Brauhaus Watzke](https://watzke.de/).

### Tuesday, September 19, 2023

{{< table >}}
  Time | Speaker | Title
-------|---------|------
09:00-10:00  | Luca Heltai | How do we make large opensource projects sustainable? (lessons learned from 25 years of deal.II) *(online)*
10:00-10:30  | *Coffee break* |
10:30-10:55  | Andreas Dedner | A General Approach to Implementing Virtual Element Schemes in Dune *(online)*
10:55-11:20  | Carsten Gräser | What's new in dune-functions?
11:20-11:45  | Simon Praetorius | A general tensor implementation for Dune
11:45-12:10  | Dennis Gläser  | GridFormat: a header-only C++-Library for grid file I/O
{{< /table >}}


## Participants

{{< table >}}
Participant         | Affiliation
------------------- | ------------------------------------------------------------
Blatt, Markus       | Dr.Blatt - HPC-Simulation-Software & Services
Böhnlein, Klaus     | TU Dresden
Dedner, Andreas     | University of Warwick
Engwer, Christian   | University of Münster
Gläser, Dennis      | University of Stuttgart
Gräser, Carsten     | FAU Erlangen-Nürnberg
Happel, Lea         | TU Dresden
Jaap, Patrick       | WIAS Berlin
de Kinkelder, Eloy  | TU Bergakademie Freiberg
Klöfkorn, Robert    | Lund University
Koch, Timo          | University of Oslo
Mokbel, Marcel      | TU Bergakademie Freiberg
Müller, Alexander   | University of Stuttgart
Nebel, Lisa Julia   | TU Dresden
Ospina, Santiago    | IWR, Heidelberg University
Porrmann, Maik      | TU Dresden
Praetorius, Simon   | TU Dresden
Renelt, Lukas       | University of Münster
Residori, Mirko     | TU Dresden
Ritorto, Antonella  | Dr.Blatt - HPC-Simulation-Software & Services
Sander, Oliver      | TU Dresden
Seelinger, Linus    | Heidelberg University
Seifu Bekele, Samson | NTNU, Hawassa University
Stadler, Leopold    | Bundesanstalt für Wasserbau (BAW)
Steinweg, Tilman    | Federal Waterways Engineering and Research Institute (BAW)
Utz, Martin         | Federal Waterways Engineering and Research Institute (BAW)
Vinod Kumar Mitruka, Tarun Kumar Mitruka | Institute for Structural Mechanics, University of Stuttgart
{{< /table >}}


## Organizing Committee

Contact: [the organizing committee](mailto:meeting-2023@dune-project.org)

* Simon Praetorius
* Oliver Sander
* Markus Blatt


## Accommodation

Dresden offers a wide range of accommodation options for all budgets. Please contact
[the organizers](mailto:meeting-2023@dune-project.org) if you need help finding an
accommodation.
