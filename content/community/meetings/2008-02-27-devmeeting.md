+++
title = "DUNE Developer Meeting February 2008"
+++

dune-minutes
============

These are the decisions made at the DUNE developer meeting held at the
Freie Universität Berlin on 27. and 28. of February 2008.

Changes to the DUNE interface
-----------------------------

-   **Coarse grid creation interface**
     There will be a well defined interface for the creation of
    coarse grids. It will be similar to the interface already
    implemented for UGGrid. However, the functionality will be moved
    into a separate factory class. File readers will then use
    this factory. *Oliver Sander* will implement a first prototype
    for UGGrid. Freiburg will split up the DGF code. The `GridPtr` class
    will be removed, all GridFactories will return a `Grid*`. The
    DGFParser will be updated to use the Factory interface.
     * Assigned to *Oliver Sander* and the *DGFParser developers*.

-   **Index type returned by index sets**
     The type for the indices returned by index sets will now be
    available as a typedef in the class. Existing implementations will
    be switched to unsigned int.
     * Assigned to *Oliver Sander*.

-   **Polynomial order of geometries**
     The necessity for geometries to hand out information about its
    smoothness properties is generally recognized. However no good
    solution has been found. The topic needs further research.
     * *Postponed*

-   **Boundary ID**
     The boundaryID method will be made available in UGGrid.
     * Assigned to *Oliver Sander*.

-   **Keeping EntityPointers small by specifying reference lifetime**
     The EntityPointer class gets a method compactify(). Calling this
    methods suggests to the class to release all nonessential memory
    allocated by the class. This allows to store large amounts of
    EntityPointers.
    * Assigned to *Carsten Gräser*.

-   **Entity vs. EntityPointer**
     There will be two new methods mark() and getMark() in the grid
    class which take an Entity as the argument. The corresponding
    methods taking EntityPointers will be deprecated. Also, the class
    Entity will get a method pointer() which returns an EntityPointer to
    the current Entity.
    * Assigned to *Robert Klöfkorn*.

-   **Generic Geometry classes**
    Generic geometry classes will be made available to simplify grid
    implementations.
    * Assigned to *Martin Nolte*.

-   **XYZDefault classes**
    The default classes will be removed successively from the
    interface.
    * Assigned to *every grid developer*.

-   **Make IntersectionIterator a real iterator**
     The two intersection iterators will be transformed to real
    iterators which offers only the iterator functionality and which can
    be referenced to yield the actual intersection, new implemented as a
    separate class.
    * Assigned to *Christian Engwer*.

-   **SubEntities**
     All grids are now required to implement the subEntity method for
    all codimensions. In the short range these entities do not have to
    provide geometries.
    * Assigned to the *YaspGrid and UGGrid developers*.

-   **Parametrize class with the Traits class**
     Alltemplate parameters should be changed to the GridTraits,
    Redundant information can be removed.
    * *Postponed*.

-   **Generic reference elements**
     Peter documents his implementation for generic cube
    reference elements. Freiburg tries to incorporate this into their
    generic reference element implementation.
    * Assigned to *Peter Bastian* and *Martin Nolte*.

-   **Save/restore for grids**
     There will be save/restore facilities in the interface. Andreas
    will work out a detailed specification.
    * Assigned to *Andreas Dedner*.

-   **Method conforming() on intersection**
     The class Intersection will get an additional method conforming(),
    which returns true iff the intersection is a complete face for both
    elements involved.
    * Assigned to *Andreas Dedner*.

-   **Thread safety**
     There will be two new capabilities threadSafe and viewThreadSafe,
    which assure overall thread safety and read-only thread safety,
    respectively.
    * Assigned to *every grid developer*.

-   **Remove GridPart functionality from the index sets**
     The methods begin() and end() and the corresponding types in the
    IndexSets will be marked deprecated and removed eventually.
    * Assigned to *Oliver Sander*.

-   **GridView**
     The grid parts will be renamed to GridViews. All relevant
    size/begin/end/intersection/indexset/communicate method will be
    moved to them. They lose the method level().
    * Assigned to *Sven Marnach* and *Oliver Sander*.
-   **HierarchicIndexSet/HierarchicGridPart**
     These will be removed from the dune-grid module.
    * Assigned to *Robert Klöfkorn*.

-   **Capability 'hasHangingNodes' and 'IsUnstructured' vs,
    'isCartesian'**
     'hasHangingNodes' and 'IsUnstructured' will be marked deprecated.
    'isCartesian' will replace 'IsUnstructured' and will have the
    following semantics:
    -   geometries are not affine; they are given by as

        ```
        $T \circ S$
        ```

        where T is a translation and S a scaling operator
    -   the axis of a normal vector is given as

        ```
        ItersectionIterator it;
        ...
        int axis = it.numberInSelf()/2;
        ```

    * Assigned to *Sven Marnach*.

-   **Separate sparsity structure and entries in BCRSMatrix**
     Martin Weiser will provide a prototype implementation for this.
    * Assigned to *Martin Weiser*.

New DUNE modules
----------------

Three new core modules will be made available on the server.

-   **dune-shapefunctions**
     Nomen est omen

-   **dune-functions**
     Containing mappers and discrete functions.

-   **dune-grid-dev-howto**
     In a first step this will contain an example grid implementation,
    which does nothing but wrapping a DUNE grid. This will help people
    to get started with their own grid implementations. Later we plan to
    have a tutorial on how to implement grids and a set of successively
    tighter unit tests.

Miscellaneous
-------------

-   **Heterogeneous Matrix Class**
    Martin Weiser donates his heterogeneous matrix class to dune-istl.
    He will get write access and clean up the code and commit it in the
    near future.
    Assigned to *Martin Weiser*.

-   **Project Overview**
    Mario sets up a list of groups, persons, and projects that are
    known to use DUNE. Each will get a small description, possibly
    formalized in some way. The page 'Publications using DUNE' will be
    removed.
    Assigned to *Mario Olberger*.

------------------------------------------------------------------------

[Oliver Sander](mailto:sander@haile.mi.fu-berlin.de)

Last modified: Sun Mar 2 14:07:26 CET 2008
