+++
title = "Dune Developer Meeting 2021 Online"
+++

Date and Location
----

The developer meeting is held online via *Jitsi* on January 8th and 22nd, 2021.
Tentative: January 19th afternoon. 

Dates, times and the link to the online meeting are found below.

We invite all DUNE developers and contributors to participate.

## Tentative Schedule

<table>
<tr><td>Jan. 8th  </td><td>&nbsp;13:30 - 15:30, 15:50 - 17:30</td><td>&nbsp;<a href="#from-january-8">Meeting Minutes</a></td></tr>
<tr><td>Jan. 19th </td><td>&nbsp;13:30 - 15:30, 15:50 - 17:30</td><td>&nbsp;<a href="#from-january-19">Meeting Minutes</a></td></tr>
<tr><td>Jan. 22nd </td><td>&nbsp;13:30 - 15:30, 15:50 - 17:30</td><td></td></tr>
</table>

Access to the online meeting 
----------------------------

The online video meeting is planned using Jitsi: 
- [Video-Link](https://test.eichstaett.social/DuneDevMeeting2021) (will be active from about 13:00 until open end)
- Password is sent separately to the developer mailinglist. If you want to participate and didn't get the information, please send a mail to [Simon](mailto:simon.praetorius@tu-dresden.de)

Participants
-------------

- Andreas (only 22nd)
- Robert 
- Markus
- Dominic Kempf (only 22nd)
- Carsten Gräser
- Christian
- Christoph (only 8th and 22nd)
- Samuel Burbulla
- Santiago Ospina
- Oliver Sander
- René Heß
- Simon Praetorius
- Linus Seelinger

Proposed topics
---------------

Please use the edit function to add your topics or write an email to [Simon](mailto:simon.praetorius@tu-dresden.de).


### General

- Robert: Important decisions, such as required versions of hard dependencies, CMake, compilers and similar or developer meetings dates and other overarching decisions should require a confirmed vote from a vast majority of the developers and not just the wait if somebody objects until tomorrow evening type of vote.
  * Christoph: The dune devel mailing list is our main communication platform. If you don't read these emails, you are out of luck. Having more votes boils also down to forcing decisions. Nor sure whether that is good or bad, though.
- Robert: DUNE 2.8 release January 2021
  * Christoph: Sure: Who is volunteering to do it? What's the schedule?
  * Andreas: Robert and myself would be willing to do this
- Robert: Versioning: What about a yearly versioning, e.g. DUNE-2020.01 release etc, since we only have one  release per year anyway.
  * Christoph: What's the advantage? The current scheme is good. How do bugfix-releases fit into the proposed scheme?
  * Andreas: One advantage would be that we don't have to talk about the famous 3.0 anymore. We're running out of time there...
             A bug fix release could be simply DUNE-2020.02? The assumption is that there will not be two 'non' bugfix release
             in one year?
- Markus:
  * Our license is not compatible to some others as we insist on GPL version 2. There was a previous
  effort to change that but it never made it. Is there a chance to revive it and use 2+?
  * CGrü: You want <i>GPL v2+ with runtime exception</i>? I think Christian proposed LGPL v3 additional to GPL v2 with runtime exception, as LGPLv3 matches our need and would be an unmodified license.<br />
  I welcome such effort, but first we have to make sure that all core developers agree.
  And then we have to ask as many contributors as we can reach.
- Andreas: default for python bindings https://gitlab.dune-project.org/core/dune-common/-/issues/225
  would be good to decide this before 2.8, i.e., soon

### Community

### Infrastructure
- Oliver:
  * Can we agree on some automated testing of our coding style (as in [common!890](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/890))?  And maybe retire the whitespace-hook in return?
- René:
  * Policy regarding our registry: Communicate that all data in the registry could be lost, so make sure that you could always recreate the images pushed to the registry.
  * Whitespace hook: We used to have a hook that rejected commits that contain trailing whitespaces. This is currently not working [infrastructure!61](https://gitlab.dune-project.org/infrastructure/issues0/-/issues/61). Discussion: Do we want/need that and who is responsible for it. Note: This is different to the local whitespace hook `dune-common/bin/git-whitespace-hook`.
  * Dune and [dockerhub](https://hub.docker.com/): Does anybody know anything about the user `duneci` on docker hub?
  
### Core

#### dune-common
- Christoph:
  * merging geometry and localfunctions or another solution ([common#224](https://gitlab.dune-project.org/core/dune-common/-/issues/224))
    - *Andreas:* From the discussion I got the feeling this was a solution looking for a problem?
  * future of duneproject (DK: [common!892](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/892))
- Simon: 
  * cmake: (short) explanation why and how cleanup
  * cmake: required c++ standard (SP: [common!862](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/862))
    - *Andreas:* I would like this to be discussed on the 22/01 - all developers seem to be able to attend then
  * MPI in cmake: Remove handwritten hacks when linking against MPI ([common#211](https://gitlab.dune-project.org/core/dune-common/-/issues/211))

#### dune-geometry
- Christoph:
  * merging geometry and localfunctions (see dune-common)
    - *Andreas:* I would like this to be discussed on the 22/01 - all developers seem to be able to attend then

#### dune-grid
- Santiago:
  * Discuss the suitability (pros vs cons) to lead the dune grid as a set of C++20 concepts ([grid!377](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/377) and also [common!816](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/816))
    - *Andreas:* I would like this to be discussed on the 22/01 - all developers seem to be able to attend then

#### dune-localfunctions
- Christoph:
  * merging geometry and localfunctions (see dune-common)

### Other topics

#### PDELab
- Linus: 
  * Whether and how to use dune-testtools in dune-pdelab needed for ([pdelab!537](https://gitlab.dune-project.org/pdelab/dune-pdelab/-/merge_requests/537))
    - *Robert:* Why does this need to be discussed at the DUNE core developer meeting? It only concerns PDElab users, does it not?
    - *Christoph:* I am with Robert, should be discussed in a different meeting.


## Meeting minutes

### From January 8

minute taker: Christoph

#### Infrastructure:

- Do we really want a merge commit even for very small changes?
  * Technically, that would be a fast-forward merge, cf. https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html
  * There exists an according GitLab feature request: https://gitlab.com/gitlab-org/gitlab/-/issues/1535
  * We won't change the default, maybe after GitLab introduces fast-forwards per MR, we will enable it

#### dune-common:

- Optionally pass source / target ranks to communication DataHandles. (LS: [common!870](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/870))
  * Linus' proposal adds data that is already available but wasn't used in the past, it is backwards compatible
  * Christian's proposal is more general and might be more complicated
  * Vote on Linus proposal (additional interface, keep both): **Proposal:** 6 / **Keep as it is:** 0 / **abstain:** 3
  * => *Linus* is allowed merge, maybe after a rebase and cleanup
  * In future, we can think about deprecating and removing the current interface to just have the new one

- Dropping broken pkg-config support ([common#188](https://gitlab.dune-project.org/core/dune-common/-/issues/188))
  * Kaskade7 might be a first user, we can think theoretically of further users 
  * Does not include preprocessor flags set in config.h by DUNE's buildsystem (`HAVE_*`); if we do want users to not have to use a specific build system, these would need to be written to a `${module-name}-config.h` (and installed) or included in the pkg-config file. The use of `ENABLE_*` flags should probably be avoided in this case as well and the flags namespaced (e.g. `DUNE_HAVE_*` instead of `HAVE_*`)
  * => We are not sure whether this can be fixed (easily). We postpone a decision, maybe *Christian* or *Simon* are able to get a better feeling for what needs to be done.

- required TBB version (SP: [common#204](https://gitlab.dune-project.org/core/dune-common/-/issues/204))
  * Issue is related with CMake support as more recent versions provide a CMake config file.
  * => TBB 2017 seems to be fine for everybody. *Simon* will provide a MR to give people a chance to try.

- Deprecate `DUNE_DEPRECATE` and `_MSG` (CGrü: [common!777](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/777))
  * Vote on accepting [common!777](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/777) and deprecate `DUNE_DEPRECATED`, use C++'s `[[deprecated]]` and mandatory adding the Doxygen deprecated command instead: **Yes:** 6 / **No:** 1 / **Abstain:** 2

#### dune-grid
- René: Add capability for backward iteration over grids
  * Robert: This should be added as utility first with a default implementation for all grids using entity seeds, interface can look like a grid view.
  * Why not bidirectional iterators and using `std::ranges::reverse_view` or such?
  * Use case: Matrix-free methods
  * => *René* will look into the iterator of `YaspGrid`, he can use C++20 ranges for bi-directional iterator.

- Capabilities in general: implementation and support is patchy and missing in some cases, e.g.
  * `hasEntityIterator` missing (MB: [grid!431](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/431))
  * `canCommunicat`e missing for several grids, e.g. `UGGrid` and `IdentityGrid` (BF: [grid#78](https://gitlab.dune-project.org/core/dune-grid/-/issues/78))
  * => Agreed on Robert's suggestion.

- Capability `isTwistFree` (or similar, see (RK: [grid!406](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/406)))
  * The MR lacks a precise definition of twist. That should be added upon the next meeting.

#### dune-istl:
- Is it okay to merge (OS: [istl!406](https://gitlab.dune-project.org/core/dune-istl/-/merge_requests/406)) (`ColCompMatrix` renaming and cleanup)?
  * dune-FEM uses the interface, *Robert* will have a look how difficult it would be to make Oliver's MR compatible with Dune-FEM (A fallback and deprecation has been implemented that works with dune-fem and hopefully also with other codes)
  * the interface will be marked explicitly internal

### From January 19

minute taker: Simon

#### Infrastructure:
- Remove outdated pages from website (SP: [website#87](https://gitlab.dune-project.org/infrastructure/dune-website/-/issues/87), [website#88](https://gitlab.dune-project.org/infrastructure/dune-website/-/issues/88))
  * See restructuring of documentation menu
- Provide CI-tested installation instructions (Might need special CI treatment due to calling apt etc.)
  * We need a concrete proposal
  * core, pdelab, fem
  * need to create a special docker image in the registry
  * *Andreas:* CI testing pip would be great! Would also like to deploy pip packages through CI if possible
- Suggestion for documentation menu (AD: [website#93](https://gitlab.dune-project.org/infrastructure/dune-website/-/issues/93))
  * System requirements must be documented for each dune version
  * Be aware of the menu levels!
  * "Installation instruction" should be prominent
  * General agreement that a better structure should be found
  * transition between "new users" and "developers" is smooth
  * Further discussion in a MR
- Downstream Testing! ([common!913](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/913))
  * Yes, at least test all downstream core modules
  * compare to the dune-uggrid gitlab-ci.yml
  * nightly docker images shows broken system
  * Idea: add another repository that runs nightly full system tests (done: https://gitlab.dune-project.org/infrastructure/dune-nightly-test)
- CI: debian11 builds fail
  * Maybe a runner configuration problem
  * Nils is investigating

#### dune-common
- Fix the pkg-config files 
  * see initial MR ([common!910](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/910))
- config.h:
  * Idea: remove "code" stored in `config.h` files
  * Move defines to compile flags
  * Proper proposal on Jan. 22nd

#### dune-grid
- VTKWriter: 
  * bug fix parallel write and filenames (RK: [grid!444](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/444)) (merged)
  * Fixes a bug with `write(path/to/filename)` in parallel
  * Suggestion: Add test that files are written in the correct directory
  * Can be merged. (done).
- What about Simon’s dune-vtk module? Can we merge some of that into dune-grid?
  * Form a group to discuss issues and a path to get the modules into dune-grid
  * Same question for dune-gmsh.
  * Simon will send an invidtation to this discussion group
- subId: forward for all codimensions (MN: [grid!279](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/279)) and also (MN: [grid#28](https://gitlab.dune-project.org/core/dune-grid/-/issues/28))
  * Regarding multiple `index()` implementation: the idea to fix it is good, but someone needs to submit the "Ansgar-proposal" as merge-request. Keep the issue open.
  * deprecate the `index<cc>()` method
- Communicate over intersections, status? (RH: [grid!13](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/13))
  * We need a concrete application and mathematical formulation and somone who is interested in this topic.
  * Close the MR! (done).
- `Mapper::size` and `IndexSet::size` should return `size_t` or some unsigned type (AB: [grid#10](https://gitlab.dune-project.org/core/dune-grid/-/issues/10))
  * Proposal: use `auto` as return type in `size()` methods and the implementation can/should use some unsigned integral type
  * Robert ([grid!474](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/474)) (merged).
- fix docu for globalId: (AB: [grid#47](https://gitlab.dune-project.org/core/dune-grid/-/issues/47))
  * It should be possible to communicate global IDs via MPI
  * What is a formulation for this requirement?
  * Require that memcpy works on that type!
  * Needs to be checked whether this is fine for concrete grid implementation.
- module dependencies upper bound (LS: [grid!326](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/326))
  * Pro: This documents more precisely which versions are supported
  * Con: We compare against versions not yet released, e.g. `dune-geometry (>=2.7 && <2.8)`
  * Needs to be checked whether version expressions are correctly parsed (in cmake)
