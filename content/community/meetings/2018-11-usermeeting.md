+++
title = "Dune User Meeting 2018"
+++

# Dune User Meeting 2018 in Stuttgart

## Invitation

We invite all Dune users to participate at the 6th Dune User meeting, to
be held in Stuttgart from 05.11.2018 to 06.11.2018.

This meeting should be used to showcase how you're using Dune,
foster future collaborations between the users and provide input to the future development of
Dune. The meeting also welcomes contributions from other frameworks such as
DuMuX and OPM that are using DUNE.

We keep the format rather informal. All participants are encouraged
to give a presentation. In addition, we will reserve plenty of time
for more general discussions on Dune.

Financial support for the workshop is provided by the INTPART project InSPiRE funded by the 
Research Council of Norway. 

A online registration and more detailed information can be found at the
[workshop page](https://www.ians.uni-stuttgart.de/institute/news/events/Dune-User-and-Developer-Meeting-2018/).

## Location 

[campus.guest](https://www.campus-guest.de/kontakt/), 
Universitätsstraße 34, 
70569 Stuttgart

*Room 202*

## Program

### Monday, November 5, 2018

  Time             |  Title / Person          | Description / Talk Title
 ----------------- | ------------------------ | ------------------------------------------------------------------------------------------------------------------------------
   9:30            |  Welcome                 | 
  10:00-11:20      |  User Presentations      | Chair: B. Flemisch
  10:00            |  D.Kempf                 | *dune-codegen: Using code generation for performance portable implementation of finite elements*
  10:40            |  T. Leibner              | *Generic discretization module (dune-gdt)*
  11:20-11:50      |  Coffee break            |
  11:50-12:40      |  User Presentations      | Chair: A. Dedner
  11:50            |  M. Koch                 | *Consistently Orienting Edges of Dune Grids*
  12:10            |  R. Klöfkorn             | *Status of hp-Adaptivity in DUNE-Fem and OPM*
  12:40-14:00      |  Lunch                   | [campus.guest](https://www.campus-guest.de/kontakt/)
  14:00-15:20      |  User Presentations      | Chair: C.-J. Heine
  14:00            |  L. Seelinger            | *Efficient Iterative Solution and Multiscale Approximation of Anisotropic and Heterogeneous Problems*
  14:40            |  W. Giese                | *Multi-scale modelling of calcium cycling in cardiomyocytes: Efficient parallel simulations with space-time adaptivity*
  15:20-15:50      |  Coffee break            |
  15:50-16:30      |  User Presentations      | Chair: R. Klöfkorn
  15:50            |  A. Dedner               | *Writing Python bindings for Dune modules*
  16:30-17:15      |  Discussion              | 
  17:15-18:15      |  Ice breaker             | *Bistro [campus.guest](https://www.campus-guest.de/kontakt/)*
  19:00            |  Dinner                  | [Restaurant & Cafébar am Allianzstadion](http://tsv-georgii-allianz.de/restaurant-cafebar/), 10 Hessbrühl Straße, 70565 Stuttgart-Vaihingen


### Tuesday, November 6, 2018

  Time             |  Title / Person          | Description / Talk Title
 ----------------- | ------------------------ | ------------------------------------------------------------------------------------------------------------------------------
   9:30-10:50      |  User Presentations      | Chair: R. Klöfkorn
   9:30            |  G. Corbin               | *Making numerical experiments in Dune reproducible*
  10:10            |  T. Koch                 | *The DuMux Pub: A Concept for Reproducible Research with DuMux*
  10:50-11:20      |  Coffee break            |
  11:20-12:40      |  User Presentations      | Chair: A. Dedner
  11:20            |  M. Utz                  | *Dune-SWF an application for river engineering*
  12:00            |  L. Stadler              | *Why we use XDMF/HDF5 for Dune-SWF*
  12:40-14:00      |  Lunch                   | [campus.guest](https://www.campus-guest.de/kontakt/)
  14:00-15:20      |  User Presentations      | Chair: B. Flemisch
  14:00            |  J. Both                 | *Simulating flow in deformable porous media*
  14:40            |  S. Burbulla             | *A Finite-Volume Approach to Dynamically Fracturing Porous Media*
  15:20-16:00      |  Discussion              |
  16:00            |  Coffee break            |

## Participants

Participant         | Affiliation
------------------- | ------------------------------------------------------------
Alkämper, Martin    | Universität Stuttgart
Altenbernd, Mirco   | Universität Stuttgart
Blatt, Markus       | HPC-Simulation-Software & Services
Both, Jakub         | University of Bergen
Burbulla, Samuel    | Universität Stuttgart
Chamakuri, Nagaiah  | University of Hohenheim
Corbin, Gregor      | Technische Universität Kaiserslautern, AG Technomathematik
Dedner, Andreas     | University of Warwick
Dreier, Nils-Arne   | University of Münster
Engwer, Christian   | Universität Münster
Fahlke, Jorrit      | WWU Münster
Flemisch, Bernd     | Universität Stuttgart
Giese, Wolfgang     | Max-Delbrück-Centrum für Molekulare Medizin in der Helmholtz-Gemeinschaft
Heine, Claus-Justus | Universität Stuttgart
Hilb, Stephan       | Universität Stuttgart
Kane, Birane        | NORCE Norwegian Research Center
Kempf, Dominic      | Universität Heidelberg
Heß, René           | IWR, Universität Heidelberg
Klöfkorn, Robert    | NORCE Norwegian Research Center
Koch, Marcel        | Universität Münster
Koch, Timo          | Universität Stuttgart
Müthing, Steffen    | Universität Heidelberg
Leibner, Tobias     | Universität Münster
Sander, Oliver      | TU Dresden
Seelinger, Linus    | Universtät Heidelberg
Stadler, Leopold    | BAW-Federal Engineering and Research Institute
Utz, Martin         | BAW-Federal Engineering and Research Institute
