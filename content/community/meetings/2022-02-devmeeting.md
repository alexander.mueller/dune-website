+++
title = "Dune Developer Meeting 2022 Online"
+++

Date and Location
----

The developer meeting is planned to be an online meeting on Friday Feb. 25th, 09:00 - 13:00, and Tuesday Mar. 01st, 09:00- 13:00.

We invite all DUNE developers and contributors to participate.

**Location:** [BigBlueButton](https://bbb.tu-dresden.de/b/sim-a9l-8tt-vil)

## Meeting minutes Feb. 25th

Attendees: (Core devs in bold, new core devs in italics)
**Carsten**, **Dominic**, **Christoph** (away between 11:20 and 12:05), Nils, **Oliver**, Rene Heß, Santiago, **Simon**, Alex Müller, *Timo Koch*, **Andreas**, Maik Porrmann, **Markus**, **Ansgar** (joined 9:14), **Christian** (joined 9:20), *Samuel Barbulla* (joined 9:20), **Peter** (joined 9:40)

### New Dune core developers
- Samuel Burbulla: yes 7 / no 0 / abstaint 0
- Timo Koch: yes 8 / no 0 / abstaint 0

=> Welcome to the group of Dune core developers!

### Review meeting minutes from last Dune developer meeting
- A lot of topics we discussed were successfully implemented: better CI/CD pipeline (nigthly) testing, Python bindings
- some were worked on but are not merged or no code was written yet
- Overall we like what we did since the last meeting

### Remove support for Debian 10 and Ubuntu 18.04
- Christoph: Reduce number of images we test and move on
- Andreas: We could drop support after a not-so distant Dune 2.9 release
- Christian: Don't drop support for the sake of dropping support, we should be feature-driven
- Ansgar: We should decide what compilers to support and this should lead to supported distributions
- Christian: We should look at HPC installations, SPEC could guide us

=> we drop CI/CD images if they are using unsupported Compiler / CMake / Python versions

### Minimum versions of GCC, Clang, CMake, Python
1. GCC: bump version number to GCC 9 after Dune 2.9 release
2. GCC: ask on mailing list if users have concerns regarding bump to GCC 10 after Dune 2.9 release
3. Clang: bump version number to Clang 10 after Dune 2.9 release
4. Intel support: Drop unofficial support for ICC, we try to be compatible with oneAPI, we accept patches, but we do not test it (until someone sets an according image up)
5. CMake: bump version number to CMake 3.16 after Dune 2.9 release
6. Python: introduce Python minimum version 3.6 as documentatino of the status quo
7. For a release, we are going to document the required version in the CHANGELOG.md of dune-common

Vote on 1-6: yes 12 / no 0 / abstaints 0

### Do we require Python + future of dunecontrol and dunproject
- We require Python for the Python bindings. Should we require Python in general to use it for, e.g., `dunecontrol` or `duneproject`
- Should we also install the Python dev headers?
- Python-support on Windows is infierior compared to compilers and CMake
- With `duneproject` replaced by a Python-based solution, we could provide more features and a better user experience
- The improvements to the CMake build system do not contradict a Python-based dunecontrol

=> If somebody wants to make a change that would require Python, we have to decide on Python becoming a Dune dependency

### CI/CI pipeline
- We need more resources, Oliver says we should order it
- Some runners are bound to Münster or Dresden, Simon suggests to have a look into this issue
- Christian: We should improve the tags to better reflect features
- Having Pip support is difficult

### Changelog on webpage
Make links to changelog of different release versions appear equal
- easiest option: provide a link to each individual releases' changelog file
- other option: copy the changelog.md file to the website (in the website build) as a proper page
- [x] Replace Recent changes on website by a reference to the `CHANGELOG.md` of master and a list `CHANGELOG.md` from the releases
  (Done: [infrastructure/dune-website!436 (merged)](https://gitlab.dune-project.org/infrastructure/dune-website/-/merge_requests/436))

### dune-geometry
#### Transposed jacobian and jacobianInverse
- `geometry.jacobianInverseTransposed()` might not be the right thing for vector-valued finite-elements
- Proposal: Introduce a method `jacobianInverse()`, with a minimal interface requirement, needs a concrete MR. Not as a replacement but as an addition with a default implementation.
- [ ] Carsten + Oliver try to provide a MR [core/dune-grid!577](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/577), [core/dune-geometry!193](https://gitlab.dune-project.org/core/dune-geometry/-/merge_requests/193)

#### Copy-Assignment of Geometry
- See issue [dune-grid#140](https://gitlab.dune-project.org/core/dune-grid/-/issues/140)
- Should Geometry be copyable? Yes.
- [x] Simon opens a MR to make copy-assignment possible. [core/dune-grid!576 (merged)](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/576)

#### Higher-order Geometries
- Proposal for two parametrized geometry types: `ParametrizedGeometry` [dune-geometry!170](https://gitlab.dune-project.org/core/dune-geometry/-/merge_requests/170) and `MappedGeometry` [dune-geometry!169](https://gitlab.dune-project.org/core/dune-geometry/-/merge_requests/169)
- [ ] fix the definition of derivative: should be w.r.t. local coordinates. [core/dune-geometry!192](https://gitlab.dune-project.org/core/dune-geometry/-/merge_requests/192)
- not relation to a real grid element should be required.
- What is the difference between the two implementations? `ParametrizedGeometry` explicitly makes use of a LocalBasis, `MappedGeometry` uses a general differential mapping.

### dune-localfunctions
#### H2-conforming local finite-elements
- Derivatives of functions in local-interpolation: Maybe use a general interface of differentiable function (already in dune-functions)
- We need a proper proposal, e.g. for the extension of the virtual interface. For some H2-conforming finite elements, we need a tighter connection between the local geometry and the global geometry.
- Maik prepares a proposal that covers both dune-functions and dune-localfunctions aspects

### dune-istl
#### const-correctness of linear solvers
- change the constructor/what is stored in the LinearSolver, See MR [dune-istl!421](https://gitlab.dune-project.org/core/dune-istl/-/merge_requests/421)
- backward-compatibility? Is it a breaking change? yes but the interface is very recent so hopefully a change does not influence/break so much user code

#### Improvements of the solver-factory
- also here the interface is changed. See MR [dune-istl!453](https://gitlab.dune-project.org/core/dune-istl/-/merge_requests/453)
- Can we introduce macros with a different name? Prefer clean interface for the future. We need feedback from more developers/users to find the clean interface
- TODO: Check whether discretization frameworks break if these two branches are used!


## Meeting minutes Mar. 1st 

Attendees: Markus, Nils, René, Santiago, Simon, Samuel, Ansgar, Christoph, Maik, Oliver, Timo, Andreas, Robert (joined 9:45 am), Carsten (tried to join 11:15 am)


### Future of Alberta
- Christoph: Alberta has no development activity, future unclear, should we fork it or drop support for Alberta?
- Simon: Still used from time to time in his group, no reason to drop it
- Decision: keep it as it is, we might consider taking over ownership and move it to GitHub, apply patches (Christoph will do so, Simon will get admin rights) and make a small 3.0.4 release

### CMake improvements
- Simon wants to improve CMake build system to make using Dune CMake functions more optional, further the handling of the CXX standard might be changed. MR should appear probably after Dune 2.9 release.
- Christoph encourages Simon to create incremental Merge request and get writte code included to prevent bit-rot
- Christian is concerned regarding the CXX handling, but Simon is only offering more flexibility, not advertising mixing different C++ standard version

#### What to do with TBB? 
- Five years ago we decided to use TBB but no code using it emerged in the core modules.
- Should we remove "find(TBB)" and ask the downstream module to add this code in their module?
- We decide to stick to the status quo and keep and include findTBB the way it is.

### MPI Communicator
- Copy MPI communicator or re-use a single one?
- Decision: Extend MPIHelper by adding a new function to handle this. Deprecate default constructor of `Communication`.

### Capability to indicate conforming grid
- Beside static conforming flag (grid guarantees to be conforming by design), we should add a dynamic `isConforming` flag, that indicates that an actual grid is conforming despite the underlying grid might be able to handle non-conforming grids. (MR dune-grid!139)
- [x] Decision: isConforming needs to be added. We will add a default implementation forwarding to conforming: [core/dune-grid!578 (merged)](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/578)

### Linear Algebra issues
- Use concepts in tests to check for lineare algebra container inferfaces and make them more consistent.
- We want to allow concepts to be part of tests that are allowed to fail and are only built if the compiler support concepts.
- There will be merge request with an actual porposal which can be discussed there further.

### Name of default branch master or main
- Do we want to rename the default branch now? (if effort is reasonable)
  * Vote: yes 6 | nu 4 | abstaint 1
- See summary: [infrastructure/issues0#67](https://gitlab.dune-project.org/infrastructure/issues0/-/issues/67)

### Hosting GitLab of dune-project 
- Christian: ask lokal admins whether GitLab can be hosted and maintained in Münster 
- Oliver: Could be hosted in Dresden, Simon+Ansgar would do the maintaining 
- Oliver: We could try to give others outside of Dresden also admin access (of possible)

### Github.com/dune-project
- We also have https://gitlab.com/dune-project
- Whoever wants admin access to these project, reach out to Santiago.

### Publish release notes as a citable document
#### Discussion:
- Write up change log to become an article in ANS
- Have a GitLab project per release notes
- Timo will coordinate the release note publication for 2.9.
- We have to reconsider, where we want to publish releases in what form.
- Alternative journals, e.g. Journal of Open-Source Software

#### Decision:
- We need to find out the scope of JOSS, maybe alternative Zenodo
- Only if effort is not too high 
- Make the Dune releases citable 

### Use of Debian package
- Oliver and his group are using and mainting them
- If issues arise with mixed packages from deb and self-compiled, we will fix them

### Empty rows in BCRSMatrix
- See MR dune-istl!102
- From hanging nodes rows with zero diagonals may occur.
- Will be worked out in a MR

### Breaking changes in Python bindings
- Python bindings should be improved before making it official by a release
- In near future we should have a Python binding meeting
- Meeting for more discussion (see below)


## Left-over Topics:

### Infrastructure

- Robert: Version numbering and release cycle. 
  * Simon: Current status = 2.x, where x is our "major" release number, increased approx. once a year. 
  * Alternatives? Semantic versioning https://semver.org/ , Calendar Versioning https://calver.org/
- Robert: Web page, broken links, outdated info etc.


### Python bindings

- Robert: [dune-common:!1064](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/1064), make Portalocker an optional dependency.
- Robert: Precompiled Python modules, e.g. [dune-geometry:!153](https://gitlab.dune-project.org/core/dune-geometry/-/merge_requests/153)
- Timo: Use format checkers `pylint` and `black` and enforce through CI


### Core

- Christoph: How to proceed with the deprecated dune-grid-howto? We should move it out of core and create some "attic" or "graveyard" project in GitLab.
- Christoph: Remove git tags and tarballs for release candidates. They clutter the folder / tag output for no benefit. We could keep the RCs for the last stable release.
- Samuel: We thought about a python package `dune` at PyPI that would enable `pip install dune`. It could offer an easy entry poiny to DUNE, but what would it include? Just dune-grid? Or also dune-fem with an example solving a PDE?
  * Timo: If using the name `dune` I think this can only contain the core modules (maybe staging). For dune + dune-fem there already is `pip install dune-fempy`, no?


### Build system

- Timo: Separate handling of C++-language code and Python-language code in the build system (separate targets / commands / SRP), no silent magic. Related to [dune-common#279](https://gitlab.dune-project.org/core/dune-common/-/issues/279)
- Timo: User friendlyness of CMake output log. Example/problem: New user read the output and find many "foo could not be found" -> many think might be bad and go and try to install the dependency without knowing what for.
  * Is there a possibility to distiginguish between developer output and user output?
  * Is is really useful for a user to know that an optional has not been found? Isn't a list of the _found_ dependencies enough?
  * If an (optional) dependency is not found it always sounds alarming; unless I know what exactly I will miss out on


### dune-grid

- Robert: [dune-grid:571](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/571)



## Meeting minutes Mar. 3rd (Python)

### We collected several use cases of the python bindings that should be kept in mind
  - Create external venv, call dunecontrol; install packages with pip
  - Without any venv, setting the PYTHONPATH to the build directory
  - Packaging for PyPI; also combining PyPI packages and source modules
  - In CI: modules are built separately in docker containers; dune-testtools
  - Python tests should be configured correctly automatically; also for embedded Python
  - Should still be possible to build DUNE modules one after another; important for Debian packages
 
### Current problems
  - No venv -> no Python bindings (e.g. ubuntu with missing python3-venv); Should be possible to deactivate the venv and still get Python bindings
  - dune-testtools is not working any more (At least, how Timo is using it)
  - Core modules auf PyPI need update
  - Building DUNE fails if pip install fails
  - The PYTHONPATH of the Dune modules is not exported at the moment


### Discussion and suggestions
  - Is it really necessarty to require venv to use the Python bindings?
    Idea: If there is no venv found (or it is disabled by a new CMake variable DUNE_DISABLE_PYTHON_VIRTUALENVIRONMENT), generate a script that sets the PYTHONPATH correctly and checks all Python dependencies.

  - Why is `pip install` called at `configure`?
    So that internet connection is needed only during `configure`.

  - What about more precompiled Python modules, e.g. ReferenceElements? Consensus is rather (except Andreas) for having more precompiled modules.
 
  - Do we need shared libraries for singletons? Shared libs made problems in the PyPI packages (RPATH was not correct). A workaround in dune-fem was to move all singletons into the python object _fem.so.
 
  - Pybind11: Should we use the pybind11 package provided at PyPI? Yes. If it is available on the system already use this one, otherwise install pybind11.
    Proposal: Add deprecation warning in pybind11.hh now and remove pybind11 headers only after 2.9. The include paths have to be adapted (`dune/python/pybind11/` -> `pybind11/`)
 
  - There is no CMake target any more that builds everything except the Python bindings. Timo requests such additional target.
    It is no controversial to add an additional target, the question is what `all` would be?
    One idea: Introduce CMake variable which decouples the python targets from `all`.
    Better idea: Move the python targets into the configure step if possible.
