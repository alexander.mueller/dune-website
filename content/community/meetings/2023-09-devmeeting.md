+++
title = "Dune Developer Meeting 2023 in Dresden"
+++

Date
----

The developer meeting is held on September 19th and 20th, 2023 at the Technische Universität Dresden.

The meeting is co-located with the [Dune User Meeting 2023](/community/meetings/2023-09-usermeeting).

Participants
-------------

- Simon Praetorius
- Oliver Sander
- Robert
- Carsten
- Christian Engwer
- Santiago Ospina
- Christoph Grüninger (only Wednesday, only remote)
- Andreas Dedner (remote)

Video conference
----------------

The developer meeting will be streamed and allows online participation via BigBlueButton.

[Start BBB videoconference](https://tud.link/gza0)


Proposed topics
---------------

Please use the edit function to add your topics or write an email to [the organizers](mailto:meeting-2023@dune-project.org).

## General

* CGü: Propose appointing Santiago Ospina De Los Rios ( Heidelberg University, IWR) as a Dune Core Developer
* RK: Developers (old and new) and future management structure!
* RK: Stability of the gitlab infrastructure. How can we help to improve it?
* CGü: I'd like to see an hour of cleaning up. Together we scan (one or two of)
  - merge requests
  - bugs
  - the remaining open FlySpray tasks
  - Git branches
  and decide which can be closed / deleted right away and which to keep.
* CGrü: Release 2.9.1 is [blocked by two topics](https://lists.dune-project.org/pipermail/dune-devel/2023-August/003040.html),
        I did not receive too much feedback or support.
* AD: 2.9.2 release and new release paper

## dune-common

* OS: Simon and Santiago have invested a lot of time thinking about improvements to the build system.
  Can we decide on a way forward?  Can some of the work be merged?
* SO: MPIHelper [dune-common:1274](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/1274)
  - RK: What about MPI_Comm_dup while we are at it?
* SO: Add a fixed-size multi index: `Dune::TypeTree::HybridTreePath` -> `Dune::HybridMultiIndex`.
* SO: What is a Matrix? [dune-common:#253](https://gitlab.dune-project.org/core/dune-common/-/issues/253)
* SO & SP: Does anyone uses `${ProjectName}-config.cmake.in`? We would like to reomve it.

* CGü: Who is interested in SIMD (Vc-devel)? It is currently broken in master and releases/2.9, blocking the 2.9.1 release
       and `std::simd` is making further progress.
* CGr: Get rid of greedy generic implementations (`operator<<` and `FieldTraits`).

## dune-geometry

* AM: `GeometryType::none` default to simplex behaviour. Is this on purpose? [dune-grid!32](https://gitlab.dune-project.org/core/dune-geometry/-/issues/32)

* AM: Higher order derivative information and transformation


## dune-grid

* `VirtualGrid` (aka `PolymorphicGrid`) (roadmap?) [dune-grid:661](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/661)
* OS: Can `PolymorphicGrid` simplify the Python bindings?
* OS: Can we get `dune-gmsh` into `dune-grid`?
* OS: Can we get `dune-vtk` into `dune-grid`?

## dune-localfunctions

* RK: Shape function evaluations should be thread safe. [dune-localfunctions:22](https://gitlab.dune-project.org/core/dune-localfunctions/-/issues/22)
* SO: RT shape functions only for tets [dune-localfunctions:20](https://gitlab.dune-project.org/core/dune-localfunctions/-/issues/20)
* OS: Support for matrix-valued FE like the Arnold-Winther element

## dune-istl

## Discretization modules (FEM, PDELab, ...)

## Python support

* OS: What is the current state of the Python bindings?
* OS: Can we get some/better documentation about how to write bindings for you own code?  (For example, dune-functions bases?)
* OS: Are Julia bindings conceivable?`
* CGü: Can we decide a way forward for [drop or simplify pip & venv magic during configure (dune-common:330)](https://gitlab.dune-project.org/core/dune-common/-/issues/330)?
* CG: During CMake python packages are installed implicitly from the web, this is dangerous and inconvenient (e.g. DUNE does not build without internet access).

## etc

* RK: To twist or not to twist? I would like to hear opinions [dune-alugrid:89](https://gitlab.dune-project.org/extensions/dune-alugrid/-/merge_requests/89)
* AD: Perhaps we should consider if we have ideas for GSoC this time.

# Protocol


1. Vote on Santiago Ospina De Los Rios ( Heidelberg University, IWR) becoming a Dune Core Developer<br>
  9 yes, 0 no, 0 abstentions
2. Infrastructure
   - Jö is still maintaining the mailinglist server. Can we transfer this? **-> Markus offers to take over maintainance**
   - gitlab is now fully running in Dresden. Ansgar is part-time taking care.
   - some runners are broken, see Issue ???
3. Cleanup MPIHelper
   - [dune-common:1274](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/1274)
   - Should we use `MPI_Comm_dup`? **-> transition code that can be enabled/disabled**
   - `MPI_Comm_dup` should be fixed along the semantic interposition problem (different instances of the object in different ocmpilation units).
4. SIMD interface to Vc
   - latest Vc versions don't work with Dune anymore (see core/dune-common#350).
   - first we should check which Vc versions work with the old interface
   - second we should add interfaces to the new versions
   - potentially bump the requirements or even suppport both versions
   - **Christian** will take care of this.
5. **Santiago** will prepare a proposal for a `HybridMultiIndex`.
6. Cleanup CMake
   - no body is using `${ProjectName}-config.cmake.in` directly, so Santiago and Simon are free to change that part.
7. `VirtualGrid` ... Roadmap?
   - Naming? **yes**: `PolymorphicGrid`
   - Communication doesn't yet work fully. **-> to be checked**
   - A couple of optimizations are proposed. **-> postpone**
   - Should this be merged into `dune-grid` (in namespace `Dune::experimental`), in particular as Samuel left? **-> not yet, leave the MR open**
8. Can we merge `dune-gmsh4` and `dune-vtk` into `dune-grid`?
   - we start with a small change in `dune-grid` to suggest `dune-gmsh4` and potentially forward when reading `gmsh4` files.
   - there is common interest to merge `gmsh4` on the longer run and we are willing to invest work.
   - go in multiple steps to (1) merge a "minimal" Gmsh4 reader with out higher order support (2) check where the grid factory interface needs to be extended (3) merge/update the remaining higher order stuff...
9. See issue core/dune-localfunctions#22 
   - we must properly document the guarantees (add to the main `dune-localfunctions` documentation) (**-> Santiago & Carsten**)
   - shape function evaluations should be thread safe *if* the local functions objects are thread local
   - this implies that shared state between different instances is at least dangerous and must be handled with great care.
   - potential raise conditions due to shared state (e.g. `std::shared_ptr` in the monimial class of the generic finite elements) must be fixed. (**-> Robert**)
10. Future development process / project management processes
    1. Santiago: The entrance level is quite high and the mailing list is not really used anymore. How can we lower the entrance level?
       - can we make gitlab more open? -> service desk? -> enable github,google,etc. login -> ?
       - can we offe some other communication platform? -> we will try a matrix channel (via the official matrix server).
       - we want to improve the "Getting involved" information.
    2. should we formalize certain processes, e.g. 
       - how do I become a (core) developer?
       - which responsibilities come with core dev status?
       - when/how do people become alumni?
       - how are interface changes decided?
       - how do we decide on new feature?
       - **->** a lot of discussion not written down.
11. How is the general opinion on Julia bindings? WIAS is considering building upon DUNE for their Julia PDE solvers **->** we welcome contributions, at least initially as a `dune-julia` module/repository.
