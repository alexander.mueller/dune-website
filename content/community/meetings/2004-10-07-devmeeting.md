+++
title = "Protokoll des Dune Treffens am 7.10.04"
+++

11.30 Beginn

1) Berichte
-----------

  - Robert: 2 Phasen ccfv implementiert, adaptiv und parallel, dof manager, domain manager
  - Mario: upscaling Anwendungen
  - Oliver: UG-Schnittstelle, neue Mitarbeiter in Berlin
  - Peter: Parallele Schnittstelle, ISTL, 2 neue Mitarbeiter in HD
  - Christian: DG für komplexe Gebiete in Debugphase

2) Aufräumen/Modulstruktur
--------------------------

Quadraturen in ein eigenes dune/quadrature Verzeichnis (da gibt es
dann auch mal einen Artikel dazu)

istl/src ist blöd, da man dann dune/istl/src/xyz.hh includen muss

lib/io -> io/file

grape Sachen aus duneapps in io/visual/grape

io/visual hat selbe Struktur wie dune/grid

write/read aus der Gitterschnittstelle entfernen, soll aber in
einzelnen Implementierungen vorhanden sein (sgrid, albertgrid)

Allgemeiner I/O (Serialisierung bzw. persistente Objekte) soll es
später geben.

Robert: Legt Semantik der BoundaryEntity fest. Ist eine Entity mit
eingeschränkter Funktionalität

ReferenceTopology fertig programmieren, Aufrufe in Default Klassen
einbauen

Coding style strikter durchhalten!

evaluate vs operator() bei analytischen Funktionen...

Reference Parameter vs. Rückgabewerte: Oliver meint das würde der
Compiler wegoptimieren (ab gcc 3.4).
  -- Wir brauchen ein Testbeispiel ob es wirklich funktioniert.
  -- Wir brauchen eine Liste der betroffenen Funktionen

`Vec<n,T>`, `FixedArray<T,n>` soll einheitlich sein. Kein Default Type
mehr. NEU: `FieldVector`, `FieldMatrix` wandern nach common. `FieldVector`
ersetzt `Vec`, `FieldMatrix` ersetzt `Mat`. Problem: `Mat` ist spaltenweise
organisiert. `FixedArray` bleibt. Braucht man für das paper...

`const` !!! Ja wir wollen das! Muss bottom up eingebaut werden. Soll für
das Paper fertig sein. Hohe Priorität.

`feop.hh` vs. `feoperator.hh`. Unterschied ist in on-the-fly
implementation. `feop.hh` sollte `feoperator.hh` ersetzen. Mario macht
Vorschlag, stimmt mit Oliver ab und dann wird es ersetzt.

Alle Operatoren in `dune/operator` -> Robert

BoundaryType muss aus `grid.hh` raus

boundary ID wie subindex() nur von elementen aus zugänglich machen.

duneapps ist ein Müllhaufen! JEDER löscht seine Sachen da raus, die er
nicht mehr braucht. Allgemeine Sachen sollen nach dune. Das
Verzeichnis soll bleiben und advanced Beispiele enthalten.

`YaspGrid` Effizienz? Vergleich mit `simplegrid`! -> Peter (liegts an
`subindex()`?)

globally unique IDs
codim als template parameter für P-Klasse in communicate Funktion

Erweiterungen der communicate Methode: über leafs, über mehrere Level,
mehrere codims(?)

Private copy-Konstruktoren, operator=, etc., für Klassen wie Gitter

`ReferenceTopology` -> Robert


3) ISTL vorgestellt
-------------------


4) INDEX Definition
-------------------

index() : pro (codim,level,proc) eindeutig und konsekutiv ab 0, darf
sich nach Gitteradaption ändern

global_index() : pro (codim, proc) eindeutig, nicht konsekutiv (aber
micht "nicht zu großen Löchern") und persistent gegen Gitterumbau,
Robert prüft ob mit einer map die Löcher beliebig groß sein dürfen.

globally_unique_id() : pro (codim) eindeutig, nicht konsekutiv, mit
beliebig großen Löchern und natürlich persistent gegen alles.

Namen sind nicht optimal, wir überlegen uns eventuell neue Namen

Falls es mit map geht reicht die globally_unique_id()

globally_unique_id() liefert einen vom Gitter zu definierenden
Datentyp mit operator<


5) Paper
--------

Zeitschrift: Soll in Computing eingereicht werden.
 * Was soll rein?
 * Was muss in dune noch an code geschrieben werden dafür?
 * Wer macht was?

Priority List
-------------

 1. const
 1. Vec->FieldVector, Mat->FieldMatrix

TODO
----

Wir brauchen eine "Testsuite" von Anwendungen, die nach Umbauten
gecheckt werden müssen
