+++
title = "FAQ - Frequently Asked Questions"
[menu.main]
parent = "community"
weight = 30
+++

Questions that didn't find their way into this list may be found in the [Bug Tracking System](/dev/issues).

### General
#### How should I cite DUNE?
Have a look at the [publications page](/about/publications).

#### Why does DUNE not use boost?
We are trying to avoid hard dependencies of the core modules on external libraries. The reason is that software installations on really big supercomputers tend to be very old. Often the software is installed at the same time the supercomputer is built and essentially never updated. There is a good chance that there is no (recent enough) version of Boost installed.

Since we require compilers that fully support C++-11, most of the use cases for Boost diminish.

### Installation/Compilation
#### How do I start my own project?

First you have to get DUNE itself. This is explained in the [installation notes](/doc/installation). Then use the Dune project generator to create a simple application framework. This is done by going to where you want your application to be, and typing `dune-common/bin/duneproject`

The program will ask you for a project name, an initial version number and a maintainer's email. Supply all this and you get a new directory named after your project. In the directory you will find a sample .cc-file, the necessary AutoTools infrastructure and a README which tells you how to proceed from there.

#### configure fails if no f77 compiler is found.

If there is no fortran compiler installed, configure should work. If
this is not the case, please reopen [bug #185](https://gitlab.dune-project.org/flyspray/FS/issues/185).

Problems occur, if there is a fortran compiler installed but it does not match the C-compiler you are using (e.g. icc together with g77), because the tests try to link fortran from C, which cannot work.

A workaround (and crude hack) is to set F77=true. In this case the test sees that it cannot compile any fortran code and just warns about it.

#### What software do I need to use DUNE?
In order to build DUNE you need at least the following software:

* a standard compliant C++ compiler, tested are g++ and Clang; we try to stay compatibe with ICC
* pkg-config
* CMake

Detailed information on supported compiler and CMake versions
can be found in the [release notes](/releases) for releases
and in the list [recent changes](/dev/recent-changes) for the
development branch *master*.

The following software is recommend but optional:

* MPI (either OpenMPI, lam, or mpich suffice)

This will provide you with the core Dune features. The different grid managers like Alberta or UG have to be downloaded separately.

#### How do I set my favourite compiler options with this fancy build-system?

You can find information about this in the [build system documentation](/sphinx-impl/sphinx/core/dune-common.html#how-do-i-change-my-compiler-and-compiler-flags).

#### Can I use DUNE on a Windows System?

All DUNE development is currently done on Unix-like machines. For Windows you need a small set of patches. Please inquire on the mailing list. Be aware that we do not support Windows officially. This might result in larger response times and less ambitious feedback on the list.

#### How do I use the make headercheck feature?

`make headercheck` compiles all headers in your modules separately and thus checks whether
they include all headers they need. You have to enable it while configuring, see
[build system documentation](/sphinx-impl/sphinx/core/dune-common.html#why-is-it-not-possible-anymore-to-do-make-headercheck).

### The Grid Interface
#### Does a Grid Implementation Have to Provide Entities of all Codimensions?
The DUNE grid interface contains methods for access to grid entities of all codimensions. However, for some entities such as faces and edges in a 3d grid, this is sometimes difficult to implement. Also, most PDE codes will not need them anyways. Therefore, currently grid implementations are only required to provide vertices and elements. All others are optional. Please refer to the documentation for the individual grids to see what codimensions they implement.

Lagrangian finite elements of order higher than one have degrees of freedom on edges and faces. In order to implement them you don't need the actual entities, just getting their indices is enough. Therefore, the method subIndex<codim>() of IndexSet is required to be implemented for all codimensions.

#### Is There a Way to Organize a Grid in Separate Subdomains?

Some grid managers assign a domain id to each element. Using this id the grid can be viewed as consisting of several subdomains. Each subdomain can then, for example, have different material properties. Currently the DUNE grid interface does not contain this concept. There has been discussion about including it but so far nothing happened.

One reason for this omission is that the functionality is not strictly necessary. An element's domain id is element data just like a piecewise constant FE function. You can maintain a vector with the element ids in your application and feed it to your problem assembler to let it choose your material parameters correctly. **Update:** This mechanism has been implemented as the Dune meta grid [MultiDomainGrid](http://github.com/smuething/dune-multidomaingrid).

#### How do I implement my own DUNE grid?
Unfortunately, so far there is no text on how to implement your own grid manager using the DUNE interface. There is some example code in dune-grid, though. You'll find the implementation of `IdentityGrid`, which does nothing more than wrap another DUNE grid. It therefore already contains all the boilerplate code that you will need. Copy & paste this grid and add your own functionality!

#### Can I access Entities/EntityPointers by index?
No, this is not directly possible using the grid interface. The reason is that this cannot be implemented efficiently for some unstructured grid managers, hence you should not rely on it. In most cases you can restructure your code to use iterators and access your data using indices instead.

If you absolutely need random access you can store EntitySeeds in a container and use them to create EntityPointers
if you need access to the corresponding Entity. EntitySeeds are intended to use as little memory as possible while
allowing quick access to Entities. Notice, that some grids use the default implementations of EntitySeed which
internally is an EntityPointer (see below).

#### How can I access the id of an Intersection?

Only entities have ids. Intersections are not entities!

All grids have entities of codimension dim (vertices) and codimension 0 (elements), and all have intersections. Some grids have entities of other codimensions as well. Even if a grid has entities of codimension 1, these are not the same as entities. The most obvious differences are:

* Intersections know the neighbouring cells, codim 1 entities don't.
* Intersections have an orientation, codim 1 entities don't.
* Codim 1 entities have indices and ids and can be used to obtain array indices from a mapper (even if the grid does not support codim 1 entity objects!) For intersections, indices, id's and maps are not provided by core Dune, though it is possibly to build such things yourself.
* In non-conforming grids you may have the following situation:
  <table style="border-collapse: collapse;">
  <tr>
  <td rowspan="2" style="border: 1px solid #888; padding: 5ex;">a</td>
  <td style="border: 1px solid #888; padding: 2ex;">b</td>
  <td style="border: 1px solid #888; padding: 2ex;">c</td>
  </tr>

  <tr>
  <td style="border: 1px solid #888; padding: 2ex">d</td>
  <td style="border: 1px solid #888; padding: 2ex">e</td>
  </tr>

  </table>Elements b, c, d and e all have four codim 1 subentities and four corresponding intersections. Element a also has four codim 1 subentities, but it has five intersections. The codim 1 entity to the right of b is the same as the codim 1 entity to the left of d. You have three codim 1 entities to the right of a however: one covering the whole of a's right border, and two smaller ones each covering the upper and the lower half of a's border, respectively.

If you know you have a conforming grid, you have a correspondence between intersections and codim 1 entities (ignoring the orientation of the intersections). In that case you can usually get away by using the id of the corresponding codim 1 entity as the id of the intersection (even if the grid does not support codim 1 entities as independent objects. Use

```grid.globalIdSet().subId(e, i.indexInInside(), 1);```

where i is the intersection and e is the codim 0 entity i was obtained from. If you don't have e readily available, you can use

```grid.globalIdSet().subId(*i.inside(), i.indexInInside(), 1);```

instead.

#### How do I associate data to grid entities and intersections when creating a grid?

We decided that the grid should not carry any additional data and that all such should be associated to grid entities or intersections using indices or ids.

As we cannot know how a grid implementation handles its creation process the grid grid interface does explicitly allow that entities and boundary segments are reordered during creation time. Hence the corresponding indices after grid creation are not guaranteed to be consistent with the insertion order (although they are for some implementations).

In order to overcome this the `GridFactory` provides the `insertionIndex` methods that return the indices of vertices, elements, and intersections with respect to the insertion order. Combining this with the indices and ids provides by the grid, (leaf indices, level indices, and ids for entities and boundary segment indices for intersections) you can reconstruct this reordering and associate data to entities and intersections. Notice that this information is only available as long as the `GridFactory` exists.

### Core Grid Implementations

#### Why does globalRefine() sometimes segfault for AlbertaGrid<3>?
This is a known issue with ALBERTA. The segfault you experience is a actually a stack overflow during the recursive bisection algorithm. When trying to refine an element, ALBERTA tries to use a prescribed refinement edge. It tries to refine all elements that share this edge (called a refinement patch) at the same time. But this edge need not be a refinement edge for the other elements in the patch. Therefore, ALBERTA tries to recursively refine such an element before refining the current patch. This way, one can run into cycles. While ALBERTA can ensure that the recursive bisection terminates in 2d, no such algorithm is known in 3d. One has to rely on heuristics to solve this problem. A simple idea is to select the longest edge for refinement on the macro grid. If this edge is unique, it makes the algorithm terminate in 2d. In 3d, this works unless an element is refined more that once. By default, the GridFactory for AlbertaGrid does not modify the grid. There are two reasons for this:

* We don't know any reliable algorithm (not even heuristically) to make the bisection terminate in 3d. A simple idea is to select the longest edge for refinement on the macro grid, which works in 2d in general. But in 3d this does not necessarily work.
* In 2d ALBERTA handles this problem by itself (and 1d is trivial)
* Changing the refinement edge can result in a less local refinement (longest-edge bisection is known to produce very non-local refinements.

We can give you two hints, though:

* As long as you want to triangulate cube grids the DGF parser will split it into tetrahedra that ALBERTA can handle.
* The refinement edge in ALBERTA is always between vertices 0 and 1.

#### Why does adapt() on ALUGrid sometimes lead to a failing assertion?

The ALUGrid library keeps track of open iterators. Since these become invalid during adaptation, ALUGrid asserts that no dangling iterators exist. This can be avoided using -DNDEBUG in the compile flags for the ALUGrid library (which is recommended in any case).

### The Iterative Solver Template Library (ISTL)

### Local functions
#### Why does MonomialLocalInterpolation::interpolate() not recover the coefficients of polynomials?

The `interpolate()` method does in fact compute an L^2 projection of the given function with respect to the `GeometryType` handed to `MonomialLocalFiniteElement`. Although this should reproduce polynomials with exact arithmetics the implementation is unstable for higher polynomial degrees.
