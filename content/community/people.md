+++
title = "People"
[menu.main]
parent = "community"
weight = 3
+++

Dune Core Developers
--------------------

Dune is a community project and we'd like to express our gratitude toward the numerous
contributors in the past and in the future. The file
LICENSE.md within every Dune module attributes the work of numerous more contributors.

If you have questions about Dune, please send them to the [mailing list](/community/mailinglists), unless you have reason to keep your mail private.

The Dune Core Developers are the maintainers of the Dune core modules. They participate and
oversee the development of the Dune core modules and the Dune grid interface.

* [Peter Bastian][1a] ([IWR, Universität Heidelberg][1b])
* [Markus Blatt][2a] ([Dr. Markus Blatt - HPC-Simulation-Software & Services, Heidelberg][2b])
* [Samuel Burbulla][19a] ([Universität Stuttgart][19b])
* [Ansgar Burchardt][3a] ([Institut für Numerik, TU Dresden][3b])
* [Andreas Dedner][4a] ([Warwick Mathematics Institute, University of Warwick, UK][4b])
* [Christian Engwer][5a] ([Institut für Numerische und Angewandte Mathematik, Universität Münster][5b])
* [Jorrit Fahlke (Jö)][6a]
* [Carsten Gräser][8a] ([Department Mathematik, FAU Erlangen-Nürnberg][8b])
* Christoph Grüninger
* Dominic Kempf ([Scientific Software Center, Universität Heidelberg][17])
* [Robert Klöfkorn][11a] ([Lund University, Sweden][11b])
* [Timo Koch][18a] ([Universitetet i Oslo, Norway][18b])
* [Santiago Ospina De Los Ríos][1c] ([IWR, Universität Heidelberg][1b])
* [Simon Praetorius][16a] ([Institut für Wissenschaftliches Rechnen, TU Dresden][16b])
* [Oliver Sander][15a] ([Institut für Numerik, TU Dresden][15b])

[1a]: http://conan.iwr.uni-heidelberg.de/people/peter/
[1b]: http://conan.iwr.uni-heidelberg.de/
[1c]: https://conan.iwr.uni-heidelberg.de/people/santiago/
[2a]: http://www.dr-blatt.de/
[2b]: http://www.dr-blatt.de/
[3a]: https://www.math.tu-dresden.de/~ansgar/
[3b]: https://tu-dresden.de/mn/math/numerik
[4a]: http://www2.warwick.ac.uk/fac/sci/maths/people/staff/andreas_dedner/
[4b]: http://www2.warwick.ac.uk/fac/sci/maths/
[5a]: https://www.uni-muenster.de/AMM/engwer/team/engwer.shtml
[5b]: https://www.uni-muenster.de/AMM/
[6a]: mailto:jorrit@jorrit.de
[8a]: https://www.math.fau.de/angewandte-mathematik-3/mitarbeiter-am3/prof-dr-carsten-graeser/
[8b]: https://www.math.fau.de
[11a]: https://portal.research.lu.se/en/persons/robert-kl%C3%B6fkorn/
[11b]: https://www.maths.lu.se/english/
[15a]: https://tu-dresden.de/mn/math/numerik/sander
[15b]: https://tu-dresden.de/mn/math/numerik
[16a]: https://tu-dresden.de/mn/math/wir/das-institut/beschaeftigte/simon-praetorius
[16b]: https://tu-dresden.de/mn/math/wir/
[17]: https://ssc.iwr.uni-heidelberg.de/
[18a]: https://timokoch.github.io/
[18b]: https://www.mn.uio.no/
[19a]: https://www.ians.uni-stuttgart.de/institute/team/Burbulla/
[19b]: https://www.ians.uni-stuttgart.de/

Alumni
------

Former Dune Core Developers

* Christoph Gersbacher (Universität Freiburg)
* Steffen Müthing (Universität Heidelberg, Universität Stuttgart)
* Martin Nolte (Universität Freiburg)
* Mario Ohlberger (Universität Münster)
