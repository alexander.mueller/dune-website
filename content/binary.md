+++
title = "Binary Packages"
[menu.main]
parent = "releases"
weight = 2
+++

Linux distributions offer various Dune modules as binary packages for convenient download
and installation.  This page lists the packaging status of the core modules.  For others,
consult the respective module pages, or your distribution directly.

Technically, the following table shows the packaging status of `dune-common` in different distributions.
You can expect the other core modules `dune-geometry`, `dune-grid`, `dune-localfunctions`, and
`dune-istl` to be packaged in the same versions.

{{< rightpanel noframe >}}
<a href="https://repology.org/metapackage/dune-common">
<img alt="Packaging status"
src="https://repology.org/badge/vertical-allrepos/dune-common.svg" width="100%"></a>
{{< /rightpanel >}}


### Debian, Ubuntu and derivatives

Here is some more specific information on the Debian packages.
The packages are called `libdune-foo-dev`, where `foo` is a module name like for example
`common`, `grid`, `geometry`, `istl`, or `localfunctions`.  A  [complete list of packages](https://packages.debian.org/search?keywords=libdune+dev&searchon=names&suite=all&section=all)
is available.
When developing with Dune, we recommend also installing the detached debug symbols from the `libdune-foo-dbg` (or in the future `libdune-foo-dev-dbgsym`) packages.

Offline documentation is available in the `libdune-foo-doc` packages and installed to `/usr/share/doc/libdune-foo-doc/doxygen/index.html`.

If you want to help with the Debian packaging, you can download the packaging information for `dune-common` by

`git clone https://salsa.debian.org/science-team/dune-common.git`
([Browse Repository](https://salsa.debian.org/science-team/dune-common.git))

The Debian package history and the Debian issue tracker can be found in the [Debian Package Tracker](https://tracker.debian.org/pkg/dune-common).

The links for all other available modules are similar.
