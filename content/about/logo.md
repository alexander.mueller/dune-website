+++
title = "Logo"
[menu.main]
parent = "about"
weight = 7
+++

### LOGO
This logo or a modified version may be used by anyone to refer to the Dune project, but does not necessarily indicate endorsement by the project. It's main intention are talks and presentations covering the Dune project.

All PNG logos were created from this [gimp](http://www.gimp.org/) [file](/share/dune-logo.xcf). The PDF logos were created in a very similar fashion from an [SVG file](/share/dune-logo.svg) using [Inkscape](http://inkscape.org/). If none of the provided logos fit your needs you can base you own logo on this file.

![dune-logo](/img/dune-logo1.png "dune-logo1")
[Download as PDF](/pdf/dune-logo1.pdf "dune-logo1")

![dune-logo](/img/dune-logo2.png "dune-logo2")
[Download as PDF](/pdf/dune-logo2.pdf "dune-logo2")

![dune-logo](/img/dune-logo3.png "dune-logo3")
[Download as PDF](/pdf/dune-logo3.pdf "dune-logo3")

![dune-logo](/img/dune-logo4.png "dune-logo4")
[Download as PDF](/pdf/dune-logo4.pdf "dune-logo4")

![dune-logo](/img/dune-logo5.png "dune-logo5")
[Download as PDF](/pdf/dune-logo5.pdf "dune-logo5")

![dune-logo](/img/dune-logo6.png "dune-logo6")
[Download as PDF](/pdf/dune-logo6.pdf "dune-logo6")

![dune-logo](/img/dune-logo7.png "dune-logo7")
[Download as PDF](/pdf/dune-logo7.pdf "dune-logo7")

![dune-logo](/img/dune-logo8.png "dune-logo8")
[Download as PDF](/pdf/dune-logo8.pdf "dune-logo8")


### License
All logos and the gimp and SVG files are licensed under a [Creative Commons License](http://creativecommons.org/licenses/by-sa/2.5/).
