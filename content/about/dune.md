+++
# Our index.html template looks for this file!
home = 1
title = "What is Dune?"
[menu.main]
parent = "about"
weight = 1
+++

### DUNE
DUNE, the Distributed and Unified Numerics Environment is a modular
toolbox for solving partial differential equations (PDEs) with
grid-based methods. It supports the easy implementation of methods
like Finite Elements (FE), Finite Volumes (FV), and also Finite
Differences (FD).

<img src= "/img/dunedesign.png" class="img-responsive pull-right">

DUNE is free software licensed under the GPL (version 2) with a so
called "runtime exception" (see [**license**](/about/license
"License")). This licence is similar to the one under which the
**libstdc++** libraries are distributed. Thus it is possible to use
DUNE even in proprietary software.

The underlying idea of DUNE is to create slim interfaces allowing an
efficient use of legacy and/or new libraries. Modern C++ programming
techniques enable very different implementations of the same concept
using a common interface at a very low
overhead. Thus DUNE ensures efficiency in scientific computations and
supports high-performance computing applications.

Particular highlights are

 * a [generic grid interface](/doxygen/master/),
   allowing to interface a range of very different [grid implementations](/doc/grids)
 * the [Iterative Solver Template Library](/modules/dune-istl),
   featuring an algebraic multigrid preconditioner
 * Highlevel [interfaces for trial and test
   functions](/modules/dune-localfunctions) and [generic
   discretization modules](/groups/disc/)
 * [Python binding](/doc/pythonbindings) for the full grid interface
   and a flexible concept to provide bindings for user modules.

<!--more-->

#### DUNE is based on the following main principles:

* Separation of data structures and algorithms by abstract interfaces.
_This provides more functionality with less code and also ensures maintainability and extendability of the framework._

* Efficient implementation of these interfaces using generic programming techniques.
_Static polymorphism allows the compiler to do more optimizations, in particular function inlining, which in turn allows the interface to have very small functions (implemented by one or few machine instructions) without a severe performance penalty. In essence the algorithms are parametrized with a particular data structure and the interface is removed at compile time. Thus the resulting code is as efficient as if it would have been written for the special case._

* Reuse of existing finite element packages with a large body of functionality.
_In particular the finite element codes UG, ALBERTA, and ALUGrid have been adapted to the DUNE framework. Thus, parallel and adaptive meshes with multiple element types and refinement rules are available. All these packages can be linked together in one executable._


### Modules

The framework consists of a number of modules which are downloadable as separate packages. There is a set of
[core modules](/groups/core) which are used by most other packages. The [dune-grid](/modules/dune-grid) core module
already contains some grid implementation and further [grid managers](/groups/grid) are available as extra modules.
Main [discretization modules](/groups/disc) providing the infrastructure for solving partial differential equations
using DUNE are available as separate modules. The modular structure of DUNE allows to only use a small set of th
 modules (e.g., only the solver module [dune-istl](/modules/dune-istl) or the module [dune-localfunctions](/modules/dune-localfunctions)
 containing shape functions without for example using the [dune-grid](/modules/dune-grid) or a full discretization module).

For further information, have a look at the main [features](/about/features), read the [documentation for application writers](/doc/tutorials) or get in touch with the [people actively developing DUNE](/community/people).
