+++
# This page is a place holder, because we need to render the header into html
# to later put doxygen documentation into it. I have not yet found out how to
# have hugo render a site not connected to a content file.
title = "$projectname"
layout = "header"
+++
