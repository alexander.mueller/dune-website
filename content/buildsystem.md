+++
title = "Buildsystem Docs"
layout = "buildsystem"
[menu.main]
parent = "further"
identifier = "buildsystem"
weight = 5
+++

# DUNE Build System Documentation

Note that the [Dune book](/doc/book/)
contains a [chapter about the build system](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf).

Beginning from version 2.4, DUNE's CMake build system is documented with Sphinx.
Please choose the documentation you want to view from the following list.
